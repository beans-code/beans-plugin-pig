package net.beanscode.plugin.pig;

public class HadoopJob {

	private String applicationId = null;
	
	private String applicationName = null;
	
	private String applicationType = null;
	
	private HadoopJobState state = null;
	
	public HadoopJob() {
		
	}
	
	@Override
	public String toString() {
		return getApplicationId()
				+ " " + getApplicationName()
				+ " " + getApplicationType()
				+ " " + getState();
	}

	public String getApplicationId() {
		return applicationId;
	}

	public HadoopJob setApplicationId(String applicationId) {
		this.applicationId = applicationId.trim();
		return this;
	}

	public String getApplicationName() {
		return applicationName;
	}

	public HadoopJob setApplicationName(String applicationName) {
		this.applicationName = applicationName.trim();
		return this;
	}

	public String getApplicationType() {
		return applicationType;
	}

	public HadoopJob setApplicationType(String applicationType) {
		this.applicationType = applicationType.trim();
		return this;
	}

	public HadoopJobState getState() {
		return state;
	}

	public HadoopJob setState(HadoopJobState state) {
		this.state = state;
		return this;
	}

	public HadoopJob setState(String state) {
		this.state = HadoopJobState.valueOf(state.trim());
		return this;
	}
}
