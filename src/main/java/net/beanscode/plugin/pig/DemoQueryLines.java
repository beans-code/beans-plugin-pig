package net.beanscode.plugin.pig;

import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;

import java.io.File;
import java.io.IOException;

import net.beanscode.model.BeansConst;
import net.beanscode.model.dataset.Dataset;
import net.beanscode.model.dataset.DatasetFactory;
import net.beanscode.model.dataset.Table;
import net.beanscode.model.dataset.TableFactory;
import net.beanscode.model.notebook.Notebook;
import net.beanscode.model.notebook.NotebookEntryFactory;
import net.beanscode.model.notebook.NotebookFactory;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.args.ArgsUtils;
import net.hypki.libs5.weblibs.jobs.JobsManager;
import net.hypki.libs5.weblibs.user.Email;
import net.hypki.libs5.weblibs.user.User;

public class DemoQueryLines {

	public static void main(String[] args) throws ValidationException, IOException {
		System.setProperty("weblibsSettings", "settings-beans-dev.json");
		System.setProperty("javax.xml.parsers.DocumentBuilderFactory", "com.sun.org.apache.xerces.internal.jaxp.DocumentBuilderFactoryImpl");
		
		assertTrue(ArgsUtils.getString(args, "user") != null, "Arg --user is not specified");
		
		new File("test-lines--1.pdf").delete();
		new File("test-lines--20.pdf").delete();
		new File("test-lines--union.pdf").delete();
				
		final User user = User.getUser(new Email(ArgsUtils.getString(args, "user")));
		final Notebook notebook = NotebookFactory.searchNotebooks(user.getUserId(), "Test lines queries", 0, 20).getObjects().get(0);
		final Dataset dataset = DatasetFactory.searchDatasets(user.getUserId(), "line1", 0, 10).getObjects().get(0);
		final Table table = TableFactory.searchTables(user.getUserId(), dataset.getId(), "points", 0, 100).getObjects().get(0);
		
		PigScript q = null;
		if (notebook.getEntries().size() > 0) {
			final UUID firstEntryId = notebook.getEntries().get(0);
			q = (PigScript) NotebookEntryFactory.getNotebookEntry(firstEntryId);
		} else
			q = new PigScript(user, notebook.getId(), "query demo");
		
		q.setQuery("line1 = load 'line1/points' using Table();\n"
//				+ "DESCRIBE line1;"
				+ "line20 = load 'line20/points' using Table();\n"
//				+ "DESCRIBE line20;"
//				+ "l1 = FILTER line1 BY x.value > 5;"
//				+ "DUMP l1;"
//				+ "DESCRIBE l1;"
//				+ "DUMP line1;"
//				+ "DUMP line20;"
				
				+ "X = UNION line20, line1;\n"
//				+ "DESCRIBE X;"
//				+ "DUMP X;"
//				+ "lf1 = FOREACH line1 GENERATE flatten(line1);"
//				+ "lf20 = FOREACH line20 GENERATE flatten(line20);"
//				+ "f1 = FOREACH line1 GENERATE key, y, x;"
//				+ "f2 = FOREACH line20 GENERATE key, y, x;"
//				+ "Y = UNION f1, f2;"
//				+ "DUMP Y;"

//				+ "B = FOREACH line1 GENERATE flatten(x), flatten(y);"
//				+ "DESCRIBE B;"
//				+ "DUMP B;"
//				+ "C = FOREACH line20 GENERATE flatten(x), flatten(y);"
//				+ "BC = UNION B, C;"
//				+ "DUMP B;"
//				+ "DUMP C;"
//				+ "DUMP BC;"
				
//				+ "X_GROUP = GROUP X ALL;"
//				+ "xc = FOREACH X_GROUP GENERATE COUNT(X);"
//				+ "DUMP xc;"
				+ "store line1 into 'plot1/TYPE points COLUMNS x:y TITLE \"x vs. y\"' using Plot();\n"
				+ "store line20 into 'plot2/TYPE points COLUMNS x:y TITLE \"x vs. y\"' using Plot();\n"
				+ "store X into 'plot3/TYPE points COLUMNS x:y TITLE \"x vs. y\" COLOR BY tbid' using Plot();\n"
				);
		q.save();
		q.runQuery(false);
		
		JobsManager.runAllJobs(BeansConst.getAllChannelsNames());
		
//		((Plot) NotebookEntryFactory.getNotebookEntry(q.getId())).setFilename("test-lines--1.pdf").plot();;
//		((Plot) NotebookEntryFactory.getNotebookEntry(q.getId())).setFilename("test-lines--20.pdf").plot();
//		((Plot) NotebookEntryFactory.getNotebookEntry(q.getId())).setFilename("test-lines--union.pdf").plot();
//		
//		LibsLogger.debug(DemoQueryLines.class, ((Plot) NotebookEntryFactory.getNotebookEntry(q.getId())).toString());
//		LibsLogger.debug(DemoQueryLines.class, ((Plot) NotebookEntryFactory.getNotebookEntry(q.getId())).toString());
//		LibsLogger.debug(DemoQueryLines.class, ((Plot) NotebookEntryFactory.getNotebookEntry(q.getId())).toString());
		
		LibsLogger.debug(DemoQueryLines.class, "Finished!");
	}
}
