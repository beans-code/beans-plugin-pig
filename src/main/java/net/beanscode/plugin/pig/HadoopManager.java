package net.beanscode.plugin.pig;

import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.system.Basher2;

public class HadoopManager {

	public static void hadoopStop(HadoopJob hadoopJob) throws IOException {
		String hadoopHome = System.getenv("HADOOP_HOME");
		
		assertTrue(notEmpty(hadoopHome), "HADOOP_HOME is not set");
		
		new Basher2()
			.addEnv("HADOOP_HOME", hadoopHome)
			.add(hadoopHome + "/bin/yarn")
			.add("application")
			.add("-kill")
			.add(hadoopJob.getApplicationId())
			.run();
	}

	public static List<HadoopJob> getApplicationsList() throws IOException {
	//		String javaHome = System.getenv("JAVA_HOME");
			String hadoopHome = System.getenv("HADOOP_HOME");
			
	//		assertTrue(notEmpty(javaHome), "JAVA_HOME is not set");
			assertTrue(notEmpty(hadoopHome), "HADOOP_HOME is not set");
			
			List<HadoopJob> jobs = new ArrayList<HadoopJob>();
			for (String line : new Basher2()
	//								.setWorkingDir(new FileExt(hadoopHome + "/bin/"))
	//								.addEnv("JAVA_HOME", javaHome)
									.addEnv("HADOOP_HOME", hadoopHome)
									.add(hadoopHome + "/bin/yarn")
									.add("application")
									.add("-list")
									.add("-appStates")
									.add("ALL")
									.runIter()) {
				if (StringUtilities.notEmpty(line)
						&& line.startsWith("application_")) {
					String [] columns = StringUtilities.split(line, '\t');
	//				LibsLogger.info(PigManager.class, line);
				
					jobs.add(new HadoopJob()
							.setApplicationId(columns[0])
							.setApplicationName(columns[1])
							.setApplicationType(columns[2])
							.setState(columns[5]));
				}
			}
			
			return jobs;
		}

}
