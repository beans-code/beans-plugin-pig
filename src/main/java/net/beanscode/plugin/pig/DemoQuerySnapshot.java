package net.beanscode.plugin.pig;

import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;

import java.io.IOException;

import net.beanscode.model.BeansConst;
import net.beanscode.model.dataset.Dataset;
import net.beanscode.model.dataset.DatasetFactory;
import net.beanscode.model.dataset.Table;
import net.beanscode.model.dataset.TableFactory;
import net.beanscode.model.notebook.Notebook;
import net.beanscode.model.notebook.NotebookFactory;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.args.ArgsUtils;
import net.hypki.libs5.utils.utils.AssertUtils;
import net.hypki.libs5.weblibs.jobs.JobsManager;
import net.hypki.libs5.weblibs.user.Email;
import net.hypki.libs5.weblibs.user.User;

public class DemoQuerySnapshot {

	public static void main(String[] args) throws ValidationException, IOException {
		System.setProperty("weblibsSettings", "settings-beans-dev.json");
		System.setProperty("javax.xml.parsers.DocumentBuilderFactory", "com.sun.org.apache.xerces.internal.jaxp.DocumentBuilderFactoryImpl");
		
		assertTrue(ArgsUtils.getString(args, "user") != null, "Arg --user is not specified");
				
		final User user = User.getUser(new Email(ArgsUtils.getString(args, "user")));
		final Notebook notebook = NotebookFactory.searchNotebooks(user.getUserId(), "Test snapshot queries", 0, 20).getObjects().get(0);
		final Dataset dataset = DatasetFactory.searchDatasets(user.getUserId(), "MOCCA rbar55 snapshot", 0, 10).getObjects().get(0);
		final Table table = TableFactory.searchTables(user.getUserId(), dataset.getId(), "snapshot", 0, 100).getObjects().get(0);
		
		AssertUtils.assertTrue(table != null, "Table not found");
				
		PigScript q = new PigScript(user, notebook.getId(), "query demo");
//		q.setId(new UUID("eb78821c595b8d9eb53c17084b3f8541"));
		q.setQuery("snap = load 'MOCCA rbar55 snapshot/snapshot' using Table();\n"
				+ "DESCRIBE snap;\n\n"
//				+ "DUMP snap;"
				
//				+ "C = FILTER snap BY key MATCHES '.*22c84.*';" // works fine
//				+ "DUMP C;"
				
				+ "bss = FILTER snap BY type1.value == 2 OR type2.value == 2;\n"
				+ "DUMP bss;\n\n"
				
				+ "bssBinned = FOREACH bss GENERATE *, histogram(1.0, 2.0, 0.1, m2.value) as bin;\n"
				+ "DUMP bssBinned;\n\n"
				
				+ "bssGr = GROUP bssBinned BY (time, bin);\n"
				+ "DUMP bssGr;\n\n"
				
//				+ "bssGrCount = FOREACH bssGr GENERATE $1.key, $0.time.value as time, $0.bin as bin, COUNT($1) as count;"
				+ "bssGrCount = FOREACH bssGr GENERATE ('time', $0.time.value), ('bin', $0.bin), ('count', COUNT($1));\n"
				+ "DUMP bssGrCount;\n"
				+ "DESCRIBE bssGrCount;\n\n"
				
				+ "store bssGrCount into 'plot1/SPLIT BY time TYPE boxes 0.9 XRANGE 0; 10 YRANGE 0; 5 COLUMNS bin:count TITLE \"bin vs. count\"' using Plot();"
				);
		q.save();
		q.runQuery(false);
		
		JobsManager.runAllJobs(BeansConst.getAllChannelsNames());
		
//		((Plot) NotebookEntryFactory.getNotebookEntry(q.getId())).setFilename("test-snapshot--1.pdf").plot();
		
		LibsLogger.debug(DemoQuerySnapshot.class, "Finished!");
	}
}
