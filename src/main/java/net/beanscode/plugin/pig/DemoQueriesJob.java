package net.beanscode.plugin.pig;

import java.io.IOException;

import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.weblibs.jobs.Job;
import net.hypki.libs5.weblibs.user.User;
import net.hypki.libs5.weblibs.user.UserFactory;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

public class DemoQueriesJob extends Job {
	
	@NotNull
	@NotEmpty
	@AssertValid
	@Expose
	private UUID userId = null;

	public DemoQueriesJob() {
		
	}
	
	public DemoQueriesJob(UUID userId) {
		setUserId(userId);
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		try {
			final User user = UserFactory.getUser(getUserId());
			
			if (user == null) {
				LibsLogger.error(DemoQueriesJob.class, "Cannot find user with ID ", getUserId(), ". Creting DEMO queries aborted.");
				return;
			}
			
			DemoQueriesMOCCA.createDemoLineQueries(user);
		} catch (Exception e) {
			LibsLogger.error(DemoQueriesJob.class, "Cannot create demo queries, consuming job", e);
		}
	}

	public UUID getUserId() {
		return userId;
	}

	public void setUserId(UUID userId) {
		this.userId = userId;
	}
}
