package net.beanscode.plugin.pig;

import java.io.IOException;

import net.beanscode.model.dataset.Table;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.weblibs.jobs.Job;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

public class PigScriptClearEmptyTablesJob extends Job {
	
	@Expose
	@NotNull
	@AssertValid
	private UUID pigScriptId = null;
	
	private PigScript pigScriptCache = null;

	public PigScriptClearEmptyTablesJob() {
		
	}
	
	public PigScriptClearEmptyTablesJob(UUID pigScriptId) {
		setPigScriptId(pigScriptId);
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		if (getPigScript() == null) {
			LibsLogger.info(PigScriptClearEmptyTablesJob.class, "There is no Pig script " + getPigScriptId() 
					+ " anymore, skipping job..");
			return;
		}
		
		getPigScript().clearEmptyTables(false);
	}
	
	protected PigScript getPigScript() {
		if (pigScriptCache == null) {
			try {
				pigScriptCache = PigScript.getQuery(getPigScriptId());
			} catch (IOException e) {
				LibsLogger.error(PigScriptClearEmptyTablesJob.class, "Cannot get Pig script from DB", e);
			}
		}
		return pigScriptCache;
	}

	public UUID getPigScriptId() {
		return pigScriptId;
	}

	public void setPigScriptId(UUID pigScriptId) {
		this.pigScriptId = pigScriptId;
	}
}
