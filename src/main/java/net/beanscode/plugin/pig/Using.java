package net.beanscode.plugin.pig;

import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import net.beanscode.model.cass.Function;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.utils.AssertUtils;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

public class Using implements Serializable {
	
	@Expose
	@NotNull
	@AssertValid
	private UUID id = null;
	
	@Expose
	@NotNull
	@AssertValid
	private String location = null;
	
	@Expose
	@NotNull
	@AssertValid
	private Boolean store = null;
	
	@Expose
	@NotNull
	@AssertValid
	private String datasetQuery = null;
	
	@Expose
	@NotNull
	@AssertValid
	private String tableQuery = null;
	
	@Expose
	@NotNull
	@AssertValid
	private String filter = null;
	
	@Expose
	@NotNull
	@AssertValid
	private UUID tableId = null;
	
	@Expose
	@NotNull
	@AssertValid
	private UUID pigScriptId = null;
	
	@Expose
	@NotNull
	@AssertValid
	private List<Function> functions = null;
	
	@Deprecated
	@Expose
	@NotNull
	@AssertValid
	private int functionsNewColumnsCount = 0;

	public Using() {
		setId(UUID.random());
	}
	
	@Override
	public String toString() {
		return getLocation() + "-" + getDatasetQuery() + "-" + getTableQuery();
	}

	public String getLocation() {
		return location;
	}

	public Using setLocation(String location) {
		this.location = location;
		return this;
	}

	public Boolean getStore() {
		return store;
	}

	public Using setStore(Boolean store) {
		this.store = store;
		if (this.tableId == null)
			setTableId(UUID.random());
		return this;
	}

	public String getDatasetQuery() {
		return datasetQuery;
	}

	public Using setDatasetQuery(String datasetQuery) {
		this.datasetQuery = datasetQuery;
		return this;
	}

	public String getTableQuery() {
		return tableQuery;
	}

	public Using setTableQuery(String tableQuery) {
		this.tableQuery = tableQuery;
		return this;
	}

	public String getFilter() {
		return filter;
	}

	public Using setFilter(String filter) {
		this.filter = filter;
		return this;
	}

	public boolean isStoreFunctionLikely() {
		return nullOrEmpty(datasetQuery) && nullOrEmpty(tableQuery);
	}
	
	public Boolean isLoadFunction() {
		return notEmpty(datasetQuery) 
				|| notEmpty(tableQuery)
				|| (location != null && location.toLowerCase().contains("datasets"));
	}

	public Boolean isStoreFunction() {
		return !isLoadFunction();
	}

	public UUID getTableId() {
		return tableId;
	}

	public Using setTableId(UUID tableId) {
		AssertUtils.assertTrue(this.tableId == null, "TableId has id already set");
		this.tableId = tableId;
		return this;
	}

	public List<Function> getFunctions() {
		if (functions == null)
			functions = new ArrayList<Function>();
		return functions;
	}

	public Using setFunctions(List<Function> functions) {
		this.functions = functions;
		return this;
	}

	@Deprecated
	public int getFunctionsNewColumnsCount() {
		return functionsNewColumnsCount;
	}

	public void setFunctionsNewColumnsCount(int functionsNewColumnsCount) {
		this.functionsNewColumnsCount = functionsNewColumnsCount;
	}

	public boolean isFunctionListEmpty() {
		return functions == null || functions.size() == 0;
	}

	public UUID getPigScriptId() {
		return pigScriptId;
	}

	public void setPigScriptId(UUID pigScriptId) {
		this.pigScriptId = pigScriptId;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}
}
