package net.beanscode.plugin.pig;

import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;

import java.io.IOException;

import net.beanscode.model.BeansConst;
import net.beanscode.model.dataset.Dataset;
import net.beanscode.model.dataset.DatasetFactory;
import net.beanscode.model.dataset.Table;
import net.beanscode.model.dataset.TableFactory;
import net.beanscode.model.notebook.Notebook;
import net.beanscode.model.notebook.NotebookFactory;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.args.ArgsUtils;
import net.hypki.libs5.utils.utils.AssertUtils;
import net.hypki.libs5.weblibs.jobs.JobsManager;
import net.hypki.libs5.weblibs.user.Email;
import net.hypki.libs5.weblibs.user.User;

public class DemoQueryMOCCASnapshot {

	public static void main(String[] args) throws ValidationException, IOException {
		System.setProperty("weblibsSettings", "settings-beans-dev.json");
		System.setProperty("javax.xml.parsers.DocumentBuilderFactory", "com.sun.org.apache.xerces.internal.jaxp.DocumentBuilderFactoryImpl");
		
		assertTrue(ArgsUtils.getString(args, "user") != null, "Arg --user is not specified");
				
		final User user = User.getUser(new Email(ArgsUtils.getString(args, "user")));
		final Notebook notebook = NotebookFactory.searchNotebooks(user.getUserId(), "Snapshot queries", 0, 20).getObjects().get(0);
		final Dataset dataset = DatasetFactory.searchDatasets(user.getUserId(), "600k snapshot", 0, 10).getObjects().get(0);
		final Table table = TableFactory.searchTables(user.getUserId(), dataset.getId(), "snapshot", 0, 100).getObjects().get(0);
		
		AssertUtils.assertTrue(table != null, "Table not found");
				
		PigScript q = new PigScript(user, notebook.getId(), "query demo");
		q.setQuery("snap = load '600k snapshot3/snapshot3/' using Table();"
				+ "DESCRIBE snap;"
//				+ "DUMP snap;"
				
				+ "ms = FILTER snap BY type1.value == 1;"
//				+ "DUMP ms;"
				
				+ "msBinned = FOREACH ms GENERATE *, histogram(0.0, 2.0, 0.1, m1.value) as bin;"
//				+ "DESCRIBE msBinned;"
				
				+ "msGr = GROUP msBinned BY (time, bin);"
//				+ "DUMP msGr;"
				+ "DESCRIBE msGr;"
				
//				+ "msGrCount = FOREACH msGr GENERATE $1.key, $0.time.value as time, $0.bin as bin, COUNT($1) as count;"
				
				+ "msGrCount = FOREACH msGr GENERATE ('time', $0.time.value), ('bin', $0.bin), ('count', COUNT($1));"
//				+ "msGrCount = FOREACH msGr GENERATE *, ('count', COUNT($1));"
				
				+ "DUMP msGrCount;"
				
				+ "store msGrCount into 'plot1/SPLIT BY time TYPE boxes 0.1 XRANGE 0; 10 COLUMNS bin:count TITLE \"bin vs. count\"' using Plot();"
				);
		q.save();
		q.runQuery(false);
		
		JobsManager.runAllJobs(BeansConst.getAllChannelsNames());
		
//		net.hypki.moccabeans.model.plots.Plot.getPlot(q.getId(), "plot1").saveToFile("test-snapshot--");
		
		LibsLogger.debug(DemoQueryMOCCASnapshot.class, "Finished!");
	}
}
