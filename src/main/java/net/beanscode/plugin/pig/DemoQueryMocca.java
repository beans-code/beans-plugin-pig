package net.beanscode.plugin.pig;

import static net.hypki.libs5.utils.utils.AssertUtils.assertFalse;
import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;

import java.io.IOException;
import java.util.List;

import net.beanscode.model.BeansConst;
import net.beanscode.model.dataset.Dataset;
import net.beanscode.model.dataset.DatasetFactory;
import net.beanscode.model.dataset.Table;
import net.beanscode.model.dataset.TableFactory;
import net.beanscode.model.notebook.Notebook;
import net.beanscode.model.notebook.NotebookFactory;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.utils.args.ArgsUtils;
import net.hypki.libs5.utils.utils.AssertUtils;
import net.hypki.libs5.weblibs.jobs.JobsManager;
import net.hypki.libs5.weblibs.user.Email;
import net.hypki.libs5.weblibs.user.User;

public class DemoQueryMocca {

	public static void main(String[] args) throws ValidationException, IOException {
		System.setProperty("weblibsSettings", "settings-beans-dev.json");
		System.setProperty("javax.xml.parsers.DocumentBuilderFactory", "com.sun.org.apache.xerces.internal.jaxp.DocumentBuilderFactoryImpl");
		
		assertTrue(ArgsUtils.getString(args, "user") != null, "Arg --user is not specified");
		
		JobsManager.runAllJobs(BeansConst.getAllChannelsNames());
		
		final User user = User.getUser(new Email(ArgsUtils.getString(args, "user")));
		Notebook notebook = NotebookFactory.searchNotebooks(user.getUserId(), "MOCCA system.dat queries", 0, 20).getObjects().get(0);
		
		List<Dataset> datasets = DatasetFactory.searchDatasets(user.getUserId(), "MOCCA 600k rbar100", 0, 10).getObjects();
		assertFalse(datasets.size() > 1, "Too many datasets found ");
		assertFalse(datasets.size() == 0, "No datasets found");
		Dataset ds = datasets.get(0);
//		
		List<Table> tables = TableFactory.searchTables(user.getUserId(), ds.getId(), "system", 0, 100).getObjects();
		assertFalse(tables.size() > 1, "Too many tables found");
		assertFalse(tables.size() == 0, "No tables found");
		
		Table table = TableFactory.getTable(tables.get(0).getId());
		AssertUtils.assertTrue(table != null, "Table not found");
		
		PigScript q = null; 
		
//		Query q = new Query(user, notebook.getId(), "query demo");
//		q.setId(new UUID("b5f46198cc1065e118e66353a83abc21"));
//		q.setQuery("r100 = load 'MOCCA 600k rbar100/system' using Table();"
//				+ "r55 = load 'MOCCA 600k rbar55/system' using Table();"
//				+ "X = UNION r100, r55;"
//				+ "DESCRIBE r100;"
//				+ "DUMP r100;"
//				+ "DESCRIBE r55;"
//				+ "DUMP r55;"
//				+ "DESCRIBE X;"
//				+ "DUMP X;"
////				+ "X_GROUP = GROUP X ALL;"
////				+ "xc = FOREACH X_GROUP GENERATE COUNT(X);"
////				+ "DUMP xc;"
//				+ "store X into 'plot2/TYPE points "
//				+ "						COLUMNS tphys:smt "
//				+ "						TITLE \"Mass of the cluster\"' "
//				+ "										using Plot();"
//				);
////		q.save();
////		q.runQuery(false);
////		net.hypki.moccabeans.model.plots.Plot.getPlot(q.getId(), "plot2").saveToFile(table, "test-mocca--mass.pdf");
		
		
		q = new PigScript(user, notebook.getId(), "query demo 1");
//		q.setId(new UUID("8dd2f7e4ff254fd98f74aca45b14b1e5"));
		q.setQuery("r100 = load 'MOCCA 600k rbar100/system/tphys,smt' using Table();\n"
				+ "r55 = load 'MOCCA 600k rbar55/system/tphys,smt' using Table();\n"
//				+ "r35 = load 'MOCCA 600k rbar35/system' using Table();\n"
				+ "\n"
//				+ "X = UNION r100, r55, r35;\n"
				+ "X = UNION r100, r55;\n"
				+ "\n"
				+ ""
				+ "store X into 'plot1/TYPE points "
				+ "						COLUMNS tphys:smt "
				+ "						TITLE \"Mass of the clusters\" "
				+ "               		COLOR BY tbid "
				+ "										'using Plot();");
		q.save();
		q.runQuery(false);
		JobsManager.runAllJobs(BeansConst.getAllChannelsNames());
//		((Plot) NotebookEntryFactory.getNotebookEntry(q.getId())).setFilename("test-mocca--mass-multi-rbar.pdf").plot();

		
//		notebook = BeansSearchManager.searchNotebooks("").get(0);
//		q = new Query(user, notebook.getId(), "Parallel coordinates plot");
//		q.setQuery("rows = load 'Harris catalogue/Harris3' using Table();\n" 
//				  + "store rows into 'plot1/TYPE parallel COLUMNS vr:vLSR:c:rc:rh:muV:MV:th:rho0:lgTc:lgTh:massGn:bimod' using Plot();");
////		q.save();
////		q.runQuery(false);
	}
}
