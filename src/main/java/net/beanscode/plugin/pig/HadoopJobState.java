package net.beanscode.plugin.pig;

public enum HadoopJobState {
	KILLED,
	RUNNING,
	FINISHED,
	ACCEPTED
}
