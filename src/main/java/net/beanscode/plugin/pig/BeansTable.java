package net.beanscode.plugin.pig;

import static net.hypki.libs5.utils.sha.SHAManager.getSHA;
import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;
import static net.hypki.libs5.utils.string.StringUtilities.split;
import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;
import static net.hypki.libs5.utils.utils.NumberUtils.toInt;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.lang.NotImplementedException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.InputFormat;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.OutputFormat;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.RecordWriter;
import org.apache.pig.Expression;
import org.apache.pig.LoadFunc;
import org.apache.pig.LoadMetadata;
import org.apache.pig.ResourceSchema;
import org.apache.pig.ResourceSchema.ResourceFieldSchema;
import org.apache.pig.ResourceStatistics;
import org.apache.pig.StoreFuncInterface;
import org.apache.pig.backend.hadoop.executionengine.mapReduceLayer.PigSplit;
import org.apache.pig.data.DataType;
import org.apache.pig.data.Tuple;
import org.apache.pig.data.TupleFactory;

import net.beanscode.model.BeansDbManager;
import net.beanscode.model.BeansSettings;
import net.beanscode.model.cass.Cumulative;
import net.beanscode.model.cass.Function;
import net.beanscode.model.cass.UniTableInputFormat;
import net.beanscode.model.cass.UniTableOutputFormat;
import net.beanscode.model.cass.UniTableRecordReader;
import net.beanscode.model.cass.UniTableRecordWriter;
import net.beanscode.model.connectors.ConnectorList;
import net.beanscode.model.dataset.Dataset;
import net.beanscode.model.dataset.DatasetFactory;
import net.beanscode.model.dataset.Table;
import net.beanscode.model.dataset.TableFactory;
import net.beanscode.model.plugins.ColumnDef;
import net.beanscode.model.plugins.ColumnDefList;
import net.beanscode.model.plugins.Connector;
import net.beanscode.model.settings.Clipboard;
import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.schema.ColumnType;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.search.query.Query;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.SimpleDate;
import net.hypki.libs5.utils.date.Watch;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.sha.SHAManager;
import net.hypki.libs5.utils.string.RegexUtils;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.string.TrickyString;
import net.hypki.libs5.weblibs.settings.SettingFactory;

public class BeansTable extends LoadFunc implements StoreFuncInterface, LoadMetadata {
	
	private static final String VERSION = "2.1";
	    
	protected Configuration confCache;
	private UniTableRecordReader readerCache = null;
	private String loadLocationCache = null;
	private String storeLocationCache = null;
	private ResourceSchema schemaCache = null;
	private Dataset datasetCache = null;
	private String relToAbsPathForStoreLocationLocation = null;
	private String relToAbsPathForStoreLocationCurDir = null;
	private String udfcSignature = null;
	
	private PigScript pigScriptCache = null;
	private Table tableCache = null;
	private Using usingCache = null;
	private Integer tbidIndex = null;
	
	private Connector tableConnector = null;
	
	private Iterator<Row> tableIter = null;
	
	private UUID pigScriptId = null;
	
	/**
	 * False is columns set was not yet written to the output connector. 
	 * True if it was written already. 
	 */
	private boolean tableColumnDefSet = false;
		
	private int currentBeansTableSplit = 0;
	private int allBeansTableSplits = 0;
	private int currentFileSplit = 0;
	private int allFileSplits = 0;
	
	private boolean simplifyColumNames = false;
	
	private static boolean testingDbChecked = true; // do not test DB and search engine
		
	private int functionsNewColumns = -1;
	
	private long debugOneRowWritten = 0;
	
	private UniTableRecordWriter uniTableRecordWriter = null;
	
	static {
		LibsLogger.debug(BeansTable.class, "BeansTable Version ", VERSION);
	}
	
	public BeansTable() {
		LibsLogger.debug(BeansTable.class, "Default Constructor queryId=", pigScriptId, ", ver 2.0");
		
		initDatabaseProvider();
	}
	
	public BeansTable(String pigScriptId) {
		LibsLogger.debug(BeansTable.class, "Constructor queryId=", pigScriptId, ", ver 2.0");
		
		setPigScriptId(new UUID(pigScriptId));
		
		initDatabaseProvider();
	}
	
	private void saveUsing(Using using) {
		try {
			Clipboard.set(using.getPigScriptId() + "-" + getSHA(using.getLocation()), using);
		} catch (IOException e) {
			LibsLogger.error(BeansTable.class, "Cannot save using", e);
		}
	}
	
	private Using getUsing() throws IOException {
		if (usingCache == null) {
			assertTrue(getPigScriptId() != null, "Pig script ID is not specified");
			
			if (loadLocationCache != null)
				usingCache = (Using) Clipboard.get(getPigScriptId() + "-" + getSHA(loadLocationCache));
			else if (storeLocationCache != null)
				usingCache = (Using) Clipboard.get(getPigScriptId() + "-" + getSHA(storeLocationCache));
			else
				throw new NotImplementedException("Store and load locations are not defined, cannot get Using class");
			
			if (usingCache == null) {
				usingCache = new Using();
				usingCache.setPigScriptId(getPigScriptId());
				if (loadLocationCache != null) {
					usingCache.setLocation(loadLocationCache);
				} else {
					usingCache.setLocation(storeLocationCache);
					usingCache.setStore(true);
				}
				saveUsing(usingCache);
			}
		}
		return usingCache;
	}
	
//	private void setUsing(Using using) throws IOException {
//		if (loadLocationCache != null)
//			getPigScript().setUsing(loadLocationCache, using);
//		else if (storeLocationCache != null)
//			getPigScript().setUsing(storeLocationCache, using);
//		else
//			throw new NotImplementedException("Store and load locations are not defined, cannot save Using");
//	}
	
	private void logStatus(String context) throws IOException {
		Using using = null;
		if (storeLocationCache != null || loadLocationCache != null)
			using = getUsing();
		LibsLogger.debug(BeansTable.class, 
				"context=", context, 
//				" hostname=", SystemUtils.getHostname(),
				" storeLocation=", storeLocationCache,
				" loadLocation=", loadLocationCache,
				" schema=", schemaCache,
				" relToAbsPathForStoreLocationLocation=", relToAbsPathForStoreLocationLocation,
				" relToAbsPathForStoreLocationCurDir=", relToAbsPathForStoreLocationCurDir,
				" using=", 	using != null ? "not-null" : "null",
				" dsQuery=", 	using != null ? using.getDatasetQuery() : "", 
				" tabQuery=", 	using != null ? using.getTableQuery() : "",
				" tableId=", using != null ? getTableId() : "null", 
				" queryId=", pigScriptId,
				" table.name=", 	tableCache != null ? tableCache.getName() : "",
				" table.columns=", 	tableCache != null ? tableCache.getColumnDefs() : "null",
				" tableConnector=", tableConnector != null ? tableConnector : "null",
				" tableIter=", 		tableIter != null ? tableIter : "null",
				" pigScript=", 		pigScriptCache != null ? pigScriptCache.getName() : "null",
				" currentBeansTableSplit=", 	currentBeansTableSplit, 
				" allBeansTableSplits=", 		allBeansTableSplits,
				" currentFileSplit=", 	currentFileSplit, 
				" allFileSplits=", 		allFileSplits);
	}
	
	// TODO move this later to other model classes
	private void testSearchEngingeAndDb() {
		if (testingDbChecked)
			return;
		
		if (this.allBeansTableSplits == 0)
			// do nothing, UniTable is not configured yet
			return;
		
		testingDbChecked = true;
		
		try {
			logStatus("testSearchEngingeConn");
			
			if (getPigScript() != null) {
				// testing search engine
				try {
					for (Dataset ds : DatasetFactory.searchDatasets(null, "", 0, 10)) {
						LibsLogger.debug(BeansTable.class, "Dataset from search index: ", ds.getName());
					}
				} catch (Throwable t) {
					LibsLogger.error(BeansTable.class, "Cannot do search", t);
				}
				
				LibsLogger.debug(BeansTable.class, "STARTING table iter");
				int c = 0;
				for (Table tab : TableFactory.iterateTables(getPigScript().getUserId(), "MOCCA", "system")) {
					LibsLogger.debug(BeansTable.class, "Table: ", tab.getName(), " ID=", tab.getId());
					if (++c == 5)
						break;
				}
				LibsLogger.debug(BeansTable.class, "FINISHED table iter");
				
				if (isLoadFunction()) {
					LibsLogger.debug(BeansTable.class, "START rows");
					c = 0;
					for (Row row : getConnector().iterateRows(null)) {
						LibsLogger.debug(BeansTable.class, "Row ", c++, row);
						if (c == 5)
							break;
					}
					LibsLogger.debug(BeansTable.class, "STOP rows");
				}
			}
		} catch (Exception e) {
			LibsLogger.error(BeansTable.class, "Cannot test search index", e);
		}
	}

	@Override
	public InputFormat getInputFormat() throws IOException {
		logStatus("getInputFormat");
		return new UniTableInputFormat();
	}
	
	private PigScript getPigScript() throws IOException {
		if (pigScriptCache == null) {
			assertTrue(getPigScriptId() != null, "QueryId is not defined");
			
			pigScriptCache = PigScript.getQuery(getPigScriptId());
			LibsLogger.debug(BeansTable.class, "PigScript: ", pigScriptCache != null ? pigScriptCache.getName() : null);
		}
		return pigScriptCache;
	}
	
	private Dataset getDataset() throws IOException {
		if (this.datasetCache == null) {
			this.datasetCache = DatasetFactory.getDataset(getPigScript().getNotebook().getId());
			
			if (this.datasetCache == null) {
				try {
					datasetCache = new Dataset(getPigScript().getUserId(), getPigScript().getNotebook().getName());
					datasetCache.setId(getPigScript().getNotebook().getId());
					datasetCache.setDescription("This is automatically generated Dataset from within a Pig script. "
//							+ "It is a read-only Dataset. "
							);
					datasetCache.save();
					
					datasetCache.makeReadOnly();
				} catch (ValidationException e) {
					throw new IOException("Cannot create Dataset for writing", e);
				}
			}
		}
		return this.datasetCache;
	}
		
	private Table getTable() throws IOException {
//		logStatus("getTable1");
		if ((isStoreFunction() == null && isStoreFunctionLikely() == false)
				|| (isStoreFunction() != null && isStoreFunction() == false)) {
//			LibsLogger.debug(UniTable.class, "Assuming load function");
			
			if (this.tableCache == null) {
				
				// it is a load function
				final Using using = getUsing();
				
	//			if (tableMissing)
	//				return null;
				
				assertTrue(using != null
						&& (using.getDatasetQuery() != null || using.getTableQuery() != null), 
						"BeansTable is load function but dataset and table queries are empty");
	//			if (nullOrEmpty(dsQuery) && nullOrEmpty(tabQuery))
	//				return null;
			
				final String cacheKey = using.getDatasetQuery() + "/" + using.getTableQuery();
				String cache = confCache != null ? confCache.get(cacheKey) : null;
				if (StringUtilities.notEmpty(cache)) {
					LibsLogger.debug(BeansTable.class, "Taking Table from cache");
					tableCache = JsonUtils.fromJson(cache, Table.class);
//					table.updateColumnDefs();
				} else {
					LibsLogger.info(BeansTable.class, "Building the list of tables for cacheKey=", cacheKey);
					for (Table tb : iterateTables()) {
						LibsLogger.info(BeansTable.class, "Checking Table 4 ", tb, " for column definitions");
						if (tb.getColumnDefs() != null && tb.getColumnDefs().size() > 0) {
							tableCache = tb;
//							table.updateColumnDefs();
							if (confCache != null)
								confCache.set(cacheKey, JsonUtils.objectToString(tableCache));
							break;
						}
					}
				}
				
				if (tableCache == null) {
					LibsLogger.error(BeansTable.class, "Cannot find a table for cacheKey=" + cacheKey);
//					tableMissing = true;
				}
			}
		} else {
//			LibsLogger.debug(UniTable.class, "Assuming store function");
			
			// this is store function
			if (getTableId() == null)
				return null;
			
			if (this.tableCache == null) {
				this.tableCache = TableFactory.getTable(getTableId());
			}
			
			if (this.tableCache == null) {
				try {
					tableCache = new Table(getDataset(), "__");
					tableCache.setId(getUsing().getTableId());
					tableCache.getMeta().add("queryId", getPigScriptId().getId());
					
					tableCache.save();
					
					tableCache.makeReadOnly();
				} catch (ValidationException e) {
					throw new IOException("Cannot create Table for writing", e);
				}
			}
		}
//		logStatus("getTable2");
		return tableCache;
	}
	
	public Iterable<Table> iterateTables() {
		try {
			final Using using = getUsing();
			LibsLogger.debug(BeansTable.class, "iterateTables using ", using.toString());
			
			if (UUID.isValid(using.getDatasetQuery()) && UUID.isValid(using.getTableQuery())) {
				List<Table> table = new ArrayList<Table>();
				
				Table tableTmp = TableFactory.getTable(new UUID(using.getTableQuery()));
				table.add(tableTmp);
				LibsLogger.debug(BeansTable.class, "Table from DB: ", tableTmp);
				
				return table;
			}
			
			if (getPigScript() == null)
				LibsLogger.error(BeansTable.class, "PigScript is null");
			else if (getPigScript().getUserId() == null)
				LibsLogger.error(BeansTable.class, "PigScript.userId is null");
			
//			LibsLogger.debug(UniTable.class, uniTableId, " iterateTables queryId", getQueryId());
//			LibsLogger.debug(UniTable.class, uniTableId, " iterateTables pigScript", getPigScript());
//			LibsLogger.debug(UniTable.class, uniTableId, " iterateTables dsQuery", dsQuery);
//			LibsLogger.debug(UniTable.class, uniTableId, " iterateTables tabQuery", tabQuery);
			
//			testSearchEngingeConn();
		
//			return BeansSearchManager.iterateTables(getPigScript().getUserId(), dsQuery, tabQuery);
			final UUID userId = getPigScript().getUserId();
			LibsLogger.debug(BeansTable.class, "iterateTables iterating tables for userId= ", userId);
			return new Iterable<Table>() {
				@Override
				public Iterator<Table> iterator() {
					return new Iterator<Table>() {
						private int from = 0;
						
						private int tablesIdx = 0;
						
						private List<Table> tables = null;
						
						@Override
						public void remove() {
							throw new NotImplementedException();
						}
						
						@Override
						public Table next() {
							if (hasNext()) {
								LibsLogger.debug(BeansTable.class, getSplitId(), " getting tablesIdx ", tablesIdx, " from ",
										tables.size(), " tablename ", tables.get(tablesIdx).getName());
								return tables.get(tablesIdx++);
							}
							else
								return null;
						}
						
						@Override
						public boolean hasNext() {
							try {
								if (tables == null || tables.size() == tablesIdx) {
									LibsLogger.debug(BeansTable.class, getSplitId(), " iterateTables iter START userId=", userId, 
											"ds=", using.getDatasetQuery(), "tb=", using.getTableQuery(), "from=", from, "size=", 1000);
									tables = TableFactory.searchTables(userId, using.getDatasetQuery(), using.getTableQuery(), from, 1000).getObjects();
									LibsLogger.debug(BeansTable.class, getSplitId(), " tables.size() ", tables.size());
									tablesIdx = 0;
									from += tables.size();
									LibsLogger.debug(BeansTable.class, getSplitId(), " iterateTables iter STOP userId=", userId, 
											"ds=", using.getDatasetQuery(), "tb=", using.getTableQuery(), "from=", from, "size=", 1000);
								}
								LibsLogger.debug(BeansTable.class, getSplitId(), " hasNext tablesIdx ", tablesIdx, " from ",
										tables.size());
								return tablesIdx < tables.size();
							} catch (IOException e) {
								LibsLogger.error(BeansTable.class, "Cannot get the next page with results", e);
								return false;
							}
						}
					};
				}
			};
		} catch (Throwable e) {
			LibsLogger.error(BeansTable.class, "Cannot iterate over Tables", e);
			return null;
		}
	}
	
//	@Override
//	protected void finalize() throws Throwable {
//		LibsLogger.debug(UniTable.class, "TICK Closing connectors on finalize");
//
//		if (tableConnector != null)
//			getConnector().close();
//	}
	
	private int getNumberofSplits() {
		int nrSplits = SettingFactory.getSettingValueInt(BeansSettings.SETTING_PIG_MODE, "splits", 8);
		
		LibsLogger.debug(BeansTable.class, "Number of all splits ", nrSplits);
		return nrSplits;
	}
	
	private int getNumberofOneBeansTableSplits() {
		int nrOneTableSplits = SettingFactory.getSettingValueInt(BeansSettings.SETTING_PIG_MODE, "oneTableSplits", 8);
		
		LibsLogger.debug(BeansTable.class, "Number of one BeansTable splits ", nrOneTableSplits);
		return nrOneTableSplits;
	}
	
	private Connector getConnector() throws IOException {
		if (tableConnector == null) {
			LibsLogger.debug(BeansTable.class, "getConnector currentBeansTableSplit= ", this.currentBeansTableSplit, 
					" allBeansTableSplits= ", this.allBeansTableSplits,
					" currentFileSplit= ", this.allFileSplits,
					" allFileSplits= ", this.allFileSplits,
					", ", SimpleDate.now().toStringISO());
			if (isLoadFunction()) {
				ConnectorList list = new ConnectorList();
				
				List<Table> allTables = new ArrayList<Table>();
				for (Table table : iterateTables()) {
					if (table != null) {
						LibsLogger.debug(BeansTable.class, "All: ", table.getName());
						allTables.add(table);
					}
				}
				
				int perSplit = allTables.size() / getNumberofSplits();
				perSplit += 1;
				for (int i = currentBeansTableSplit * perSplit, k = 0; i < allTables.size() && k < perSplit; i++, k++) {
					Table table = allTables.get(i);

					if (table.getColumnDefs() == null || table.getColumnDefs().size() == 0)
						LibsLogger.error(BeansTable.class, "Table " + table.getId() + " does not have ColumnDefs specified. Check that table please.");
					
					for (Connector conn : table.iterateConnectors())
						list.addConnector(conn);
					
					LibsLogger.debug(BeansTable.class, "getConnector table ", table.getId().getId(), 
							" BeansTable=", getSplitId(), " ", SimpleDate.now().toStringISO());
				}
				
				tableConnector = list;
				
				testSearchEngingeAndDb();
			} else {
				logStatus("getConnector, store ");
				
//				if (!isStoreTableSplitted()) {
//					tableConnector = BeansSettings.getDefaultConnector(getTableId(), getSplitId());
//				} else {
//					tableConnector = new ConnectorSplitter(getTable().getName(), getDataset());
//					tableConnector.getInitParams().getMeta().add("queryId", getPigScriptId().getId());
//				}
//				
//				logStatus("getConnector, tableConnector set");
//				
//				try {
//					// TODO id
//					new BeansConnector(tableConnector, UUID.random().getId())
//						.save();
//				} catch (ValidationException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
			}
		}
		return tableConnector;
	}
	
	private boolean isStoreTableSplitted() throws IOException {
		if (getTable() != null) {
			return getTable().getName().contains("@");
		}
		return false;
	}
	
	private Query getFilterAsC3Where() throws IOException {
		if (getFilter() != null) {
			return Query.parseQuery(getFilter());// C3Where.parse(getFilter());
		}
		return null;
	}
	
	private Iterator<Row> getTableRowIterator() {
		if (tableIter == null) {
			try {
				tableIter = getConnector()
						.iterateRows(getFilterAsC3Where(), this.currentFileSplit, this.allFileSplits)
						.iterator();
				
				LibsLogger.debug(BeansTable.class, "Iterator opened: ", getConnector());
			} catch (IOException e) {
				LibsLogger.error(BeansTable.class, "Cannot iterate over table", e);
			}
		}
		return tableIter;
	}

	@Override
	public Tuple getNext() throws IOException {
//		LibsLogger.debug(UniTable.class, "Trying to get next...");
		try {
			if (testingDbChecked == false) { // TODO OPTY remove it later
				testSearchEngingeAndDb();
			}
			
			if (getTableRowIterator().hasNext()) {
				Row row = getTableRowIterator().next();
				
				if (tbidIndex == null) {
					tbidIndex = -1;
					int i = 0;
					for (ColumnDef col : getTable().getColumnDefs()) {
						if (col.getName().equals(Table.COLUMN_TBID)) {
							tbidIndex = i;
							break;
						}
						i += 1;
					}
				}
				
				int tupleSize = getTable().getColumnDefs().size();
				
				if (functionsNewColumns == -1) {
					functionsNewColumns = 0;
					if (!getUsing().isFunctionListEmpty())
						for (Function function : getUsing().getFunctions()) {
							if (function instanceof Cumulative) {
								Cumulative cum = (Cumulative) function;
								if (!getTable().containColumn(cum.getDestinationColumn()))
									functionsNewColumns++;
							}
						}
				}
				
				tupleSize += functionsNewColumns;
				
				int i = 0;
				Tuple newTuple = null;
				if (tbidIndex >= 0) {
					newTuple = TupleFactory.getInstance().newTuple(tupleSize);
					
					// rewrite all fields
					for (ColumnDef col : getTable().getColumnDefs()) {
	//					if (i == tbidIndex)
	//						newTuple.set(i++, getTable().getId().getId());
	//					else
							newTuple.set(i++, row.get(col.getName()));
					}
				} else {
					newTuple = TupleFactory.getInstance().newTuple(tupleSize + 1); // no PL
					
					// rewrite tbid
					newTuple.set(0, getTable().getId().getId());
					
					// rewrite all fields
					i = 1;
					for (ColumnDef col : getTable().getColumnDefs()) {
						newTuple.set(i++, row.get(col.getName()));
					}
				}
				
				// adding new columns from functions
				if (!getUsing().isFunctionListEmpty())
					for (Function function : getUsing().getFunctions()) {
						if (function instanceof Cumulative) {
							Cumulative cum = (Cumulative) function;
							cum.run(row);
							newTuple.set(i++, row.get(cum.getDestinationColumn()));
						}
					}
				
				return newTuple;
			}
			
			logStatus("getNext empty");
			return null;
		} catch (Exception e) {
			LibsLogger.error(BeansTable.class, "Cannot get next element", e);
			return null;
		}
	}

	@Override
	public void prepareToRead(RecordReader arg0, PigSplit arg1) throws IOException {
		logStatus("prepareToRead begin");
		
		try {
			this.allBeansTableSplits = arg1.getNumPaths(); // TODO tu zwraca 1, zamiast nrSplits
			this.currentBeansTableSplit = toInt(arg1.getWrappedSplit().getLocations()[0]);
			this.currentFileSplit = toInt(arg1.getWrappedSplit().getLocations()[1]);
			this.allFileSplits = getNumberofOneBeansTableSplits();
			
			logStatus("prepareToRead end");
		} catch (InterruptedException e) {
			throw new RuntimeException("Cannot determine current split, stopping apache pig...");
		}
		
		if (tableIter != null && tableIter.hasNext())
			throw new NotImplementedException("Something is wrong, iterator has some more elements to read");
		
		this.tableIter = null;
		this.tableConnector = null;
		
        this.readerCache = (UniTableRecordReader) arg0;
	}

	@Override
	public void setLocation(String location, Job job) throws IOException {
		try {			
			this.loadLocationCache = location;
			
			assertTrue(getUsing() == null
					|| isStoreFunction() == null 
					|| isStoreFunction() == false, 
					"Something is wrong, this BeansTable is already set as a store function");
			
			logStatus("setLocation");
			setStoreFunction(false);
			
			confCache = job.getConfiguration();
			
			// queryId
			String queryId = job.getConfiguration().get(PigManager.QUERY_ID);
			if (getPigScriptId() == null)
				setPigScriptId(new UUID(queryId));
			
			// validation 
			assertTrue(getPigScriptId() != null, "setLocation: QueryId is not defined");
//			assertTrue(getSplitId() != null, "setLocation: uniTableId is not defined");
			
			// pig script
			LibsLogger.debug(BeansTable.class, "DbObject ", DbObject.getDatabaseProvider().getInitParams());
			assertTrue(getPigScript() != null, "setLocation: pig script is null");
			assertTrue(notEmpty(getPigScript().getName()), "setLocation: pig script is null");
			
			// datasets, tables query
			updateDsAndTabQuery(location);
			
			// validation
			assertTrue(notEmpty(getDsQuery()) || notEmpty(getTabQuery()), "DS or TAB query has to be specified");
			
			testSearchEngingeAndDb();
		} catch (net.hypki.libs5.utils.utils.ValidationException | ValidationException e) {
			throw new IOException("Cannot set location", e);
		}
	}

	private void initDatabaseProvider(/*Job job*/) {
		// it is possible that UniTable is called from Pig grunt client - DbObject
		// has to be initialized
		if (!DbObject.isDatabaseProviderSpecified())
			DbObject.init(BeansDbManager.getMainDatabaseProvider());
		
//		if (!DbObject.isDatabaseProviderSpecified()) {
//			String mainDatabase = null;
//			try {
//				mainDatabase = job.getConfiguration().get(PigManager.DB_MAIN_DATABASE_PROVIDER);
//				DbObject.init((DatabaseProvider) Class.forName(mainDatabase).newInstance());
//			} catch (Exception e) {
//				LibsLogger.error(UniTable.class, "Cannot initiate DbObject class with " + 
//						(mainDatabase != null ? mainDatabase : "") + " provider", e);
//			}
//		}
	}

	private void updateDsAndTabQuery(String location) throws IOException, ValidationException {
		// checking if location path is in the form DATASETS="..." TABLES="..." QUERY="..."
		if (RegexUtils.firstGroup("(datasets[\\s]*=[\\s]*)\"", location, Pattern.CASE_INSENSITIVE) != null
				&& RegexUtils.firstGroup("(tables[\\s]*=[\\s]*)\"", location, Pattern.CASE_INSENSITIVE) != null) {
			String dsQuery = RegexUtils.firstGroup("datasets[\\s]*=[\\s]*\"([^\\\"]*)\"", location, Pattern.CASE_INSENSITIVE);
			String tabQuery = RegexUtils.firstGroup("tables[\\s]*=[\\s]*\"([^\\\"]*)\"", location, Pattern.CASE_INSENSITIVE);
			String filter = RegexUtils.firstGroup("filter[\\s]*=[\\s]*\"([^\\\"]*)\"", location, Pattern.CASE_INSENSITIVE);
			
			setDsQuery(dsQuery);
			setTabQuery(tabQuery);
			setFilter(filter);
			
			String locationCopy = location;
			if (notEmpty(dsQuery))
				locationCopy = RegexUtils.replaceFirstGroup("(datasets[\\s]*=[\\s]*\"[^\\\"]*\")", locationCopy, "");
			if (notEmpty(tabQuery))
				locationCopy = RegexUtils.replaceFirstGroup("(tables[\\s]*=[\\s]*\"[^\\\"]*\")", locationCopy, "");
			if (notEmpty(filter))
				locationCopy = RegexUtils.replaceFirstGroup("(filter[\\s]*=[\\s]*\"[^\\\"]*\")", locationCopy, "");
			
			// parsing for functions
			TrickyString ts = new TrickyString(locationCopy.replaceFirst("beans:///", ""));
			while (notEmpty(locationCopy)) {
				String cmd = ts.checkNextAlphanumericWord();
				
				if (nullOrEmpty(cmd))
					break;
				else if (cmd.equalsIgnoreCase(new Cumulative().getName())) {
					ts.getNextAlphanumericWord();
					ts.getNextPrintableCharacter(); // =
					ts.getNextPrintableCharacter(); // "
					String argsStr = ts.getNextStringUntil("\"", false);
					String[] args = StringUtilities.split(argsStr, ':');
					String[] splitBy = notEmpty(args[2]) ? split(args[2], ';') : null;
					ts.getNextPrintableCharacter();
					
					Cumulative cum = new Cumulative(args[0], args[1], splitBy);
					getUsing()
						.getFunctions()
						.add(cum);
					
					// checking if there are some new functions
//					if (getTable().getColumnDef(cum.getDestinationColumn()) == null) {
//						getUsing().setFunctionsNewColumnsCount(getUsing().getFunctionsNewColumnsCount() + 1);
//					}
				} else {
					LibsLogger.error(BeansTable.class, "Cannot parse further location: " + ts.getLine());
				}
			}
			
			if (getUsing().getFunctions().size() > 0)
				saveUsing(getUsing());
		} else {
			// get two last strings separated by '/'
			List<String> groups = RegexUtils.allGroups("^(.*)/([^/]*)", location, Pattern.CASE_INSENSITIVE);
			int slash = groups.get(0).lastIndexOf('/');
			setDsQuery(groups.get(0).substring(slash >= 0 ? slash + 1 : 0));
			setTabQuery(groups.get(1));
		}
		
		LibsLogger.debug(BeansTable.class, "Ds query set to ", getDsQuery());
		LibsLogger.debug(BeansTable.class, "Tab query set to ", getTabQuery());
		LibsLogger.debug(BeansTable.class, "Filter ", getUsing().getFilter());
	}

	private String getDsQuery() throws IOException {
		return getUsing().getDatasetQuery();
	}

	private void setDsQuery(String dsQuery) throws IOException, ValidationException {
		getUsing()
			.setDatasetQuery(dsQuery);
		saveUsing(getUsing());
	}

	private String getTabQuery() throws IOException {
		return getUsing().getTableQuery();
	}
	
	private void setTabQuery(String tabQuery) throws IOException, ValidationException {
		getUsing()
			.setTableQuery(tabQuery);
		saveUsing(getUsing());
	}

	@Override
	public String[] getPartitionKeys(String arg0, Job arg1) throws IOException {
		logStatus("getPartitionKeys");
		return null;
	}

	@Override
	public ResourceSchema getSchema(String location, Job job) throws IOException {
		Watch w = new Watch();
		try {			
			if (loadLocationCache == null)
				loadLocationCache = location;
			
			final ResourceSchema sch = new ResourceSchema();
			
			String queryId = job.getConfiguration().get(PigManager.QUERY_ID);
			if (getPigScriptId() == null)
				setPigScriptId(new UUID(queryId));
			
			if (getDsQuery() == null && getTabQuery() == null)
				updateDsAndTabQuery(location);
			
			if (getTable() == null) {
				LibsLogger.warn(BeansTable.class, "Table ", getTableId(), " does not exist, no schema");
				return sch;
			}
	
			LibsLogger.trace(BeansTable.class, "Table is  ", getTable());
			
			int columnsCount = getTable().getColumnDefs().size();
			
			boolean containsTbid = getTable().containColumn(Table.COLUMN_TBID);
			
			if (!containsTbid)
				columnsCount++;
			
			if (!getUsing().isFunctionListEmpty())
				for (Function function : getUsing().getFunctions()) {
					if (function instanceof Cumulative) {
						Cumulative cum = (Cumulative) function;
						if (!getTable().containColumn(cum.getDestinationColumn())) {
							columnsCount++;
						}
					}
				}
//			if (getUsing().getFunctionsNewColumnsCount() > 0)
//				columnsCount += getUsing().getFunctionsNewColumnsCount();
			ResourceFieldSchema[] newFields = new ResourceFieldSchema[columnsCount];
			
			ResourceFieldSchema tbidSchema = new ResourceFieldSchema();
	//      AbstractType<?> validator = validators.get(ByteBufferUtil.bytes(cdef.getName()));
			
			int index = 0;
			
			if (!containsTbid) {
				tbidSchema.setName(Table.COLUMN_TBID);
				tbidSchema.setType(DataType.CHARARRAY);
				newFields[0] = tbidSchema;
				
				index = 1;
			}
			
			for (ColumnDef columnDef : getTable().getColumnDefs()) {
				// TODO ad-hoc solution change it 
//				if (columnDef.getName().equalsIgnoreCase(DbObject.COLUMN_PK)
//						|| columnDef.getName().equalsIgnoreCase(Table.COLUMN_TBID)) {
//					if (index > 1)
//						columnDef.setName(columnDef.getName() + "_");
//					else
//						continue;
//				}
				
				newFields[index++] = PigManager.columnToResourceFieldSchema(columnDef);
				LibsLogger.trace(BeansTable.class, "Field ", newFields[index - 1], " added to resource schema");
			}
			
			// new columns from Functions
			if (!getUsing().isFunctionListEmpty())
				for (Function function : getUsing().getFunctions()) {
					if (function instanceof Cumulative) {
						Cumulative cum = (Cumulative) function;					
						if (!getTable().containColumn(cum.getDestinationColumn())) {
							ColumnDef columnsDef = new ColumnDef(cum.getDestinationColumn(), 
									"Cumulative column fo the column " + cum.getSourceColumn(), 
									ColumnType.DOUBLE);
							newFields[index++] = PigManager.columnToResourceFieldSchema(columnsDef);
						}
					}
				}
			
			sch.setFields(newFields);
			
			LibsLogger.debug(BeansTable.class, "ResourceSchema is ", sch);
			
			schemaCache = sch;
			
			return sch;
		} catch (ValidationException e) {
			throw new IOException("Cannot getSchema", e);
		} finally {
			if (w.ms() > 100)
				LibsLogger.warn(BeansTable.class, "Slow, getting schema in " + w.toString());
		}
	}

	@Override
	public ResourceStatistics getStatistics(String arg0, Job arg1) throws IOException {
		LibsLogger.debug(BeansTable.class, " getStatistics ", arg0);
		return null;
	}

	@Override
	public void setPartitionFilter(Expression arg0) throws IOException {
		logStatus("setPartitionFilter");
		LibsLogger.debug(BeansTable.class, arg0);
	}
	
	private String getSplitId() {
//		assertTrue(relToAbsPathForStoreLocationCurDir != null, "relToAbsPathForStoreLocationCurDir is null");
		return this.relToAbsPathForStoreLocationCurDir;
	}
	
	@Override
	public void checkSchema(ResourceSchema schema) throws IOException {
		this.schemaCache = schema;
		
//		getPigScript().getUsing(uniTableId.getId());
		
		// pigScript_uniTableId -> serialized schema
		Clipboard.set(getPigScriptId() + "_" + getSplitId(), schema);
		
		logStatus("checkSchema");
		
		LibsLogger.debug(BeansTable.class, "checkSchema ", schema);

//		boolean saveColumns = false;
//		if (getTable() != null 
//				&& getTable().getColumnDefs().size() == 0) {
//			saveColumns = true;
//		} else if (isStoreFunctionLikely()) {
//			saveColumns = true;
//			
//			setStoreFunction(true);
//			
//			if (getTableId() == null)
//				setTableId(UUID.random());
//		}
//		
//		if (saveColumns) {
//			LibsLogger.debug(UniTable.class, "Saving column ", schema, " to the table ", getTable());
//			for (ResourceFieldSchema field : schema.getFields()) {
//				String fieldName = field.getName();
//				
////				if (isSimplifyColumNames() && fieldName.lastIndexOf("::") > 0)
////					fieldName = fieldName.substring(fieldName.lastIndexOf("::") + 2);
//				
//				ColumnDef col = new ColumnDef(fieldName, field.getDescription(), PigManager.pigTypeToColumnType(field.getType()));
//				
//				if (StringUtilities.nullOrEmpty(col.getName()))
//					col.setName("c" + (getTable().getColumnDefs().size() + 1));
//				
//				getTable().getColumnDefs().add(col);
//			}
//			
//			try {
//				getTable().save();
//			} catch (ValidationException e) {
//				throw new IOException("Cannot save Table with column definitions", e);
//			}
//		}
	}
	
	@Override
	protected void finalize() throws Throwable {
		if (tableConnector != null
				&& debugOneRowWritten > 0) {
//			LocalLogger.debug(getPigScriptId(), "-", getTableId(), " closing in finalize() ", getConnector().getInitParams());
			getConnector().close();
		}
		
		super.finalize();
		
	}

	@Override
	public void cleanupOnFailure(String arg0, Job arg1) throws IOException {
		try {
			LibsLogger.debug(BeansTable.class, "TICK Closing connectors on failure");
	
			if (tableConnector != null)
				getConnector().close();
		
		} catch (Throwable e) {
			throw new IOException("Cannot close connectors", e);
		}
	}

	@Override
	public void cleanupOnSuccess(String location, Job arg1) throws IOException {
		try {
			if (this.storeLocationCache == null)
				this.storeLocationCache = location;
			
			LibsLogger.debug(BeansTable.class, "TICK Closing connectors on success");

			if (tableConnector != null)
				getConnector().close();

		} catch (Throwable e) {
			throw new IOException("Cannot close connectors", e);
		}
		
//		try {
//			if (isStoreTableSplitted()
//					|| getTable().getName().equals("__"))
//				getTable().remove();
//		} catch (Exception e) {
//			LibsLogger.error(BeansTable.class, "Cannot remove __ Table from splitted Table", e);
//		}
	}

	@Override
	public OutputFormat getOutputFormat() throws IOException {
		ColumnDefList columnDefs = (ColumnDefList) Clipboard.get(getPigScriptId() + "_" + getSHA(getUsing().getLocation()));
		
		return new UniTableOutputFormat(getTableId(), udfcSignature, columnDefs);
	}

	@Override
	public void prepareToWrite(RecordWriter writer) throws IOException {
		this.uniTableRecordWriter = (UniTableRecordWriter) writer;
		
		ColumnDefList columnDefs = (ColumnDefList) Clipboard.get(getPigScriptId() + "_" + getSHA(getUsing().getLocation()));
		this.uniTableRecordWriter.setColumnsDef(columnDefs);
		
		LibsLogger.debug(BeansTable.class, "prepareToWrite ", writer.toString());
	}

	@Override
	public void putNext(Tuple arg0) throws IOException {
		try {
			ColumnDefList defsList = null;
			
			// TODO maybe here always check if recordWriter has columns definitions, if it does not maybe save them here?
					
			if (getTable() == null
					|| getTable().getColumnDefs() == null
					|| getTable().getColumnDefs().size() == 0) {
				logStatus("putNext, columns empty");
				
				try {
					defsList = (ColumnDefList) Clipboard.get(getPigScriptId() + "_" + getSHA(storeLocationCache));
					
					if (defsList == null)
						throw new IOException("ColumnDefs empty, there is nothing in Clipboard");
					
					uniTableRecordWriter.setColumnsDef(defsList);
					
//				getTable().setColumnDefs(defsList);
//				getTable().save();
				} catch (Throwable e) {
					throw new IOException("Cannot save ColumnDefs for table", e);
				}
				
//			// double check
//			if (getTable() == null
//					|| getTable().getColumnDefs() == null
//					|| getTable().getColumnDefs().size() == 0)
//				throw new IOException("ColumnDefs for table still empty");
			}
			
//		if (getTable().getColumnDefs().size() != arg0.size()) {
//			LibsLogger.debug(BeansTable.class, "getTable().getColumnDefs().size() != arg0.size()");
//			LibsLogger.debug(BeansTable.class, getTable().getColumnDefs());
//			LibsLogger.debug(BeansTable.class, arg0);
//		}
			
			if (defsList == null) {
				defsList = getTable().getColumnDefs();
				
				if (defsList == null)
					defsList = (ColumnDefList) Clipboard.get(getPigScriptId() + "_" + getSHA(storeLocationCache));
				
				if (defsList == null)
					throw new IOException("ColumnDefs empty, there is nothing in Table, and in Clipboard");
			}
			
			Row row = new Row(UUID.random().getId());
//		int i = 0;

			// adding tbid if needed
			boolean tbidAdded = false;
			if (arg0.size() < defsList.size()
					&& defsList.get(0).getName().equals(Table.COLUMN_TBID)) {
				row.addColumn(Table.COLUMN_TBID, getTable().getId().getId());
				tbidAdded = true;
			}
			
			for (int i = 0 + (tbidAdded ? 1 : 0), j = 0; i < defsList.size(); i++, j++) {
				ColumnDef columnDef = defsList.get(i);
//		}
			
//		for (ColumnDef columnDef : defsList) {
//			if (tbidAdded && i == 0 && row.size() > 0)
//				continue;
				row.addColumn(columnDef.getName(), arg0.get(j));
//			i++;
			}
			
//		if (!tableColumnDefSet) {
//			tableColumnDefSet = true;
//			getConnector().setColumnDefs(getTable().getColumnDefs());
//		}

//		getConnector().write(row);
			try {
				uniTableRecordWriter.write(row, null);
			} catch (InterruptedException e) {
				LibsLogger.error(BeansTable.class, "Cannot write Row to RecordWriter", e);
			}
			
			if (debugOneRowWritten++ == 0) {
				// first row is written
//			LocalLogger.debug(" one-row-written ",
//					", pigId= ", getPigScriptId(), 
//					", tableId= ", getTableId()//, 
////					", connector= ", uniTableRecordWriter. getConnector().getInitParams()
//					);
			}
			
			if (row == null)
				LibsLogger.debug(BeansTable.class, "TICK writing empty row");
		} catch (Exception e) {
			LibsLogger.error(BeansTable.class, "Cannot put next element", e);
		}
	}

	@Override
	public String relToAbsPathForStoreLocation(String location, Path curDir) throws IOException {
		this.relToAbsPathForStoreLocationLocation = location;
		this.relToAbsPathForStoreLocationCurDir = curDir.toString();
		LibsLogger.debug(BeansTable.class, "relToAbsPathForStoreLocation: ", location, " ", curDir);
		return location;
	}

	@Override
	public void setStoreFuncUDFContextSignature(String signature) {
		this.udfcSignature = signature;
				
		LibsLogger.debug(BeansTable.class, "setStoreFuncUDFContextSignature: ", signature);
	}

	@Override
	public void setStoreLocation(String location, Job job) throws IOException {
		try {
			this.storeLocationCache = location;
			
			assertTrue(isStoreFunction() == null || isStoreFunction() == true, 
					"Something is wrong, this BeansTable is already a LoadFunction");
			
			setStoreFunction(true);
			logStatus("setStoreLocation");
			
			String queryId = job.getConfiguration().get(PigManager.QUERY_ID);
			
			if (getPigScriptId() == null)
				setPigScriptId(new UUID(queryId));
			
			if (getTableId() == null)
				setTableId(getUsing().getTableId());
			
			if (this.schemaCache != null) {
				LibsLogger.debug(BeansTable.class, "Saving column ", schemaCache, " to the table ", getTable());
				ColumnDefList columnDefs = new ColumnDefList();
				for (ResourceFieldSchema field : schemaCache.getFields()) {
					String fieldName = field.getName();
					
//				if (isSimplifyColumNames() && fieldName.lastIndexOf("::") > 0)
//					fieldName = fieldName.substring(fieldName.lastIndexOf("::") + 2);
					
					ColumnDef col = new ColumnDef(fieldName, field.getDescription(), PigManager.pigTypeToColumnType(field.getType()));
					
					if (StringUtilities.nullOrEmpty(col.getName()))
						col.setName("c" + (getTable().getColumnDefs().size() + 1));
					
//					getTable().getColumnDefs().add(col);
					columnDefs.add(col);
				}
				
//				try {
					Clipboard.set(getPigScriptId() + "_" + getSHA(storeLocationCache), columnDefs);
					
//					getTable().setColumnDefs(columnDefs);
//					getTable().save();
//				} catch (ValidationException e) {
//					throw new IOException("Cannot save Table with column definitions", e);
//				}
			}
					
			if (getTable().getName().equals("__")) {
				try {
					final String tableName = RegexUtils.firstGroup("name[\\s=]*['\"](.*)['\"]", location, Pattern.CASE_INSENSITIVE);
					
					// name
					getTable().setName(tableName);
					getTable().save();
					
					// simplify column names?
					String locationWoName = 
							location.replace(RegexUtils.firstGroup("(name[\\s=]*['\"].*['\"])", location, Pattern.CASE_INSENSITIVE), "");
					if (locationWoName.toLowerCase().matches(".*simplify.*"))
						setSimplifyColumNames(true);
					if (isSimplifyColumNames()) {
						for (ColumnDef col : getTable().getColumnDefs()) {
							int idx = col.getName().lastIndexOf("::");
							if (idx >= 0)
								col.setName(col.getName().substring(idx + 2));
						}
					}
					
					if (!isStoreTableSplitted()) {
						getTable().save();
					} else {
//					getTable().remove();
					}
				} catch (ValidationException e) {
					throw new IOException("Cannot update the name of the Table to " + getTable().getName(), e);
				}
			}
			
			LibsLogger.debug(BeansTable.class, "setStoreLocation, location ", location, ", QUERY_ID ", queryId);
		} catch (net.hypki.libs5.utils.utils.ValidationException | ValidationException e) {
			throw new IOException("Cannot set store location", e);
		}
	}

	public UUID getPigScriptId() throws IOException {
		if (pigScriptId == null && confCache != null) {
			String queryIdConf = confCache.get(PigManager.QUERY_ID);
			if (pigScriptId == null)
				pigScriptId = new UUID(queryIdConf);
		}
		
		LibsLogger.debug(BeansTable.class, "PigScriptId ", pigScriptId);
		
		return pigScriptId;
	}

	public void setPigScriptId(UUID pigScriptId) {
		this.pigScriptId = pigScriptId;
		
		if (this.confCache != null)
			this.confCache.set(PigManager.QUERY_ID, pigScriptId.getId());
	}

	private UUID getTableId() throws IOException {
		return getUsing().getTableId();
	}

	private void setTableId(UUID tableId) throws net.hypki.libs5.utils.utils.ValidationException, IOException, ValidationException {
		assertTrue(getUsing().getTableId() == null, "TableId is already set");
		
		getUsing().setTableId(tableId);
		saveUsing(getUsing());
	}

//	private String getTableNameStore() {
//		return tableNameStore;
//	}
//
//	private void setTableNameStore(String tableNameStore) {
//		this.tableNameStore = tableNameStore;
//	}

	private Boolean isStoreFunction() throws IOException {
		return getUsing().isStoreFunction();
	}
	
	private boolean isStoreFunctionLikely() throws IOException {
		return getUsing().isStoreFunctionLikely();
	}
	
	private Boolean isLoadFunction() throws IOException {
		return !getUsing().isStoreFunction();
	}

	private void setStoreFunction(boolean isStoreFunction) throws IOException, ValidationException {
//		this.isStoreFunction = isStoreFunction;
		getUsing()
			.setStore(isStoreFunction);
		saveUsing(getUsing());
	}

	private boolean isSimplifyColumNames() {
		return simplifyColumNames;
	}

	private void setSimplifyColumNames(boolean simplifyColumNames) {
		this.simplifyColumNames = simplifyColumNames;
	}

	private String getFilter() throws IOException {
		return getUsing().getFilter();
	}

	private void setFilter(String filter) throws IOException, ValidationException {
		getUsing()
			.setFilter(filter);
		saveUsing(getUsing());
	}
}
