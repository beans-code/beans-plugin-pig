package net.beanscode.plugin.pig.udfs;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.hypki.libs5.weblibs.CacheManager;

import org.apache.pig.EvalFunc;
import org.apache.pig.data.Tuple;

public class CacheContains extends EvalFunc<Integer> {

	public static long allMs = 0;
	
	private List listCache = null;

	public CacheContains() {
		
	}

	@Override
	public Integer exec(Tuple input) throws IOException {
		long start = System.currentTimeMillis();
		
		String name = input.get(0).toString();
//		Object value = input.get(1);
		
		if (listCache == null)
			listCache = CacheManager
				.cacheInstance()
				.get(List.class, CacheAppend.class.getSimpleName(), name);
		
		if (listCache == null) {
//			CacheManager
//				.cacheInstance()
//				.put(CacheContains.class.getSimpleName(), name, new ArrayList());
			allMs += System.currentTimeMillis() - start;
			return 0;
		}
		
		for (int i = 1; i < input.size(); i++) {
			if (listCache.contains(input.get(i))) {
				allMs += System.currentTimeMillis() - start;
				return 1;
			}
		}
		
		allMs += System.currentTimeMillis() - start;
		return 0;
	}
}
