package net.beanscode.plugin.pig.udfs;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.beanscode.model.settings.Clipboard;
import net.hypki.libs5.weblibs.CacheManager;

import org.apache.pig.EvalFunc;
import org.apache.pig.data.Tuple;

public class GetInt extends EvalFunc<Integer> {

	public GetInt() {
		
	}

	@Override
	public Integer exec(Tuple input) throws IOException {
		String name = input.get(0).toString();
		Integer defaultValue = (Integer) input.get(1);
		
		Integer v = CacheManager
			.cacheInstance()
			.get(Integer.class, SetInt.class.getSimpleName(), name);
		
		if (v != null)
			return v;
		
		// getting from Clipboard
		Object o = Clipboard.get(name);
		
		if (o != null) {
			CacheManager
				.cacheInstance()
				.put(SetInt.class.getSimpleName(), name, (Integer) o);
			return (Integer) o;
		}
		
		return defaultValue;
	}
}
