package net.beanscode.plugin.pig.udfs;

import static net.hypki.libs5.utils.collections.ArrayUtils.toList;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;

import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.collections.ArrayUtils;
import net.hypki.libs5.utils.collections.CollectionUtils;

import org.apache.pig.EvalFunc;
import org.apache.pig.data.Tuple;
import org.apache.pig.data.TupleFactory;

public class Floor extends EvalFunc<Double> {

	public Floor() {

	}

	@Override
	public Double exec(Tuple input) throws IOException {
		if (input == null || input.size() == 0)
			return null;
		if (input.size() == 2) {
			double v = toDouble(input.get(0));
			double base = toDouble(input.get(1));
			return v - (v - ((int) (v / base) * base));
		} else if (input.size() == 3) {
			double v = toDouble(input.get(0));
			double base = toDouble(input.get(1));
			double start = toDouble(input.get(2));
			return v - (v - ((int) ((v - start) / base) * base));
		} else
			throw new IOException("Floor expects 2 arguments: (value, base) OR 3 arguments (value, base, start)");
	}
	
	private double toDouble(Object obj) {
		if (obj instanceof Double) {
			return (double) obj;
		} else if (obj instanceof Long) {
			return ((Long) obj).doubleValue();
		} else if (obj instanceof Integer) {
			return ((Integer) obj).doubleValue();
		} else if (obj instanceof BigInteger) {
			return ((BigInteger) obj).doubleValue();
		} else if (obj instanceof BigDecimal) {
			return ((BigDecimal) obj).doubleValue();
		} else 
			throw new org.apache.commons.lang.NotImplementedException("FLOOR toDouble() not implemented for " + obj);
	}
	
	public static void main(String[] args) throws IOException {
		LibsLogger.info("v=210,base=200.0,start=0.0 -> " 
					+ new Floor().exec(TupleFactory.getInstance().newTuple(toList(new Object[]{210.0, 200.0}))));
		LibsLogger.info("v=10,base=200.0,start=0.0 -> " 
					+ new Floor().exec(TupleFactory.getInstance().newTuple(toList(new Object[]{10.0, 200.0}))));
		LibsLogger.info("v=210,base=200.0,start=5.0 -> " 
					+ new Floor().exec(TupleFactory.getInstance().newTuple(toList(new Object[]{210.0, 200.0, 5.0}))));
		LibsLogger.info("v=204,base=200.0,start=5.0 -> " 
					+ new Floor().exec(TupleFactory.getInstance().newTuple(toList(new Object[]{204.0, 200.0, 5.0}))));
		LibsLogger.info("v=4,base=200.0,start=5.0 -> " 
				+ new Floor().exec(TupleFactory.getInstance().newTuple(toList(new Object[]{4.0, 200.0, 5.0}))));
		LibsLogger.info("v=5,base=200.0,start=5.0 -> " 
				+ new Floor().exec(TupleFactory.getInstance().newTuple(toList(new Object[]{5.0, 200.0, 5.0}))));
		LibsLogger.info("v=6,base=200.0,start=5.0 -> " 
				+ new Floor().exec(TupleFactory.getInstance().newTuple(toList(new Object[]{6.0, 200.0, 5.0}))));
		LibsLogger.info("v=404,base=200.0,start=5.0 -> " 
				+ new Floor().exec(TupleFactory.getInstance().newTuple(toList(new Object[]{404.0, 200.0, 5.0}))));
		LibsLogger.info("v=406,base=200.0,start=5.0 -> " 
				+ new Floor().exec(TupleFactory.getInstance().newTuple(toList(new Object[]{406.0, 200.0, 5.0}))));
	}
}