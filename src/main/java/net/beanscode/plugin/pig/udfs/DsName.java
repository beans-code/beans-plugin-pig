package net.beanscode.plugin.pig.udfs;

import java.io.IOException;

import net.beanscode.model.dataset.Dataset;
import net.beanscode.model.dataset.DatasetFactory;
import net.beanscode.model.dataset.Table;
import net.beanscode.model.dataset.TableFactory;
import net.hypki.libs5.weblibs.CacheManager;

import org.apache.pig.EvalFunc;
import org.apache.pig.data.Tuple;

public class DsName extends EvalFunc<String> {
	
	public DsName() {

	}

	@Override
	public String exec(Tuple input) throws IOException {
		if (input == null || input.size() == 0 || input.size() != 1)
			return null;
		
		String tmp = CacheManager.cacheInstance().get(String.class, "tbIdToDsName", input.get(0).toString());
		
		if (tmp != null)
			return tmp;
		
		Table table = TableFactory.getTable(new net.hypki.libs5.db.db.weblibs.utils.UUID(input.get(0).toString()));
		Dataset ds = table != null ? DatasetFactory.getDataset(table.getDatasetId()) : null;
		
		if (ds == null)
			// maybe it is dsid, not tbid
			ds = DatasetFactory.getDataset(new net.hypki.libs5.db.db.weblibs.utils.UUID(input.get(0).toString()));
		
		if (ds != null) {
			CacheManager.cacheInstance().put("tbIdToDsName", input.get(0).toString(), ds.getName());
			return ds.getName();
		} else {
			CacheManager.cacheInstance().put("tbIdToDsName", input.get(0).toString(), null);
			return null;
		}
	}
}
