package net.beanscode.plugin.pig.udfs;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import net.hypki.libs5.utils.LibsLogger;

import org.apache.pig.EvalFunc;
import org.apache.pig.data.DataType;
import org.apache.pig.data.Tuple;
import org.apache.pig.data.TupleFactory;
import org.apache.pig.impl.logicalLayer.FrontendException;
import org.apache.pig.impl.logicalLayer.schema.Schema;
import org.apache.pig.impl.logicalLayer.schema.Schema.FieldSchema;

public class SimplifySchema extends EvalFunc<Tuple> {
	
//	private String prefix = null;
	
//	private List<Integer> stay = null;
	
	public SimplifySchema() {

	}

	@Override
	public Tuple exec(Tuple input) throws IOException {
		return input;
//		if (prefix == null)
//			prefix = input.get(0).toString();
		
//		HashSet<String> names = new HashSet<String>();
//		for (FieldSchema field : getInputSchema().getFields()) {
//			String name = field.alias;
//			int semiIndex = name.lastIndexOf("::");
//			name = name.substring(semiIndex >= 0 ? semiIndex + 2 : 0);
//			if (!names.contains(name)) {
//				tupleSchema.add(new Schema.FieldSchema(name, field.type));
//				names.add(name);
//				stay.add(i);
//			}
//			i++;
//		}
//		
//		final Tuple newTuple = TupleFactory.getInstance().newTuple();
//		
//		int i = 0;
//		for (Integer rewrite : stay) {			
//			newTuple.set(i++, input.get(rewrite));
//		}		
//		
//		return newTuple;
	}
	
	@Override
	public Schema outputSchema(Schema input) {
        try {
			Schema tupleSchema = new Schema();
//			stay = new ArrayList<Integer>();
			HashSet<String> names = new HashSet<String>();
			
			// TODO removing duplicates?
//			int i = 0;
			for (FieldSchema field : input.getFields()) {
				String name = field.alias;
				int semiIndex = name.lastIndexOf("::");
				name = name.substring(semiIndex >= 0 ? semiIndex + 2 : 0);
				if (!names.contains(name)) {
					tupleSchema.add(new Schema.FieldSchema(name, field.type));
					names.add(name);
//					stay.add(i);
				} else {
					tupleSchema.add(field); // names is duplicated -> no changes
				}
//				i++;
			}
			
//			return new Schema(tupleSchema);
			
//			return new Schema(tupleSchema.getFields());
			
			return new Schema(new Schema.FieldSchema(
					"ss",
					tupleSchema,
					DataType.TUPLE
					));
		} catch (FrontendException e) {
			LibsLogger.error(SimplifySchema.class, "Cannot create Schema for HistoryLine", e);
			return null;
		}
	}
}
