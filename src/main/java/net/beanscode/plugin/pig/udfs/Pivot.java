package net.beanscode.plugin.pig.udfs;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.utils.AssertUtils;

import org.apache.pig.Accumulator;
import org.apache.pig.EvalFunc;
import org.apache.pig.data.BagFactory;
import org.apache.pig.data.DataBag;
import org.apache.pig.data.DataType;
import org.apache.pig.data.DefaultDataBag;
import org.apache.pig.data.Tuple;
import org.apache.pig.data.TupleFactory;
import org.apache.pig.impl.logicalLayer.FrontendException;
import org.apache.pig.impl.logicalLayer.schema.Schema;
import org.apache.pig.impl.logicalLayer.schema.Schema.FieldSchema;
import org.javatuples.Pair;

public class Pivot extends EvalFunc<Tuple> {
		
	public Pivot() {
		
	}
	
	@Override
	public Tuple exec(Tuple tuple) throws IOException {
//		DataBag b = BagFactory.getInstance().newDefaultBag();
		
		final DataBag inputBag = (DataBag) tuple.get(0);
		final Tuple newTuple = TupleFactory.getInstance().newTuple((int) inputBag.size());
		
		int i = 0;
		Iterator<Tuple> it = inputBag.iterator();
    	while (it.hasNext()) {
			Tuple t = (Tuple) it.next();
			if (t != null && t.size() != 0 && t.get(0) != null) {
								
//				b.add(t);
				newTuple.set(i++, t);
			}
		}
		
		return newTuple;
	};
	
}
