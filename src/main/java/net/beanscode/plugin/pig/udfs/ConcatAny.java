package net.beanscode.plugin.pig.udfs;

import java.io.IOException;
import java.util.Iterator;

import org.apache.pig.EvalFunc;
import org.apache.pig.data.DataBag;
import org.apache.pig.data.Tuple;

public class ConcatAny extends EvalFunc<String> {
		
	public ConcatAny() {
		
	}
	
	@Override
	public String exec(Tuple tuple) throws IOException {
//		DataBag b = BagFactory.getInstance().newDefaultBag();
		
		String ret = "";
		
		for (Object o : tuple) {
			ret += String.valueOf(o);
		}
		
		return ret;
	};
	
}
