package net.beanscode.plugin.pig.udfs;

import java.io.IOException;

import net.beanscode.model.dataset.Dataset;
import net.beanscode.model.dataset.DatasetFactory;
import net.hypki.libs5.cache.CacheProvider;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.string.Meta;
import net.hypki.libs5.utils.string.MetaList;
import net.hypki.libs5.weblibs.CacheManager;

import org.apache.pig.EvalFunc;
import org.apache.pig.data.Tuple;

public class DsParamDouble extends EvalFunc<Double> {
	
	public DsParamDouble() {

	}

	@Override
	public Double exec(Tuple input) throws IOException {
		if (input == null || input.size() == 0 || input.size() != 2)
			return 0.0;
		
		try {
			String dsId = input.get(0).toString();
			String param = input.get(1).toString();
			
			MetaList metaList = null;//CacheManager.cacheInstance().get(MetaList.class, MetaList.class.getName(), dsId);
			
//			if (metaList == null) {
			
				Dataset ds = DatasetFactory.getDataset(dsId); 
			
				if (ds == null)
					return null;
				
				metaList = ds.getMeta();
				
//				CacheManager.cacheInstance().put(MetaList.class.getName(), dsId, metaList);
//			}
			
			Meta meta = metaList.get(param);
			return meta != null ? meta.getAsDouble() : null;
		} catch (Exception e) {
			LibsLogger.error(DsParamDouble.class, "Cannot read Dataset param for a tuple ", input);
			return null;
		}
	}
}
