package net.beanscode.plugin.pig.udfs;

import java.io.IOException;

import net.beanscode.model.BeansDbManager;
import net.beanscode.model.dataset.Table;
import net.beanscode.model.dataset.TableFactory;
import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.weblibs.CacheManager;

import org.apache.pig.EvalFunc;
import org.apache.pig.data.Tuple;

public class DsId extends EvalFunc<String> {
	
	public DsId() {
		// TODO move it to TableFactory or something
		if (!DbObject.isDatabaseProviderSpecified()) {
			LibsLogger.info(DsId.class, "Initializing DbObject in UDF...");
			DbObject.init(BeansDbManager.getMainDatabaseProvider());
		}
	}

	@Override
	public String exec(Tuple input) throws IOException {
		if (input == null 
				|| input.size() == 0 
				|| input.size() != 1 
				|| input.get(0) == null
				|| input.get(0).toString().length() == 0)
			return null;
		
		String tmp = CacheManager.cacheInstance().get(String.class, "tbIdToDsId", input.get(0).toString());
		
		if (tmp == null) {
			Table table = TableFactory.getTable(new net.hypki.libs5.db.db.weblibs.utils.UUID(input.get(0).toString()));
			if (table != null) {
				CacheManager.cacheInstance().put("tbIdToDsId", input.get(0).toString(), table.getDatasetId().getId());
				return table.getDatasetId().getId();
			} else {
				CacheManager.cacheInstance().put("tbIdToDsId", input.get(0).toString(), null);
				return null;
			}
		} else
			return tmp;
	}
}
