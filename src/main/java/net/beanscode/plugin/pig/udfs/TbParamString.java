package net.beanscode.plugin.pig.udfs;

import java.io.IOException;

import net.beanscode.model.dataset.Table;
import net.beanscode.model.dataset.TableFactory;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.string.Meta;
import net.hypki.libs5.utils.string.MetaList;
import net.hypki.libs5.weblibs.CacheManager;

import org.apache.pig.EvalFunc;
import org.apache.pig.data.Tuple;

public class TbParamString extends EvalFunc<String> {
	
	public TbParamString() {

	}

	@Override
	public String exec(Tuple input) throws IOException {
		if (input == null || input.size() == 0 || input.size() != 2)
			return null;
		
		try {
			String tbId = input.get(0).toString();
			String param = input.get(1).toString();
			
			MetaList metaList = CacheManager.cacheInstance().get(MetaList.class, MetaList.class.getName(), tbId);
			
			if (metaList == null) {
			
				Table tb = TableFactory.getTable(new net.hypki.libs5.db.db.weblibs.utils.UUID(tbId)); 
				
				if (tb == null)
					return null;
				
				metaList = tb.getMeta();
				
				CacheManager.cacheInstance().put(MetaList.class.getName(), tbId, metaList);
			}
				
			Meta meta = metaList.get(param);
			String v = meta != null ? meta.getAsString() : null;
			return v;
		} catch (Exception e) {
			LibsLogger.error(TbParamString.class, "Cannot read Table param for a tuple ", input);
			return null;
		}
	}
}
