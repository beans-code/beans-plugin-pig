package net.beanscode.plugin.pig.udfs;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.beanscode.model.settings.Clipboard;
import net.hypki.libs5.weblibs.CacheManager;

import org.apache.pig.EvalFunc;
import org.apache.pig.data.Tuple;

public class GetString extends EvalFunc<String> {

	public GetString() {
		
	}

	@Override
	public String exec(Tuple input) throws IOException {
		String name = input.get(0).toString();
		String defaultValue = (String) input.get(1);
		
		String v = CacheManager
			.cacheInstance()
			.get(String.class, SetString.class.getSimpleName(), name);
		
		if (v != null)
			return v;
		
		// getting from Clipboard
		Object o = Clipboard.get(name);
		
		if (o != null) {
			CacheManager
				.cacheInstance()
				.put(SetString.class.getSimpleName(), name, (String) o);
			return (String) o;
		}
		
		return defaultValue;
	}
}
