package net.beanscode.plugin.pig.udfs;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.hypki.libs5.weblibs.CacheManager;

import org.apache.pig.EvalFunc;
import org.apache.pig.data.Tuple;

public class CacheAppend extends EvalFunc<Integer> {

	public CacheAppend() {
		
	}

	@Override
	public Integer exec(Tuple input) throws IOException {
		String name = input.get(0).toString();
		Object value = input.get(1);
		
		List list = CacheManager
			.cacheInstance()
			.get(List.class, CacheAppend.class.getSimpleName(), name);
		
		if (list == null)
			list = new ArrayList();
		
		if (list.contains(value))
			return 0;
		
		boolean ret = list.add(value);
		
		CacheManager
			.cacheInstance()
			.put(CacheAppend.class.getSimpleName(), name, list);
		
		return ret ? 1 : 0;
	}
}
