package net.beanscode.plugin.pig.udfs;

import java.io.IOException;

import org.apache.pig.EvalFunc;
import org.apache.pig.data.Tuple;

public class Histogram extends EvalFunc<Integer> {

	public Histogram() {

	}

	@Override
	public Integer exec(Tuple input) throws IOException {
		if (input == null || input.size() == 0)
			return null;

		if (input.size() == 2) {
//				double min = toDouble(input.get(0));
//				double max = toDouble(input.get(1));
			double step = toDouble(input.get(0));
			double v = toDouble(input.get(1));
			int binNumber = (int) Math.floor(v / step);
			return binNumber;
		} else
			throw new IOException("Histogram expects 2 arguments: (step, value)");
	}
	
	private double toDouble(Object obj) {
		if (obj instanceof Double) {
			return (double) obj;
		} else if (obj instanceof Long) {
			return ((Long) obj).doubleValue();
		} else 
			throw new org.apache.commons.lang.NotImplementedException();
	}
}
