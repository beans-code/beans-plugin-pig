package net.beanscode.plugin.pig.udfs;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.hypki.libs5.weblibs.CacheManager;

import org.apache.pig.EvalFunc;
import org.apache.pig.data.Tuple;

public class CacheClear extends EvalFunc<Integer> {

	public CacheClear() {
		
	}

	@Override
	public Integer exec(Tuple input) throws IOException {
		String name = input.get(0).toString();
		
		CacheManager
			.cacheInstance()
			.put(CacheClear.class.getSimpleName(), name, null);
		
		return 1;
	}
}
