package net.beanscode.plugin.pig.udfs;

import java.io.IOException;

import net.hypki.libs5.utils.utils.NumberUtils;

import org.apache.pig.EvalFunc;
import org.apache.pig.data.Tuple;

public class ToDouble extends EvalFunc<Double> {

	public ToDouble() {

	}

	@Override
	public Double exec(Tuple input) throws IOException {
		if (input == null || input.size() == 0)
			return 0.0;
		
		return toDouble(input.get(0));
	}
	
	private double toDouble(Object obj) {
		if (obj instanceof Double) {
			return (double) obj;
		} else if (obj instanceof Long) {
			return ((Long) obj).doubleValue();
		} else if (obj instanceof Integer) {
			return ((Integer) obj).doubleValue();
		} else if (obj instanceof String) {
			return NumberUtils.toDouble((String) obj);
		} else 
			throw new org.apache.commons.lang.NotImplementedException();
	}
}
