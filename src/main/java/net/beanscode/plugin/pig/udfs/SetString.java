package net.beanscode.plugin.pig.udfs;

import java.io.IOException;

import net.beanscode.model.settings.Clipboard;
import net.hypki.libs5.weblibs.CacheManager;

import org.apache.pig.EvalFunc;
import org.apache.pig.data.Tuple;

public class SetString extends EvalFunc<String> {

	public SetString() {
		
	}

	@Override
	public String exec(Tuple input) throws IOException {
		String name = input.get(0).toString();
		String value = (String) input.get(1);
		
		Clipboard.set(name, value);
		
		CacheManager
			.cacheInstance()
			.put(SetString.class.getSimpleName(), name, value);
		
		return value;
	}
}
