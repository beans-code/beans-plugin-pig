package net.beanscode.plugin.pig.udfs;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.beanscode.model.settings.Clipboard;
import net.hypki.libs5.weblibs.CacheManager;

import org.apache.pig.EvalFunc;
import org.apache.pig.data.Tuple;

public class GetDouble extends EvalFunc<Double> {

	public GetDouble() {
		
	}

	@Override
	public Double exec(Tuple input) throws IOException {
		String name = input.get(0).toString();
		Double defaultValue = (Double) input.get(1);
		
		Double v = CacheManager
			.cacheInstance()
			.get(Double.class, SetDouble.class.getSimpleName(), name);
		
		if (v != null)
			return v;
		
		// getting from Clipboard
		Object o = Clipboard.get(name);
		
		if (o != null) {
			CacheManager
				.cacheInstance()
				.put(SetDouble.class.getSimpleName(), name, (Double) o);
			return (Double) o;
		}
		
		return defaultValue;
	}
}
