package net.beanscode.plugin.pig.udfs;

import java.io.IOException;
import java.math.BigDecimal;

import org.apache.pig.EvalFunc;
import org.apache.pig.data.Tuple;

public class Pow extends EvalFunc<Double> {

	public Pow() {

	}

	@Override
	public Double exec(Tuple input) throws IOException {
		if (input == null || input.size() == 0)
			return null;
		if (input.size() == 2) {
			double base = toDouble(input.get(0));
			double v = toDouble(input.get(1));
			return Math.pow(base, v);
		} else
			throw new IOException("Pow expect 2 arguments: (a, b)");
	}
	
	private double toDouble(Object obj) {
		if (obj instanceof Double) {
			return (double) obj;
		} else if (obj instanceof Long) {
			return ((Long) obj).doubleValue();
		} else if (obj instanceof Integer) {
			return ((Integer) obj).doubleValue();
		} else if (obj instanceof BigDecimal) {
			return ((BigDecimal) obj).doubleValue();
		} else 
			throw new org.apache.commons.lang.NotImplementedException();
	}
}
