package net.beanscode.plugin.pig.udfs;

import java.io.IOException;

import net.beanscode.model.dataset.Table;
import net.beanscode.model.dataset.TableFactory;
import net.hypki.libs5.weblibs.CacheManager;

import org.apache.pig.EvalFunc;
import org.apache.pig.data.Tuple;

public class TbName extends EvalFunc<String> {
	
	public TbName() {

	}

	@Override
	public String exec(Tuple input) throws IOException {
		if (input == null || input.size() == 0 || input.size() != 1)
			return null;
		
		String tmp = CacheManager.cacheInstance().get(String.class, "tbIdToTbName", input.get(0).toString());
		
		if (tmp == null) {
			Table table = TableFactory.getTable(new net.hypki.libs5.db.db.weblibs.utils.UUID(input.get(0).toString()));
			if (table != null) {
				CacheManager.cacheInstance().put("tbIdToTbName", input.get(0).toString(), table.getName());
				return table.getName();
			} else {
				CacheManager.cacheInstance().put("tbIdToTbName", input.get(0).toString(), null);
				return null;
			}
		} else
			return tmp;
	}
}
