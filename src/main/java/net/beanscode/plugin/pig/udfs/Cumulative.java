package net.beanscode.plugin.pig.udfs;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.utils.AssertUtils;

import org.apache.pig.Accumulator;
import org.apache.pig.EvalFunc;
import org.apache.pig.data.BagFactory;
import org.apache.pig.data.DataBag;
import org.apache.pig.data.DataType;
import org.apache.pig.data.DefaultDataBag;
import org.apache.pig.data.Tuple;
import org.apache.pig.data.TupleFactory;
import org.apache.pig.impl.logicalLayer.FrontendException;
import org.apache.pig.impl.logicalLayer.schema.Schema;
import org.apache.pig.impl.logicalLayer.schema.Schema.FieldSchema;
import org.javatuples.Pair;

public class Cumulative extends EvalFunc<DataBag> { //implements Accumulator<DataBag> {
	
	private List<Pair<Integer, Integer>> cumPairs = null;
	private Map<Integer, Double> cumMap = null;
	private DataBag b = null;
	
	public Cumulative() {
		
	}
	
	@Override
	public DataBag exec(Tuple tuple) throws IOException {
		List<Pair<Integer, Integer>> cumPairs = new ArrayList<Pair<Integer,Integer>>();
		Map<Integer, Double> cumMap = new HashMap<Integer, Double>();
		DataBag b = BagFactory.getInstance().newDefaultBag();
		
		return doJob(tuple, cumPairs, cumMap, b);
	};

	public DataBag doJob(Tuple tuple, List<Pair<Integer, Integer>> cumPairs, 
			Map<Integer, Double> cumMap, DataBag b) throws IOException {
		
		final DataBag inputBag = (DataBag) tuple.get(0);
		
		for (int i = 1; i < tuple.size(); i += 2) {
			final String from = (String) tuple.get(i);
			final String to = (String) tuple.get(i + 1);
			
			int fromPos = getInputSchema().getField(0).schema.getField(0).schema.getPosition(from);
			int toPos = getInputSchema().getField(0).schema.getField(0).schema.getPosition(to);
			
			cumPairs.add(new Pair<Integer, Integer>(fromPos, toPos));
			cumMap.put(fromPos, 0.0);
		}
		
		Iterator<Tuple> it = inputBag.iterator();
    	while (it.hasNext()) {
			Tuple t = (Tuple) it.next();
			if (t != null && t.size() != 0 && t.get(0) != null) {
				
				for (Pair pair : cumPairs) {
					int fromPos = (int) pair.getValue0();
					int toPos = (int) pair.getValue1();
					double cum = cumMap.get(fromPos);
					
					cum += (double) t.get(fromPos);
				
					t.set(toPos, cum);
					cumMap.put(fromPos, cum);
				}
				
				b.add(t);
			}
		}
		
		return b;
	}

//	@Override
//	public void accumulate(Tuple tuple) throws IOException {
//		if (cumMap == null) {
//			cumPairs = new ArrayList<Pair<Integer,Integer>>();
//			cumMap = new HashMap<Integer, Double>();
//			b = BagFactory.getInstance().newDefaultBag();
//			
//			LibsLogger.info(Cumulative.class, "Created Cumulative hash maps");
//		}
//		
//		doJob(tuple, cumPairs, cumMap, b);
//	}
//
//	@Override
//	public void cleanup() {
//		cumPairs = new ArrayList<Pair<Integer,Integer>>();
//		cumMap = new HashMap<Integer, Double>();
//		b = BagFactory.getInstance().newDefaultBag();
//		
//		LibsLogger.info(Cumulative.class, "Cleared Cumulative hash maps");
//	}
//
//	@Override
//	public DataBag getValue() {
//		return b;
//	}
	
}
