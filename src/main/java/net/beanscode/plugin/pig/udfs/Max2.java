package net.beanscode.plugin.pig.udfs;

import java.io.IOException;

import org.apache.pig.EvalFunc;
import org.apache.pig.data.Tuple;

public class Max2 extends EvalFunc<Double> {

	public Max2() {

	}

	@Override
	public Double exec(Tuple input) throws IOException {
		if (input == null || input.size() == 0)
			return null;
		if (input.size() == 2) {
			double d1 = toDouble(input.get(0));
			double d2 = toDouble(input.get(1));
			return Math.max(d1, d2);
		} else
			throw new IOException("Max expect 2 arguments: (a, b)");
	}
	
	private double toDouble(Object obj) {
		if (obj instanceof Double) {
			return (double) obj;
		} else if (obj instanceof Long) {
			return ((Long) obj).doubleValue();
		} else if (obj instanceof Integer) {
			return ((Integer) obj).doubleValue();
		} else 
			throw new org.apache.commons.lang.NotImplementedException();
	}
}
