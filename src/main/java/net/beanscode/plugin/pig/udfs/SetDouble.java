package net.beanscode.plugin.pig.udfs;

import java.io.IOException;

import net.beanscode.model.settings.Clipboard;
import net.hypki.libs5.weblibs.CacheManager;

import org.apache.pig.EvalFunc;
import org.apache.pig.data.Tuple;

public class SetDouble extends EvalFunc<Double> {

	public SetDouble() {
		
	}

	@Override
	public Double exec(Tuple input) throws IOException {
		String name = input.get(0).toString();
		Double value = (Double) input.get(1);
		
		Clipboard.set(name, value);
		
		CacheManager
			.cacheInstance()
			.put(SetDouble.class.getSimpleName(), name, value);
		
		return value;
	}
}
