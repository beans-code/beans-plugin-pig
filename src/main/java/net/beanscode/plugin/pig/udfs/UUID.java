package net.beanscode.plugin.pig.udfs;

import java.io.IOException;

import org.apache.pig.EvalFunc;
import org.apache.pig.data.Tuple;

public class UUID extends EvalFunc<String> {

	public UUID() {

	}

	@Override
	public String exec(Tuple input) throws IOException {
		return net.hypki.libs5.db.db.weblibs.utils.UUID.random().getId();
	}
}
