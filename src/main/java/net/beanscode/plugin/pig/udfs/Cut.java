package net.beanscode.plugin.pig.udfs;

import java.io.IOException;

import net.hypki.libs5.utils.string.RegexUtils;
import net.hypki.libs5.utils.utils.NumberUtils;

import org.apache.pig.EvalFunc;
import org.apache.pig.data.Tuple;

public class Cut extends EvalFunc<String> {

	public Cut() {

	}

	/**
	 * It cuts from a first value in the tuple the substring specified with the expression in the second value in the tuple.
	 */
	@Override
	public String exec(Tuple input) throws IOException {
		if (input == null || input.size() == 0)
			return null;
		
		if (input.size() == 2) {
			String toCut = (String) input.get(0);
			String pattern = (String) input.get(1);
			
			String tmp = null;
			while ((tmp = RegexUtils.firstGroup("\\(([\\d]+-[\\d]+)\\)", pattern)) != null) {
				pattern = pattern.replaceFirst("\\([\\d]+-[\\d]+\\)", cut(toCut, tmp));
			}
			
			return pattern;
		} else
			throw new IOException("Cut expect 2 arguments: (stringToCut, cutPattern)");
	}
	
	private String cut(String from, String pattern) {
		String [] parts = pattern.split("-");
		return from.substring(NumberUtils.toInt(parts[0]), NumberUtils.toInt(parts[1]));
	}
}
