package net.beanscode.plugin.pig;

import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;

import java.io.IOException;

import net.beanscode.model.BeansConst;
import net.beanscode.model.notebook.Notebook;
import net.beanscode.model.notebook.NotebookFactory;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.args.ArgsUtils;
import net.hypki.libs5.weblibs.jobs.JobsManager;
import net.hypki.libs5.weblibs.user.Email;
import net.hypki.libs5.weblibs.user.User;

public class DemoQueryMOCCASnapshot3 {

	public static void main(String[] args) throws ValidationException, IOException {
		BeansConst.init(new String[] {"--settings", "beans-dev.json"});
		
		assertTrue(ArgsUtils.getString(args, "user") != null, "Arg --user is not specified");
				
		final User user = User.getUser(new Email(ArgsUtils.getString(args, "user")));
		final Notebook notebook = NotebookFactory.searchNotebooks(user.getUserId(), "Demo MOCCA queries", 0, 20).getObjects().get(0);
		
		PigScript q = new PigScript(user, notebook.getId(), "query demo");
		q.setQuery("snap = load 'MOCCA 3000/snapshot' using MultiTable();"
//				+ "DESCRIBE snap;"
//				+ ""
//				+ "snapShort = FOREACH snap GENERATE $0, $1, $2, $3, $4, $5, $6;"
				+ "snapShort = FOREACH snap GENERATE tbid, im, sm1, sm2, systime;"
//				+ "snapShort = ORDER snapShort BY systime;"
//				+ "DESCRIBE snapShort;"
//				+ "DUMP snapShort;"
//				+ "x = LIMIT snapShort 30; DUMP x;"
//				+ ""
//				+ "time0 = FILTER snapShort BY systime.value < 1;"
//				+ "DESCRIBE time0;"
//				+ "DUMP time0;"
//				+ "x = LIMIT time0 30; DUMP x;"
//				+ ""
				+ "snapBinned = FOREACH snapShort GENERATE *, histogram(0.0, 100.0, 1.0, sm1.value + sm2.value) as bin;"
//				+ "DESCRIBE time0Binned;"
//				+ "DUMP time0Binned;"
//				+ "x = LIMIT time0Binned 30; DUMP x;"
//				+ ""
				+ "snapGrouped = GROUP snapBinned BY (systime, tbid, bin);"
//				+ "DESCRIBE time0Grouped;"
//				+ "DUMP time0Grouped;"
//				+ "x = LIMIT time0Grouped 30; DUMP x;"
//				+ ""
				+ "snapGroupedCount = FOREACH snapGrouped GENERATE group.systime, group.tbid, ('bin', group.bin), ('count', COUNT(snapBinned));"
				+ "DESCRIBE snapGroupedCount;"
				+ "DUMP snapGroupedCount;"
//				+ "snapGroupedCountAsc = ORDER snapGroupedCount BY t ASC;"
//				+ "DESCRIBE snapGroupedCountAsc;"
//				+ "DUMP snapGroupedCountAsc;"
//				+ ""
//				+ "--store snapGroupedCount into 'plot1/TYPE boxes 0.9 XRANGE 0; 10 YRANGE 0; 5 COLUMNS bin:count TITLE \"Initial mass distribution\"' using Plot();"
				);
		q.save();
		q.runQuery(false);
		
		JobsManager.runAllJobs(BeansConst.getAllChannelsNames());
		
//		net.hypki.moccabeans.model.plots.Plot.getPlot(q.getId(), "plot1").saveToFile("test-snapshot--");
		
		LibsLogger.debug(DemoQueryMOCCASnapshot3.class, "Finished!");
	}
}
