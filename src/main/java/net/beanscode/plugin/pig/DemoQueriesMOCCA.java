package net.beanscode.plugin.pig;

import static java.lang.String.format;
import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.beanscode.model.BeansConst;
import net.beanscode.model.notebook.Notebook;
import net.beanscode.model.notebook.NotebookFactory;
import net.beanscode.model.notebook.NotebookUtils;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.search.SearchResults;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.args.ArgsUtils;
import net.hypki.libs5.utils.reflection.SystemUtils;
import net.hypki.libs5.weblibs.SearchManager;
import net.hypki.libs5.weblibs.jobs.JobsManager;
import net.hypki.libs5.weblibs.user.User;

public class DemoQueriesMOCCA {

	public static void main(String[] args) throws ValidationException, IOException {
		BeansConst.init(new String[] {"--settings", "beans-dev.json"});

		final String userEmail = ArgsUtils.getString(args, "user");
		assertTrue(notEmpty(userEmail), "Arg --user is not specified");
		
		final User user = SearchManager.searchUsers(userEmail, 0, 1).getObjects().get(0);
		assertTrue(user != null, format("User %s not found", userEmail));
		
		createDemoQueries(user);
		
		JobsManager.runAllJobs(BeansConst.getAllChannelsNames());
	}
	
	public static void createDemoLineQueries(final User user) throws IOException, ValidationException {
		Notebook notebook = new Notebook(user.getUserId(), "Demo Lines queries")
			.save();
		
		String simpleScript = SystemUtils.readFileContent(DemoQueriesMOCCA.class, "testdata/demo-queries.notebook");
		
//		NotebookUtils.importSimpleScriptToNotebook(notebook, simpleScript);
	}
	
	public static void createDemoQueries(final User user) throws IOException, ValidationException {
//		JobsManager.runAllJobs();
		
		// Harris queries
//		Notebook notebook = new Notebook(user.getUserId(), "Demo Harris catalogue queries");
//		notebook.setDescription("This set of plots and queries presents the features of BEANS software using Harris Catalogue - a catalogue "
//				+ "containing the global properties of almost all star clusters present in the Milky Way.");
//		notebook.save();
//
//		//---------------
//		Query q = new Query(user, notebook.getId(), "Selected star clusters properties in parallel view");
//		q.setQuery("rows = load 'Harris catalogue/Harris3' using Table();\n" 
//				  + "store rows into 'plot1/TYPE parallel COLUMNS vr:vLSR:c:rc:rh:muV:MV:th:rho0:lgTc:lgTh:massGn:bimod' using Plot();");
//		q.save();
//		notebook.getEntries().add(q.getId());
//		notebook.save();
		
		// MOCCA queries
		/////////////////////////////////////////
		
		// notebook
		SearchResults<Notebook> res = NotebookFactory.searchNotebooks(user.getUserId(), "Demo MOCCA queries", 0, 1);
		Notebook notebook = res.maxHits() > 0 ? res.getObjects().get(0) : new Notebook(user.getUserId(), "Demo MOCCA queries").save();
		
		// list of files to import
		List<String> moccaToImport = new ArrayList<>();
		moccaToImport.add("mocca-diff-average-masses.beans");
		moccaToImport.add("mocca-diff-binary-escapers-number.beans");
		moccaToImport.add("mocca-diff-binary-number.beans");
		moccaToImport.add("mocca-diff-binary-number-lagr.beans");
		moccaToImport.add("mocca-diff-bin-energy.beans");
		moccaToImport.add("mocca-diff-bss-in-binaries.beans");
		moccaToImport.add("mocca-diff-bss-number.beans");
		moccaToImport.add("mocca-diff-central-density.beans");
		moccaToImport.add("mocca-diff-central-potential.beans");
		moccaToImport.add("mocca-diff-central-velocity.beans");
		moccaToImport.add("mocca-diff-collisions-number.beans");
		moccaToImport.add("mocca-diff-core-mass.beans");
		moccaToImport.add("mocca-diff-core-number.beans");
		moccaToImport.add("mocca-diff-cpu.beans");
		moccaToImport.add("mocca-diff-escaper-number.beans");
		moccaToImport.add("mocca-diff-escapers-energy.beans");
		moccaToImport.add("mocca-diff-exchanges-number.beans");
		moccaToImport.add("mocca-diff-interactions-number.beans");
		moccaToImport.add("mocca-diff-kick-energy.beans");
		moccaToImport.add("mocca-diff-mass-distribution.beans");
		moccaToImport.add("mocca-diff-mass-loss.beans");
		moccaToImport.add("mocca-diff-maximum-masses.beans");
		moccaToImport.add("mocca-diff-nt.beans");
		moccaToImport.add("mocca-diff-radii.beans");
		moccaToImport.add("mocca-diff-stars-types.beans");
		moccaToImport.add("mocca-diff-stars-types-mass.beans");
		moccaToImport.add("mocca-diff-sturn.beans");
		moccaToImport.add("mocca-diff-tau.beans");
		moccaToImport.add("mocca-diff-tidal-mass-loss.beans");
		moccaToImport.add("mocca-diff-timestep.beans");
		moccaToImport.add("mocca-diff-timestep-energy.beans");
		moccaToImport.add("mocca-diff-total-energy.beans");
		moccaToImport.add("mocca-diff-total-mass.beans");

		for (String oneFileToImport : moccaToImport) {
			LibsLogger.debug(DemoQueriesMOCCA.class, "Adding demo query ", oneFileToImport, " to Notebook ", notebook.getName());
			
			final String queryStr = SystemUtils.readFileContent(PigScript.class, "testdata/mocca/compare/" + oneFileToImport);
			
			final String queryTitle = queryStr.substring(2, queryStr.indexOf('\n'));
			final String queryScript = queryStr.substring(queryStr.indexOf('\n') + 1).trim();
			
			PigScript q = new PigScript(user, notebook.getId(), queryTitle);
			q.setQuery(queryScript);
			
			q.save();
			
			notebook.getEntries().add(q.getId());
			notebook.save();
		}
		
		LibsLogger.debug(DemoQueriesMOCCA.class, "Demo queries added to notebook ", notebook.getName());
	}
}
