package net.beanscode.plugin.pig;

import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;

import java.io.IOException;

import net.beanscode.model.BeansConst;
import net.beanscode.model.notebook.Notebook;
import net.beanscode.model.notebook.NotebookFactory;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.args.ArgsUtils;
import net.hypki.libs5.utils.reflection.SystemUtils;
import net.hypki.libs5.weblibs.jobs.JobsManager;
import net.hypki.libs5.weblibs.user.Email;
import net.hypki.libs5.weblibs.user.User;

public class DemoQueryRun {

	public static void main(String[] args) throws ValidationException, IOException {
		BeansConst.init(new String[] {"--settings", "beans-dev.json"});
		
		assertTrue(ArgsUtils.getString(args, "user") != null, "Arg --user is not specified");
		
		final User user = User.getUser(new Email(ArgsUtils.getString(args, "user")));
		final Notebook notebook = NotebookFactory.searchNotebooks(user.getUserId(), "Demo MOCCA queries", 0, 20).getObjects().get(0);
		
//		final String filename = "mocca-diff-central-potential.beans";
//		final String filename = "mocca-diff-maximum-masses.beans";
//		final String filename = "mocca-diff-tau.beans";
//		final String filename = "mocca-diff-timestep.beans";
//		final String filename = "mocca-diff-sturn.beans";
//		final String filename = "mocca-diff-nt.beans";
//		final String filename = "mocca-diff-core-number.beans";
//		final String filename = "mocca-diff-binary-number.beans";
//		final String filename = "mocca-diff-escaper-number.beans";
//		final String filename = "mocca-diff-escapers-energy.beans";
//		final String filename = "mocca-diff-bin-energy.beans";
//		final String filename = "mocca-diff-collisions-number.beans";
//		final String filename = "mocca-diff-interactions-number.beans";
//		final String filename = "mocca-diff-binary-escapers-number.beans";
//		final String filename = "mocca-diff-exchanges-number.beans";
//		final String filename = "mocca-diff-mass-loss.beans";
//		final String filename = "mocca-diff-bss-number.beans";
//		final String filename = "mocca-diff-bss-in-binaries.beans";
//		final String filename = "mocca-diff-kick-energy.beans";
//		final String filename = "mocca-diff-stars-types.beans";
//		final String filename = "mocca-diff-stars-types-mass.beans";
//		final String filename = "mocca-diff-binary-number-lagr.beans";
//		final String filename = "mocca-diff-cpu.beans";
		final String filename = "mocca-diff-average-masses.beans";
		
		final String queryStr = SystemUtils.readFileContent(PigScript.class, "testdata/mocca/" + filename);
		final String queryTitle = queryStr.substring(2, queryStr.indexOf('\n'));
		final String queryScript = queryStr.substring(queryStr.indexOf('\n') + 1).trim();
		
		PigScript q = new PigScript(user, notebook.getId(), queryTitle);
		q.setQuery(queryScript);
		
		q.save();
		q.runQuery(false);
		
		JobsManager.runAllJobs(BeansConst.getAllChannelsNames());
		
//		((Plot) NotebookEntryFactory.getNotebookEntry(q.getId()))
//			.setFilename(filename.substring(0, filename.length() - 6) + ".pdf")
//			.plot();
		
		LibsLogger.debug(DemoQueryRun.class, "Finished!");
	}
}
