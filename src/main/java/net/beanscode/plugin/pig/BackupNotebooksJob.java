package net.beanscode.plugin.pig;

import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;

import java.io.FileWriter;
import java.io.IOException;

import net.beanscode.model.BeansSettings;
import net.beanscode.model.notebook.Notebook;
import net.beanscode.model.notebook.NotebookEntry;
import net.beanscode.model.notebook.NotebookFactory;
import net.hypki.libs5.db.db.BackupUtils;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.SimpleDate;
import net.hypki.libs5.utils.date.Watch;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.weblibs.WeblibsConst;
import net.hypki.libs5.weblibs.jobs.Job;
import net.hypki.libs5.weblibs.settings.Setting;
import net.hypki.libs5.weblibs.settings.SettingFactory;
import net.hypki.libs5.weblibs.user.UserFactory;

public class BackupNotebooksJob extends Job {

	public BackupNotebooksJob() {
		
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		String backupFile = SimpleDate.now().toStringISO() + "_" + WeblibsConst.KEYSPACE + "_pigqueries.bac";
		
		Setting backupPathSetting = SettingFactory.getSetting(BeansSettings.SETTING_BACKUP_PATH);
		assertTrue(backupPathSetting != null, BeansSettings.SETTING_BACKUP_PATH, " setting does not exist");
		
		FileExt backupFolder = new FileExt(backupPathSetting.getValue("path").getValueAsString());
		backupFolder.mkdirs();
		
		BackupNotebooksJob.backupPlainPigQueries(WeblibsConst.KEYSPACE_LOWERCASE, backupFolder.getAbsolutePath() + "/" + backupFile);
	}

	public static void backupPlainPigQueries(final String keyspace, final String outputFile) throws IOException {
		Watch w = new Watch();
		LibsLogger.debug(BackupNotebooksJob.class, "Creating backup of notebooks for keyspace ", keyspace, " to the file ", outputFile, "...");
		
		// creating output folder
		FileExt out = new FileExt(outputFile);
		if (out.getParentFile() != null)
			out.getParentFile().mkdirs();
		
		FileWriter fw = null;
		
		try {
			fw = new FileWriter(outputFile);
			
			for (Notebook nt : NotebookFactory.iterateNotebooks()) {
				for (NotebookEntry ne : nt.iterateEntries()) {
					if (ne instanceof PigScript) {
						PigScript ps = (PigScript) ne;
						fw.append("-- name: " + ps.getName() + "\n");
						fw.append("-- create: " + ps.getCreate() + "\n");
						fw.append("-- notebookId: " + ps.getNotebookId() + "\n");
						fw.append("-- user: " + UserFactory.getUser(ps.getUserId()).getEmail() + "\n");
						if (ps.getQuery() != null)
							fw.append(ps.getQuery());
						fw.append("\n\n\n");
					}
				}
			}
						
			LibsLogger.debug(BackupUtils.class, "Backup of notebooks for a keyspace ", keyspace, " done to the file ",
					out.getAbsolutePath(), " in ", w.toString());
		} catch (Throwable t) {
			LibsLogger.error(BackupUtils.class, "Cannot backup keyspace " + keyspace, t);
		} finally {
			if (fw != null)
				fw.close();
		}
	}
}
