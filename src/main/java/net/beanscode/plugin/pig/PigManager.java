package net.beanscode.plugin.pig;

import static java.lang.String.format;
import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Pattern;

import org.apache.cassandra.hadoop.pig.CqlNativeStorage;
import org.apache.commons.lang.NotImplementedException;
import org.apache.hadoop.mapreduce.Job;
import org.apache.pig.ExecType;
import org.apache.pig.PigServer;
import org.apache.pig.ResourceSchema.ResourceFieldSchema;
import org.apache.pig.backend.executionengine.ExecJob;
import org.apache.pig.backend.executionengine.ExecJob.JOB_STATUS;
import org.apache.pig.data.DataType;
import org.apache.pig.tools.pigstats.ScriptState;
import org.apache.tez.dag.api.TezConfiguration;
import org.apache.tez.runtime.library.api.TezRuntimeConfiguration;

import net.beanscode.model.BeansCacheManager;
import net.beanscode.model.BeansSettings;
import net.beanscode.model.LocalSettings;
import net.beanscode.model.cass.BeansTable2;
import net.beanscode.model.dataset.Table;
import net.beanscode.model.dataset.TableFactory;
import net.beanscode.model.plugins.ColumnDef;
import net.beanscode.model.plugins.Func;
import net.beanscode.model.plugins.Plugin;
import net.beanscode.model.plugins.PluginManager;
import net.beanscode.plugin.pig.udfs.CacheAppend;
import net.beanscode.plugin.pig.udfs.CacheClear;
import net.beanscode.plugin.pig.udfs.CacheContains;
import net.beanscode.plugin.pig.udfs.ConcatAny;
import net.beanscode.plugin.pig.udfs.Cumulative;
import net.beanscode.plugin.pig.udfs.Cut;
import net.beanscode.plugin.pig.udfs.DsParamDouble;
import net.beanscode.plugin.pig.udfs.DsParamInteger;
import net.beanscode.plugin.pig.udfs.DsParamString;
import net.beanscode.plugin.pig.udfs.Floor;
import net.beanscode.plugin.pig.udfs.GetDouble;
import net.beanscode.plugin.pig.udfs.GetInt;
import net.beanscode.plugin.pig.udfs.GetString;
import net.beanscode.plugin.pig.udfs.Histogram;
import net.beanscode.plugin.pig.udfs.Max2;
import net.beanscode.plugin.pig.udfs.Min2;
import net.beanscode.plugin.pig.udfs.Pivot;
import net.beanscode.plugin.pig.udfs.Pow;
import net.beanscode.plugin.pig.udfs.SetDouble;
import net.beanscode.plugin.pig.udfs.SetInt;
import net.beanscode.plugin.pig.udfs.SetString;
import net.beanscode.plugin.pig.udfs.SimplifySchema;
import net.beanscode.plugin.pig.udfs.TbParamDouble;
import net.beanscode.plugin.pig.udfs.TbParamInteger;
import net.beanscode.plugin.pig.udfs.TbParamString;
import net.beanscode.plugin.pig.udfs.ToDouble;
import net.hypki.libs5.db.db.schema.ColumnType;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.db.weblibs.Settings;
import net.hypki.libs5.search.SearchResults;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.Watch;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.file.FileUtils;
import net.hypki.libs5.utils.reflection.SystemUtils;
import net.hypki.libs5.utils.string.RegexUtils;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.system.Basher;
import net.hypki.libs5.utils.system.Basher2;
import net.hypki.libs5.weblibs.settings.Setting;
import net.hypki.libs5.weblibs.settings.SettingFactory;

public class PigManager {
	
	private static final long MAX_NR_ROWS_FOR_FAST_READER = 1000 * 1000; 
	
	public static final String QUERY_ID 	= "QUERY_ID";
	public static final String DATA_ID 		= "DATA_ID";
	
	private static final boolean PIG_CHANGE_TO_UNIONS = false;
	
	public static Iterable<BeansTable> iterateLoadTables(final UUID queryId, final String pigQuery) {
		return new Iterable<BeansTable>() {
			@Override
			public Iterator<BeansTable> iterator() {
				return new Iterator<BeansTable>() {
					private Iterator<String> groupsIter = null;
					
					@Override
					public BeansTable next() {
						if (groupsIter == null)
							hasNext();
						
						try {
							String group = groupsIter.next();
							BeansTable uni = new BeansTable();
							uni.setPigScriptId(queryId);
							uni.setLocation(RegexUtils.firstGroup("'(.*)'", group), new Job());
							return uni;
						} catch (IOException e) {
							LibsLogger.error(PigManager.class, "Cannot parse Load tables from Pig script", e);
							return null;
						}
					}
					
					@Override
					public boolean hasNext() {
						if (StringUtilities.nullOrEmpty(pigQuery))
							return false;
						if (groupsIter == null) {
							List<String> groups = RegexUtils.allGroups("(load[\\s]+'.*'[\\s]+using[\\s]+" + BeansTable.class.getSimpleName() + ")", 
									pigQuery, Pattern.CASE_INSENSITIVE);
							groupsIter = groups.iterator();
						}
						return groupsIter.hasNext();
					}
				};
			}
		};
	}
	
	public static ColumnType pigTypeToColumnType(byte pigType) {
		if (pigType == DataType.LONG)
			return ColumnType.LONG;
		
		else if (pigType == DataType.INTEGER)
			return ColumnType.INTEGER;
        
		else if (pigType == DataType.CHARARRAY)
        	return ColumnType.STRING;
        
		else if (pigType == DataType.DOUBLE)
        	return ColumnType.DOUBLE;
        
		else
        	throw new NotImplementedException("Pig column type " + pigType 
        			+ " not supported " + DataType.findTypeName(pigType));
	}
	
	public static ResourceFieldSchema columnToResourceFieldSchema(final ColumnDef columnDef) {
		ResourceFieldSchema valSchema = new ResourceFieldSchema();
//        AbstractType<?> validator = validators.get(ByteBufferUtil.bytes(cdef.getName()));
        valSchema.setName(columnDef.getName());
        valSchema.setType(columnDef.getPigType());
        return valSchema;
	}
	
	private static String decodeWithParams(final String s, Map<String, String> params) {
		if (params == null || params.size() == 0)
			return s;
		
		List<String> groups = RegexUtils.allGroups("\\$([\\w\\d]+)", s);
		String newS = s;
		for (String group : groups) {
			String newVal = params.get(group);
			if (newVal != null)
				newS = newS.replaceAll("\\$" + group, newVal);
		}
		return newS;
	}
	
	private static List<List<Table>> splitByTheSameColumns(Iterable<Table> ntsIter) throws IOException {
		List<Table> remaining = new ArrayList<>();
		
		for (Table ts : ntsIter) {
			if (!ts.isInNotebook())
				remaining.add(ts);
		}
		
		List<List<Table>> splits = new ArrayList<List<Table>>();
		
		while (remaining.size() > 0) {
			Table toMatch = remaining.get(0);
			
			List<Table> oneSplit = new ArrayList<>();
			oneSplit.add(toMatch);
			
			List<Table> remainingTmp = new ArrayList<>();
			for (int i = 1; i < remaining.size(); i++) {
				if (toMatch.areColumnsTheSame(remaining.get(i)))
					oneSplit.add(remaining.get(i));
				else
					remainingTmp.add(remaining.get(i));
			}
			
			splits.add(oneSplit);
			remaining = remainingTmp;
		}
		
		return splits;
	}
	
	public static String convertBeansToPig(final String pigMode,
			final UUID userId, 
			final UUID queryId, 
			final String beansScript, 
			final Map<String, String> params) throws IOException {
		// building pig script:
		//   - BEANS adds additional header options for each script
		StringBuilder pigScript = new StringBuilder();
		
//		pigScript.append("SET pig.disable.counter true;\n");
		
		// INFO finally this hadoop.tmp.dir worked
		FileExt pigTemp = new FileExt(BeansSettings.getExecutionPath().getAbsolutePath() + "/pig-temp/");
		if (!pigTemp.exists())
			pigTemp.mkdirs();
		pigScript.append("SET hadoop.tmp.dir " + pigTemp.getAbsolutePath() + ";\n");
		pigScript.append("SET pig.temp.dir " + pigTemp.getAbsolutePath() + ";\n");
		if (!pigMode.contains(PigScript.PIG_MODE_LOCAL))
			pigScript.append("SET tez.lib.uris /tez.tar.gz;\n");
//		pigScript.append("SET pig.notification.listener " + PigScriptListener.class.getName() + ";\n");
//		pigScript.append("SET pig.notification.listener.arg abc;\n");
		
		// adding header commands
		pigScript.append("DEFINE CqlNativeStorage  " + CqlNativeStorage.class.getName() + "(); \n");
		
//		pigScript.append("DEFINE NotebookTable  " + NotebookTableStorage.class.getName() + "(); \n");
//		pigScript.append("DEFINE Table 		" + TableStorageOriginal.class.getName() + "(); \n");
		
		pigScript.append("DEFINE BeansTable 		" + BeansTable.class.getName() + "('" + queryId + "'); \n");
		pigScript.append("DEFINE BT2 		" + BeansTable2.class.getName() + "('" + queryId + "'); \n");
		
//		pigScript.append("DEFINE MultiTable " + TableStorageMulti.class.getName() + "(); \n");
//		pigScript.append("DEFINE TableFast " + TableStorageFast.class.getName() + "(); \n");
//		pigScript.append("DEFINE TableFast " + TableStorageOriginal.class.getName() + "(); \n");
		
		// TODO move these functions to some plugin
		pigScript.append("DEFINE HISTOGRAM 	" + Histogram.class.getName() + "(); \n");
		pigScript.append("DEFINE POW 		" + Pow.class.getName() + "(); \n");
		pigScript.append("DEFINE FLOOR 		" + Floor.class.getName() + "(); \n");
		pigScript.append("DEFINE CUMULATIVE	" + Cumulative.class.getName() + "(); \n");
		pigScript.append("DEFINE TODOUBLE	" + ToDouble.class.getName() + "(); \n");
		pigScript.append("DEFINE CUT 		" + Cut.class.getName() + "(); \n");
		pigScript.append("DEFINE MAX2 		" + Max2.class.getName() + "(); \n");
		pigScript.append("DEFINE MIN2 		" + Min2.class.getName() + "(); \n");
		pigScript.append("DEFINE PIVOT 		" + Pivot.class.getName() + "(); \n");
		pigScript.append("DEFINE CONCATANY 	" + ConcatAny.class.getName() + "(); \n");
		pigScript.append("DEFINE UUID 		" + net.beanscode.plugin.pig.udfs.UUID.class.getName() + "(); \n");
		pigScript.append("DEFINE TBNAME 	" + net.beanscode.plugin.pig.udfs.TbName.class.getName() + "(); \n");
		pigScript.append("DEFINE DSNAME 	" + net.beanscode.plugin.pig.udfs.DsName.class.getName() + "(); \n");
		pigScript.append("DEFINE DSPARAMDOUBLE 	" + DsParamDouble.class.getName() + "(); \n");
		pigScript.append("DEFINE DSPARAMINTEGER " + DsParamInteger.class.getName() + "(); \n");
		pigScript.append("DEFINE DSPARAMSTRING 	" + DsParamString.class.getName() + "(); \n");
		pigScript.append("DEFINE TBPARAMDOUBLE 	" + TbParamDouble.class.getName() + "(); \n");
		pigScript.append("DEFINE TBPARAMINTEGER " + TbParamInteger.class.getName() + "(); \n");
		pigScript.append("DEFINE TBPARAMSTRING 	" + TbParamString.class.getName() + "(); \n");
		pigScript.append("DEFINE " + SimplifySchema.class.getSimpleName() + " 	" + SimplifySchema.class.getName() + "(); \n");
		pigScript.append("DEFINE DSID 		" + net.beanscode.plugin.pig.udfs.DsId.class.getName() + "(); \n");
		pigScript.append("DEFINE " + CacheAppend.class.getSimpleName().toUpperCase() + " 		" + CacheAppend.class.getName() + "(); \n");
		pigScript.append("DEFINE " + CacheContains.class.getSimpleName().toUpperCase() + " 		" + CacheContains.class.getName() + "(); \n");
		pigScript.append("DEFINE " + CacheClear.class.getSimpleName().toUpperCase() + " 		" + CacheClear.class.getName() + "(); \n");
		
		pigScript.append("DEFINE " + SetInt.class.getSimpleName().toUpperCase() + " " + SetInt.class.getName() + "(); \n");
		pigScript.append("DEFINE " + GetInt.class.getSimpleName().toUpperCase() + " " + GetInt.class.getName() + "(); \n");
		pigScript.append("DEFINE " + SetDouble.class.getSimpleName().toUpperCase() + " " + SetDouble.class.getName() + "(); \n");
		pigScript.append("DEFINE " + GetDouble.class.getSimpleName().toUpperCase() + " " + GetDouble.class.getName() + "(); \n");
		pigScript.append("DEFINE " + SetString.class.getSimpleName().toUpperCase() + " " + SetString.class.getName() + "(); \n");
		pigScript.append("DEFINE " + GetString.class.getSimpleName().toUpperCase() + " " + GetString.class.getName() + "(); \n");
		
		
		// loading all UDF from plugins
		for (Plugin plugin : PluginManager.iterateAllPlugins()) {
			List<Func> funcs = plugin.getFuncList();
			if (funcs != null)
				for (Func func : funcs) {
					pigScript.append("DEFINE " + func.getName() + " " + func.getClass().getName() + "(); \n");
				}
		}
		
		// adding beans script itself
		pigScript.append(beansScript);
		
		// replace all MultiTable() statements with Table() and UNION command
		//   e.g.  rows = LOAD 'sse/' using MultiTable();
		String completePigScript = pigScript.toString();
		pigScript = new StringBuilder();
		for (String line : completePigScript.split("[\\r\\n]+")) {
			
			// fixing beanstable -> beanstable()
			if (line != null
					&& line.toLowerCase().trim().endsWith("beanstable;")) {
//				line = line.replace("beanstable;", "beanstable();");
				String load = RegexUtils.firstGroup("(beanstable;)", line, Pattern.CASE_INSENSITIVE);
				line = line.replace(load, BeansTable.class.getSimpleName() + "();");
			}
			
			if (line != null 
					&& line.toLowerCase().contains("beanstable()")
					&& line.toLowerCase().contains("load ")) {
				
				String into = RegexUtils.firstGroup("([\\s]*(load)[\\s]*[\\'\\\"])", line, Pattern.CASE_INSENSITIVE);
				line = line.replaceAll(into, into + "beans:///");
				
				if (PIG_CHANGE_TO_UNIONS == false) {
					
					String load = RegexUtils.firstGroup("using[\\s]*(beanstable\\(\\))", line, Pattern.CASE_INSENSITIVE);
					line = line.replace(load, BeansTable.class.getSimpleName() + "('" + queryId.getId() + "')");
					
					String datasets = RegexUtils.firstGroup("(datasets[\\s]*=[\\s]*\".*\")", line, Pattern.CASE_INSENSITIVE);
					String newDatasets = decodeWithParams(datasets, params);
					line = line.replace(datasets, newDatasets);
					
					String tables = RegexUtils.firstGroup("(tables[\\s]*=[\\s]*\".*\")", line, Pattern.CASE_INSENSITIVE);
					String newTables = decodeWithParams(tables, params);
					line = line.replace(tables, newTables);
					
					pigScript.append(line);
					pigScript.append("\n");
					continue;
				}
				
				final String alias = RegexUtils.firstGroup("[\\s]*([\\w\\d]+)[\\s]*=", line, Pattern.CASE_INSENSITIVE);
				final String multi = RegexUtils.firstGroup("[\\s]*[\\w\\d]+[\\s]*=[\\s]*load[\\s]+'(.*)'[\\s]+using[\\s]+BeansTable\\(\\)\\;", line, Pattern.CASE_INSENSITIVE);
				final String[] parts = StringUtilities.split(multi, '/');
				final String dsQuery = decodeWithParams(parts[0], params);
				final String tabQuery = decodeWithParams(parts.length > 1 ? parts[1] : "", params);
				
				int resCount = 0;
//				HashMap<String, Long> nrRows = new HashMap<>();
				for (Table nts : TableFactory.iterateTables(userId, dsQuery, tabQuery)) {
//					nrRows.put(nts.getId().getId(), TableFactory.getTable(nts.getId()).getNrOfRows());
					resCount++;
				}
				
				if (resCount == 0) {
					
					assertTrue(false, "No tables found matching criteria: " + dsQuery + "/" + tabQuery);
				
				} else if (resCount == 1) {
					
					Table nts = null;
					for (Table tmp : TableFactory.iterateTables(userId, dsQuery, tabQuery)) {
						nts = tmp;
						break;
					}
					if (nts != null)
						pigScript.append(format(alias + " = LOAD 'beans:/// %s/%s' using BeansTable();\n", 
								nts.getDatasetId().getId(), nts.getId().getId()));//line.replace("MultiTable();", nrRows.get(nts.getId()) > 0 && nrRows.get(nts.getId()) <= MAX_NR_ROWS_FOR_FAST_READER ? "TableFast()" : "Table()") + ";\n");
				
				} else {
						
					String unionCmd = (alias + " = UNION ");
					for (Table table : TableFactory.iterateTables(userId, dsQuery, tabQuery)) {
						String oneSplitId = UUID.random().getId();
						
//						String tableIds = "";
//						for (Table table : tables)
//							tableIds += table.getId().getId() + ",";
						
						if (TableFactory.getTable(table.getId()) != null) {
							pigScript.append(format("tmp%s = LOAD 'beans:/// %s/%s' using BeansTable();\n", 
									oneSplitId, table.getDatasetId().getId(), table.getId().getId()));
							
							if (!unionCmd.trim().endsWith("UNION"))
								unionCmd += ", ";
							unionCmd += "tmp" + oneSplitId;
						}
					}
					pigScript.append(unionCmd + ";\n");
					
				}
				
			} else if (line != null && line.toLowerCase().contains(" table()")) {
				
				final String location = RegexUtils.firstGroup("[\\s]*[\\w\\d]+[\\s]*=[\\s]*load[\\s]+'(.*)'[\\s]+using[\\s]+Table\\(\\)\\;", line, Pattern.CASE_INSENSITIVE);
				final String[] parts = StringUtilities.split(location, '/');
				final String dsQuery = decodeWithParams(parts[0], params);
				final String tabQuery = decodeWithParams(parts.length > 1 ? parts[1] : "", params);
				final String alias = RegexUtils.firstGroup("[\\s]*([\\w\\d]+)[\\s]*=", line, Pattern.CASE_INSENSITIVE);
				
				SearchResults<Table> res = TableFactory.searchTables(userId, dsQuery, tabQuery, 0, 10);
				assertTrue(res.size() > 0, "No datasets found for datasets query: ", dsQuery, " ", tabQuery);
				assertTrue(res.size() == 1, "Found more than one dataset for datasets query: ", dsQuery, " ", tabQuery);
					
				Table nts = res.getObjects().get(0);
//				assertTrue(nts.isInDataset(), "Currently, tables can only be read from datasets");
				
				Table table = nts;//Table.getTable(new UUID(nts.getId()));
				
				long totalRows = table.getNrOfRows();
				
				if (totalRows > 0 && totalRows <= MAX_NR_ROWS_FOR_FAST_READER) {
//					pigScript.append(line.replaceAll("table\\(\\)", "TableFast\\(\\)"));
					pigScript.append(alias + " = LOAD 'beans:/// " + table.getId().getId() + "' using Table();");
					pigScript.append("\n");
				} else {
//					pigScript.append(line);
					pigScript.append(alias + " = LOAD 'beans:/// " + table.getId().getId() + "' using Table();");
					pigScript.append("\n");
				}
				
			} else if (line != null && line.toLowerCase().matches("(.*)store(.*)beanstable\\(\\)(.*)")) {
				
				// adding UUID as a tbid for every BeansTable STORE
				
				String into = RegexUtils.firstGroup("([\\s]*(into)[\\s]*[\\'\\\"])", line, Pattern.CASE_INSENSITIVE);
				line = line.replaceAll(into, into + "beans:/// ");
				
				into = RegexUtils.firstGroup("using[\\s]*(beanstable\\(\\))", line, Pattern.CASE_INSENSITIVE);
				line = line.replace(into, BeansTable.class.getSimpleName() + "('" + queryId.getId() + "')");
				
				pigScript.append(line);
				pigScript.append("\n");
				
			} else {
				
				pigScript.append(line);
				pigScript.append("\n");
				
			}
		}
		
		LibsLogger.debug(PigManager.class, "BEANS script -> Pig Script:");
		int lineIdx = 1;
		for (String line : StringUtilities.split(pigScript.toString(), "\n")) {
			LibsLogger.debug(PigManager.class, "[" + (lineIdx++) + "] " + line);
		}
		
		return pigScript.toString();
	}
	
	@Deprecated
	public static void runCassPigScriptBasher(final PigScript pigScript, Map<String, String> params) throws IOException {
		Watch pigTime = Watch.fromNow();
		String pigScriptStr = null;
		
		try {
			assertTrue(pigScript != null, "Pig script cannot be empty");
			
			LibsLogger.debug(PigManager.class, "Pig script queryId=", pigScript.getId(), " dataId= ", pigScript.getDataId());
	
			String pigMode = "spark_local";
			
			pigScriptStr = convertBeansToPig(pigMode, pigScript.getUserId(), pigScript.getId(), pigScript.getQuery(), params);
			
			final Setting sett = SettingFactory.getSetting(BeansSettings.SETTING_PIG_FOLDER);
			final FileExt pigFolder = new FileExt(sett.getValue("path").getValueAsString());
			File pigScriptFile = new File(pigFolder.getAbsolutePath() + "/beans-" + pigScript.getId() + ".pig");
			FileUtils.saveToFile(pigScriptStr, pigScriptFile, true);
		
			LibsLogger.debug(PigManager.class, "Starting Apache Pig script in ", pigMode, " mode");
			Basher.execStreamApachePig(new String[] {"./bin/pig", 
					"-x", pigMode, 
					"-f", "beans-" + pigScript.getId() + ".pig"}, 
					pigFolder,
					new File(pigFolder.getAbsolutePath() + "/beans-" + pigScript.getId() + ".log"));
			
			LibsLogger.info(PigManager.class, "Finished");
		} catch (Throwable t) {
			int i = 1;
			if (pigScriptStr != null)
				for (String line : pigScriptStr.split("\\n"))
					LibsLogger.error(PigManager.class, "[" + (i++) + "] " + line);
			throw new IOException(t.getMessage(), t);
		} finally {
			LibsLogger.info(PigManager.class, "------------------------------------------------------");
			LibsLogger.info(PigManager.class, "| ");
			LibsLogger.info(PigManager.class, "| ");
			LibsLogger.info(PigManager.class, "| Pig script finished in " + pigTime);
			LibsLogger.info(PigManager.class, "| ");
			LibsLogger.info(PigManager.class, "------------------------------------------------------");
		}
	}
	
	public static void runPigScriptBasher(final PigScript pigScript, final String pigMode, 
			Map<String, String> params) throws IOException {
		Watch pigTime = Watch.fromNow();
		String pigScriptStr = null;
		final Setting sett = SettingFactory.getSetting(BeansSettings.SETTING_PIG_FOLDER);
		final FileExt pigFolder = new FileExt(sett.getValue("path").getValueAsString());
		
		FileWriter pigOut = new FileWriter(new File(pigFolder.getAbsolutePath() + 
				"/beans-" + pigScript.getId() + ".log"));
		
		try {
			assertTrue(pigScript != null, "Pig script cannot be empty");
			
			LibsLogger.debug(PigManager.class, "Pig script queryId=", pigScript.getId(), " dataId= ", pigScript.getDataId());
			
			// clear previous TableLocation instances
			BeansCacheManager.clearTableLocationCache();
			
			pigScriptStr = convertBeansToPig(pigMode, pigScript.getUserId(), pigScript.getId(), pigScript.getQuery(), params);

			File pigScriptFile = new File(pigFolder.getAbsolutePath() + "/beans-" + pigScript.getId() + ".pig");
			FileUtils.saveToFile(pigScriptStr, pigScriptFile, true);
			
			LibsLogger.debug(PigManager.class, "Starting Apache Pig script in ", pigMode, " mode");
			
			String javaHome = System.getenv("JAVA_HOME");
			String hadoopHome = System.getenv("HADOOP_HOME");
			
			assertTrue(notEmpty(javaHome), "JAVA_HOME is not set");
			assertTrue(notEmpty(hadoopHome), "HADOOP_HOME is not set");
			
			String error = "";
			for (String line : new Basher2()
									.setWorkingDir(pigFolder)
									.addEnv("JAVA_HOME", javaHome)
									.addEnv("HADOOP_HOME", hadoopHome)
									.add("./bin/pig")
									.add("-Dpig.notification.listener=" + PigScriptListener.class.getName())
									.add("-Dpig.notification.listener.arg=" + pigScript.getId() 
											+ ":" + pigScript.getUserId() 
											+ ":" + pigScript.getName().replaceAll("[\\s]+", "___"))
									.add("-x", pigMode)
									.add("-stop_on_failure")
									.add("-f", "beans-" + pigScript.getId() + ".pig")
									.runIter()) {
				LibsLogger.info(PigManager.class, line);
				
				if (line != null
						&& line.contains(" ERROR ")) {
					error += line.substring(line.lastIndexOf(" ERROR ") + 5);
				}
				
				pigOut.append(line);
				pigOut.append("\n");
			}
			
			if (StringUtilities.notEmpty(error))
				throw new IOException(error);
			
			LibsLogger.info(PigManager.class, "Finished");
		
		} catch (IOException e) {
			int i = 1;
			for (String line : pigScriptStr.split("\\n"))
				LibsLogger.error(PigManager.class, "[" + (i++) + "] " + line);
			throw e;
		} catch (Exception e) {
			int i = 1;
			if (pigScriptStr != null)
				for (String line : pigScriptStr.split("\\n"))
					LibsLogger.error(PigManager.class, "[" + (i++) + "] " + line);
			throw new IOException(e.getMessage(), e);
		} finally {
			if (pigOut != null)
				pigOut.close();
			
			LibsLogger.info(PigManager.class, "------------------------------------------------------");
			LibsLogger.info(PigManager.class, "| ");
			LibsLogger.info(PigManager.class, "| ");
			LibsLogger.info(PigManager.class, "| Pig script finished in " + pigTime);
			LibsLogger.info(PigManager.class, "| ");
			LibsLogger.info(PigManager.class, "------------------------------------------------------");
		}
	}
	
	public static void runCassPigScriptLocal(final PigScript pigScript, Map<String, String> params) throws IOException {
		Watch pigTime = Watch.fromNow();
		String pigScriptStr = null;
		try {
			assertTrue(pigScript != null, "Pig script cannot be empty");
			
			LibsLogger.debug(PigManager.class, "Pig script queryId=", pigScript.getId(), " dataId= ", pigScript.getDataId());
			
			// clear previous TableLocation instances
			BeansCacheManager.clearTableLocationCache();
			
//			String pigMode = "tez_local";
			String pigMode = "local";
			
			Properties props = new Properties();
//			PigServer pigServer = new PigServer(ExecType.LOCAL, props);
//			PigServer pigServer = new PigServer("spark_local", props);
			PigServer pigServer = new PigServer(pigMode, props);
			
			pigScriptStr = convertBeansToPig(pigMode, pigScript.getUserId(), pigScript.getId(), pigScript.getQuery(), params);
			
			pigServer.setBatchOn();
			
			// setting parameters to each pig script
			pigServer.getPigContext().getProperties().put(QUERY_ID, pigScript.getId().getId());
			pigServer.getPigContext().getProperties().put(DATA_ID, pigScript.getDataId().getId());
//			pigServer.getPigContext().getProperties().put(DB_MAIN_DATABASE_PROVIDER, BeansDbManager.getMainDatabaseProvider().getClass().getName());
//			pigServer.getPigContext().getProperties().put(DB_TABLE_DATA_PROVIDER, BeansDbManager.getTableDataDatabaseProvider().getClass().getName());
			
			if (params != null && params.size() > 0)
				for (String key : params.keySet()) {
					params.put(key, "'" + params.get(key) + "'");
				}
			
			pigServer.registerScript(new ByteArrayInputStream(pigScriptStr.getBytes()), params);
			
			// registering pig listener for progress reports
			ScriptState.get().registerListener(new PigScriptListener(pigScript.getId(), pigScript.getUserId(), pigScript.getName()));
//			LibsLogger.debug(PigManager.class, "Listeners count " + (ScriptState.get().getAllListeners() != null ? ScriptState.get().getAllListeners().size() : 0));
			
//			System.setProperty(StorageHelper.PIG_INPUT_FORMAT, BeansCqlInputFormat.class.getName());
//			System.setProperty(StorageHelper.PIG_OUTPUT_FORMAT, BeansCqlOutputFormat.class.getName());
			
			pigServer.parseAndBuild();
			
			List<ExecJob> execJobs = pigServer.executeBatch(false);
			
			pigServer.shutdown();
			
			// check status of the job
			StringBuilder msg = null;
			for (ExecJob execJob : execJobs) {
				if (execJob.getStatus() == JOB_STATUS.FAILED) {
					if (msg == null)
						msg = new StringBuilder("Error occured in pig script. ");
					msg.append("FAILED " + execJob.getException() + " " + execJob.toString());
				}
			}
			
			if (msg != null)
				throw new IOException(msg.toString());
		} catch (IOException e) {
			int i = 1;
			for (String line : pigScriptStr.split("\\n"))
				LibsLogger.error(PigManager.class, "[" + (i++) + "] " + line);
			throw e;
		} catch (Exception e) {
			int i = 1;
			if (pigScriptStr != null)
				for (String line : pigScriptStr.split("\\n"))
					LibsLogger.error(PigManager.class, "[" + (i++) + "] " + line);
			throw new IOException(e.getMessage(), e);
		} finally {
			LibsLogger.info(PigManager.class, "------------------------------------------------------");
			LibsLogger.info(PigManager.class, "| ");
			LibsLogger.info(PigManager.class, "| ");
			LibsLogger.info(PigManager.class, "| Pig script finished in " + pigTime);
			LibsLogger.info(PigManager.class, "| ");
			LibsLogger.info(PigManager.class, "------------------------------------------------------");
		}
	}
	
	@Deprecated
	public static void runCassPigScriptYarnSparkClean(final PigScript pigScript, Map<String, String> params) throws IOException {
		Watch pigTime = Watch.fromNow();
		String pigScriptStr = null;
		try {
			assertTrue(pigScript != null, "Pig script cannot be empty");
			
			LibsLogger.debug(PigManager.class, "Pig script queryId=", pigScript.getId(), " dataId= ", pigScript.getDataId());
			
			// clear previous TableLocation instances
			BeansCacheManager.clearTableLocationCache();
			
			Properties props = new Properties();
						
			System.setProperty("SPARK_HOME", "$HOME/spark/");
//			System.setProperty("HADOOPDIR", "$HOME/hadoop-2.7.7/etc/hadoop/");
//			System.setProperty("HADOOP_HOME", "$HOME/hadoop-2.7.7/");
//			
//			LibsLogger.debug(PigManager.class, "HADOOP_HOME=", System.getenv("HADOOP_HOME"));
//			LibsLogger.debug(PigManager.class, "HADOOPDIR=", System.getenv("HADOOPDIR"));
			
			props.setProperty("fs.default.name", "hdfs://hadoop-master:9000");
			
//			props.setProperty("mapreduce.jobtracker.address", "hadoop-master:54311");
//			props.setProperty("mapred.jobtracker.address", "hadoop-master:54311");
//			props.setProperty("mapred.job.tracker", "hadoop-master:8021");
//	        props.setProperty("yarn.resourcemanager.address", "hadoop-master:8088");
				
			String pigMode = "spark";
			
//			PigServer pigServer = new PigServer("mapreduce");//ExecType.MAPREDUCE);
//			PigServer pigServer = new PigServer(ExecType.MAPREDUCE, props);
//			PigServer pigServer = new PigServer(pigMode, props);
//			PigServer pigServer = new PigServer("tez", props);
			PigServer pigServer = new PigServer(pigMode, props);
			
			
			pigScriptStr = convertBeansToPig(pigMode, pigScript.getUserId(), pigScript.getId(), pigScript.getQuery(), params);
			
//			Properties props = new Properties();
//			props.setProperty("fs.default.name", "hdfs://localhost:50175");
//			props.setProperty("mapreduce.jobtracker.address", "localhost:54311");
//			pigServer.getPigContext().setLog4jProperties(getPigLo4jProperties(pigScript));
//			pigServer.debugOn();
			
//			pigServer.debugOff();
			pigServer.setBatchOn();
			
			// setting parameters to each pig script
			pigServer.getPigContext().getProperties().put(QUERY_ID, pigScript.getId().getId());
			pigServer.getPigContext().getProperties().put(DATA_ID, pigScript.getDataId().getId());
//			pigServer.getPigContext().getProperties().put(DB_MAIN_DATABASE_PROVIDER, BeansDbManager.getMainDatabaseProvider().getClass().getName());
//			pigServer.getPigContext().getProperties().put(DB_TABLE_DATA_PROVIDER, BeansDbManager.getTableDataDatabaseProvider().getClass().getName());
			
//			pig.getPigContext().getProperties().put("mapreduce.job.counters.max", "512");
//			pig.getPigContext().getProperties().put("mapreduce.job.counters.limit", "512");
			
			
			if (params != null && params.size() > 0)
				for (String key : params.keySet()) {
					params.put(key, "'" + params.get(key) + "'");
				}
			
//			LibsLogger.debug(PigManager.class, "BEANS script -> Pig script: ", pigScriptStr);
			
			pigServer.registerScript(new ByteArrayInputStream(pigScriptStr.getBytes()), params);
			
			// registering pig listener for progress reports
			ScriptState.get().registerListener(new PigScriptListener(pigScript.getId(), pigScript.getUserId(), pigScript.getName()));
			LibsLogger.debug(PigManager.class, "Listeners count " + (ScriptState.get().getAllListeners() != null ? ScriptState.get().getAllListeners().size() : 0));
			
//			System.setProperty(StorageHelper.PIG_INPUT_FORMAT, BeansCqlInputFormat.class.getName());
//			System.setProperty(StorageHelper.PIG_OUTPUT_FORMAT, BeansCqlOutputFormat.class.getName());
			
			pigServer.parseAndBuild();
			
			List<ExecJob> execJobs = pigServer.executeBatch(false);
			
			pigServer.shutdown();
			
			// check status of the job
			StringBuilder msg = null;
			for (ExecJob execJob : execJobs) {
				if (execJob.getStatus() == JOB_STATUS.FAILED) {
					if (msg == null)
						msg = new StringBuilder("Error occured in pig script. ");
					msg.append("FAILED " + execJob.getException() + " " + execJob.toString());
				}
			}
			
			if (msg != null)
				throw new IOException(msg.toString());
		} catch (IOException e) {
			int i = 1;
			for (String line : pigScriptStr.split("\\n"))
				LibsLogger.error(PigManager.class, "[" + (i++) + "] " + line);
			throw e;
		} catch (Exception e) {
			int i = 1;
			if (pigScriptStr != null)
				for (String line : pigScriptStr.split("\\n"))
					LibsLogger.error(PigManager.class, "[" + (i++) + "] " + line);
			throw new IOException(e.getMessage(), e);
		} finally {
			LibsLogger.info(PigManager.class, "------------------------------------------------------");
			LibsLogger.info(PigManager.class, "| ");
			LibsLogger.info(PigManager.class, "| ");
			LibsLogger.info(PigManager.class, "| Pig script finished in " + pigTime);
			LibsLogger.info(PigManager.class, "| ");
			LibsLogger.info(PigManager.class, "------------------------------------------------------");
		}
	}
	
	@Deprecated
	public static void runCassPigScriptInternal(final PigScript pigScript, Map<String, String> params) throws IOException {
		Watch pigTime = Watch.fromNow();
		String pigScriptStr = null;
		try {
			assertTrue(pigScript != null, "Pig script cannot be empty");
			
			LibsLogger.debug(PigManager.class, "Pig script queryId=", pigScript.getId(), " dataId= ", pigScript.getDataId());
			
			// clear previous TableLocation instances
			BeansCacheManager.clearTableLocationCache();
			
			ExecType pigMode = Settings.getString("Pig/mode", PigScript.PIG_MODE_LOCAL).equalsIgnoreCase("hadoop") ? ExecType.MAPREDUCE : ExecType.MAPREDUCE;
			
			Properties props = new Properties();
			
			
			String pigTmp = BeansSettings.getExecutionPath().getAbsolutePath() + "/pig-temp/";
			LibsLogger.info(PigManager.class, "Pig tmp folder: ", pigTmp);
			props.setProperty("pig.temp.dir", pigTmp);
			props.setProperty("java.io.tmpdir", pigTmp);
			LibsLogger.debug(PigManager.class, "Pig tmp folder set to ", pigTmp);
			
			if (pigMode == ExecType.MAPREDUCE) {
				System.setProperty("HADOOPDIR", "$HOME/hadoop-2.7.6/etc/hadoop/");
				System.setProperty("HADOOP_HOME", "$HOME/hadoop-2.7.6/");
				
				LibsLogger.debug(PigManager.class, "HADOOP_HOME=", System.getenv("HADOOP_HOME"));
				LibsLogger.debug(PigManager.class, "HADOOPDIR=", System.getenv("HADOOPDIR"));
				
	////			props.setProperty("fs.defaultFS", "hdfs://zegge.strw.leidenuniv.nl:9000");
	////			props.setProperty("mapred.job.tracker", "zegge.strw.leidenuniv.nl:54311");
				
//				props.setProperty("fs.defaultFS", "hdfs://0.0.0.0:50175");
//				props.setProperty("fs.defaultFS", "hdfs://172.19.0.3:50175");
				props.setProperty("fs.defaultFS", "hdfs://172.19.0.2:9000");
//				props.setProperty("fs.defaultFS", "hdfs://hadoop-master:9000/");
				
	//			props.setProperty("fs.default.name", "hdfs://localhost");
	//			props.setProperty("fs.default.name", "hdfs://localhost:50175");
	//			props.setProperty("mapred.job.tracker", "localhost:54311");
//				props.setProperty("mapreduce.jobtracker.address", "localhost:54311");
				props.setProperty("mapreduce.jobtracker.address", "172.19.0.2:54311");
				props.setProperty("mapred.jobtracker.address", "172.19.0.2:54311");
//				props.setProperty("mapred.map.tasks", "32");
				props.setProperty("fs.hdfs.impl", "org.apache.hadoop.hdfs.DistributedFileSystem");
				
				// INFO trying to show jobs on ResourceManager web page
	//			props.setProperty("mapreduce.framework.name", "yarn");
		        props.setProperty("yarn.resourcemanager.address", "172.19.0.2:8032");
				
	//			Properties props = new Properties();
	//			props.setProperty("fs.default.name", "hdfs://zegge.strw.leidenuniv.nl:9000");
	//			props.setProperty("mapred.job.tracker", "zegge.strw.leidenuniv.nl:54311");
			
			} else {
				System.setProperty("java.io.tmpdir", pigTmp);
				LibsLogger.debug(PigManager.class, "System java.io.tmpdir set to " + pigTmp);
			}
			
			props.setProperty("pig.pretty.print.schema", "true");
			
//			boolean tezLocalMode = BeansSettings.getSettingAsBoolean(null, "TEZ_LOCAL_MODE", true);
//			if (tezLocalMode)
				props.setProperty(TezConfiguration.TEZ_LOCAL_MODE, "true");
//			props.setProperty("fs.default.name", "file://" + BeansSettings.getExecutionPath());
			props.setProperty(TezRuntimeConfiguration.TEZ_RUNTIME_OPTIMIZE_LOCAL_FETCH, "true");
//			props.setProperty("tez.task.resource.memory.mb", "-Xmx4000m");
//			props.setProperty("tez.am.resource.memory.mb", "4096");
//			props.setProperty("mapreduce.map.memory.mb", "5000");
			props.setProperty("tez.task.launch.cmd-opts", "-Xms400m -Xmx2000m");
			props.setProperty("mapreduce.map.memory.mb", "2000");
			props.setProperty("tez.lib.uris", "$HOME/apache-tez-0.9.2-bin/share/tez.tar.gz");
//			pigMode = "tez";

//			PigServer pigServer = new PigServer("mapreduce");//ExecType.MAPREDUCE);
			PigServer pigServer = new PigServer(pigMode, props);
//			PigServer pigServer = new PigServer(pigMode, props);
//			PigServer pigServer = new PigServer("tez", props);
			LibsLogger.debug(PigManager.class, "Starting Pig script in ", pigMode, " mode");
			
			pigScriptStr = convertBeansToPig(pigMode.toString(), pigScript.getUserId(), pigScript.getId(), pigScript.getQuery(), params);
			
//			Properties props = new Properties();
//			props.setProperty("fs.default.name", "hdfs://localhost:50175");
//			props.setProperty("mapreduce.jobtracker.address", "localhost:54311");
//			pigServer.getPigContext().setLog4jProperties(getPigLo4jProperties(pigScript));
//			pigServer.debugOn();
			pigServer.debugOff();
			pigServer.setBatchOn();
			
			// setting parameters to each pig script
			pigServer.getPigContext().getProperties().put(QUERY_ID, pigScript.getId().getId());
			pigServer.getPigContext().getProperties().put(DATA_ID, pigScript.getDataId().getId());
//			pigServer.getPigContext().getProperties().put(DB_MAIN_DATABASE_PROVIDER, BeansDbManager.getMainDatabaseProvider().getClass().getName());
//			pigServer.getPigContext().getProperties().put(DB_TABLE_DATA_PROVIDER, BeansDbManager.getTableDataDatabaseProvider().getClass().getName());
			
//			pig.getPigContext().getProperties().put("mapreduce.job.counters.max", "512");
//			pig.getPigContext().getProperties().put("mapreduce.job.counters.limit", "512");
			
			
			if (params != null && params.size() > 0)
				for (String key : params.keySet()) {
					params.put(key, "'" + params.get(key) + "'");
				}
			
//			LibsLogger.debug(PigManager.class, "BEANS script -> Pig script: ", pigScriptStr);
			
			pigServer.registerScript(new ByteArrayInputStream(pigScriptStr.getBytes()), params);
			
			// registering pig listener for progress reports
			ScriptState.get().registerListener(new PigScriptListener(pigScript.getId(), pigScript.getUserId(), pigScript.getName()));
			LibsLogger.debug(PigManager.class, "Listeners count " + (ScriptState.get().getAllListeners() != null ? ScriptState.get().getAllListeners().size() : 0));
			
//			System.setProperty(StorageHelper.PIG_INPUT_FORMAT, BeansCqlInputFormat.class.getName());
//			System.setProperty(StorageHelper.PIG_OUTPUT_FORMAT, BeansCqlOutputFormat.class.getName());
			
			pigServer.parseAndBuild();
			
			List<ExecJob> execJobs = pigServer.executeBatch(false);
			
			pigServer.shutdown();
			
			// check status of the job
			StringBuilder msg = null;
			for (ExecJob execJob : execJobs) {
				if (execJob.getStatus() == JOB_STATUS.FAILED) {
					if (msg == null)
						msg = new StringBuilder("Error occured in pig script. ");
					msg.append("FAILED " + execJob.getException() + " " + execJob.toString());
				}
			}
			
			if (msg != null)
				throw new IOException(msg.toString());
		} catch (IOException e) {
			int i = 1;
			for (String line : pigScriptStr.split("\\n"))
				LibsLogger.error(PigManager.class, "[" + (i++) + "] " + line);
			throw e;
		} catch (Exception e) {
			int i = 1;
			if (pigScriptStr != null)
				for (String line : pigScriptStr.split("\\n"))
					LibsLogger.error(PigManager.class, "[" + (i++) + "] " + line);
			throw new IOException(e.getMessage(), e);
		} finally {
			LibsLogger.info(PigManager.class, "------------------------------------------------------");
			LibsLogger.info(PigManager.class, "| ");
			LibsLogger.info(PigManager.class, "| ");
			LibsLogger.info(PigManager.class, "| Pig script finished in " + pigTime);
			LibsLogger.info(PigManager.class, "| ");
			LibsLogger.info(PigManager.class, "------------------------------------------------------");
		}
	}
	
	@Deprecated
	public static void runCassPigScriptYarn(final PigScript pigScript, Map<String, String> params) throws IOException {
		Watch pigTime = Watch.fromNow();
		String pigScriptStr = null;
		try {
			assertTrue(pigScript != null, "Pig script cannot be empty");
			
			LibsLogger.debug(PigManager.class, "Pig script queryId=", pigScript.getId(), " dataId= ", pigScript.getDataId());
			
			// clear previous TableLocation instances
			BeansCacheManager.clearTableLocationCache();
			
//			System.setProperty("HADOOPDIR", "$HOME/hadoop-2.7.7/etc/hadoop/");
//			System.setProperty("HADOOP_HOME", "$HOME/hadoop-2.7.7/");
			
			ExecType pigMode = Settings.getString("Pig/mode", PigScript.PIG_MODE_LOCAL).equalsIgnoreCase("hadoop") ? ExecType.MAPREDUCE : ExecType.MAPREDUCE;
			
			Properties props = new Properties();
			
			String pigTmp = BeansSettings.getExecutionPath().getAbsolutePath() + "/pig-temp/";
			
//			if (pigMode == ExecType.MAPREDUCE) {
				props.setProperty("fs.defaultFS", "hdfs://hadoop-master:9000"); 
//		        props.setProperty("yarn.resourcemanager.address", "hadoop-master:8032");
				
//				props.setProperty("mapreduce.jobtracker.address", "172.19.0.2:54311");
//				props.setProperty("mapred.jobtracker.address", "172.19.0.2:54311");
//				props.setProperty("fs.hdfs.impl", "org.apache.hadoop.hdfs.DistributedFileSystem");
//		        props.setProperty("yarn.resourcemanager.address", "172.19.0.2:8032");
//				props.setProperty("mapreduce.framework.name", "yarn");
//			} else {
//			}
//			PigServer pigServer = new PigServer(ExecType.MAPREDUCE, props);
			PigServer pigServer = new PigServer(pigMode);//, props);
			LibsLogger.debug(PigManager.class, "Starting Pig script in ", pigMode, " mode");
			
			pigScriptStr = convertBeansToPig(pigMode.toString(), pigScript.getUserId(), pigScript.getId(), pigScript.getQuery(), params);
			
			pigServer.debugOff();
			pigServer.setBatchOn();
			
			// setting parameters to each pig script
			pigServer.getPigContext().getProperties().put(QUERY_ID, pigScript.getId().getId());
			pigServer.getPigContext().getProperties().put(DATA_ID, pigScript.getDataId().getId());
//			pigServer.getPigContext().getProperties().put(DB_MAIN_DATABASE_PROVIDER, BeansDbManager.getMainDatabaseProvider().getClass().getName());
//			pigServer.getPigContext().getProperties().put(DB_TABLE_DATA_PROVIDER, BeansDbManager.getTableDataDatabaseProvider().getClass().getName());
			
//			pig.getPigContext().getProperties().put("mapreduce.job.counters.max", "512");
//			pig.getPigContext().getProperties().put("mapreduce.job.counters.limit", "512");
			
			
			if (params != null && params.size() > 0)
				for (String key : params.keySet()) {
					params.put(key, "'" + params.get(key) + "'");
				}
			
//			LibsLogger.debug(PigManager.class, "BEANS script -> Pig script: ", pigScriptStr);
			
			pigServer.registerScript(new ByteArrayInputStream(pigScriptStr.getBytes()), params);
			
			// registering pig listener for progress reports
			ScriptState.get().registerListener(new PigScriptListener(pigScript.getId(), pigScript.getUserId(), pigScript.getName()));
			LibsLogger.debug(PigManager.class, "Listeners count " + (ScriptState.get().getAllListeners() != null ? ScriptState.get().getAllListeners().size() : 0));
			
//			System.setProperty(StorageHelper.PIG_INPUT_FORMAT, BeansCqlInputFormat.class.getName());
//			System.setProperty(StorageHelper.PIG_OUTPUT_FORMAT, BeansCqlOutputFormat.class.getName());
			
			pigServer.parseAndBuild();
			
			List<ExecJob> execJobs = pigServer.executeBatch(false);
			
			pigServer.shutdown();
			
			// check status of the job
			StringBuilder msg = null;
			for (ExecJob execJob : execJobs) {
				if (execJob.getStatus() == JOB_STATUS.FAILED) {
					if (msg == null)
						msg = new StringBuilder("Error occured in pig script. ");
					msg.append("FAILED " + execJob.getException() + " " + execJob.toString());
				}
			}
			
			if (msg != null)
				throw new IOException(msg.toString());
		} catch (IOException e) {
			int i = 1;
			for (String line : pigScriptStr.split("\\n"))
				LibsLogger.error(PigManager.class, "[" + (i++) + "] " + line);
			throw e;
		} catch (Exception e) {
			int i = 1;
			if (pigScriptStr != null)
				for (String line : pigScriptStr.split("\\n"))
					LibsLogger.error(PigManager.class, "[" + (i++) + "] " + line);
			throw new IOException(e.getMessage(), e);
		} finally {
			LibsLogger.info(PigManager.class, "------------------------------------------------------");
			LibsLogger.info(PigManager.class, "| ");
			LibsLogger.info(PigManager.class, "| ");
			LibsLogger.info(PigManager.class, "| Pig script finished in " + pigTime);
			LibsLogger.info(PigManager.class, "| ");
			LibsLogger.info(PigManager.class, "------------------------------------------------------");
		}
	}

	public static void runCassPigScriptAsGrunt(PigScript pigScript, HashMap<String, String> pigParams) throws IOException {
		new FileExt(LocalSettings.SETTINGS_DIR, "pig").mkdirs();
		FileExt pigScriptFile = new FileExt(LocalSettings.SETTINGS_DIR, "pig", "pig-" + pigScript.getId() + ".beans");
		FileUtils.saveToFile(pigScript.getQuery(), pigScriptFile, true);
		
		FileExt pigScriptOut = new FileExt(LocalSettings.SETTINGS_DIR, "pig", "pig-" + pigScript.getId() + ".log");
		Basher.execStreamOut(new String[] {"bash", "-ic", "$HOME/pig-0.17.0/bin/pig", "-x", "mapreduce", "-f", pigScriptFile.getAbsolutePath()}, 
				new File(SystemUtils.getHomePath()),
				pigScriptOut);
		
//		PigServer pigServer = new PigServer(PigScript.PIG_MODE_LOCAL);
//		
//		pigServer.debugOff();
//		pigServer.setBatchOn();
//
//		pigServer.registerScript(new ByteArrayInputStream(pigScript.getQuery().getBytes()));
//		
//		pigServer.parseAndBuild();
//		
//		List<ExecJob> execJobs = pigServer.executeBatch(false);
//		
//		pigServer.shutdown();
	}
	
	public static void main(String[] args) throws IOException {
//		Basher.execStreamOut(new String[]{"bash", "-ic", "./bin/pig", "-x", "mapreduce", "-f", "./bin/beans-parallel-read-3.pig"}, 
//				new FileExt("$HOME/pig-0.17.0"), 
//				new FileExt("pig-pig.log"));
		
//		PigRunner.run(new String[]{"-x", "mapreduce", "-f", "$HOME/pig-0.17.0/bin/beans-parallel-read-3.pig"}, null);
		
		for (HadoopJob job : HadoopManager.getApplicationsList()) {
//			LibsLogger.info(PigManager.class, job);
			System.out.println(job);
		}
	}
}
