package net.beanscode.plugin.pig;

import static java.lang.String.format;

import java.io.IOException;

import net.beanscode.model.cass.notification.Notification;
import net.beanscode.model.cass.notification.NotificationFactory;
import net.beanscode.model.cass.notification.NotificationType;
import net.beanscode.model.notebook.NotebookEntryProgress;
import net.beanscode.model.notebook.NotebookEntryProgressFactory;
import net.beanscode.plugin.pig.udfs.CacheContains;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.Watch;
import net.hypki.libs5.utils.string.StringUtilities;

import org.apache.pig.impl.plan.OperatorPlan;
import org.apache.pig.tools.pigstats.JobStats;
import org.apache.pig.tools.pigstats.OutputStats;

public class PigScriptListener implements org.apache.pig.tools.pigstats.PigProgressNotificationListener {
	
	private Watch watch = new Watch();
	private int lastProgress = 0;
	private UUID queryId = null;
	private UUID userId = null;
	private String scriptName = null;
	
//	private PigScript pig = null;
	
	
	public PigScriptListener(UUID queryId, UUID userId, String scriptName) {
		this.queryId = queryId;
		this.userId = userId;
		this.scriptName = scriptName;
//		try {
//			this.pig = PigScript.getQuery(queryId);
//		} catch (IOException e) {
//			LibsLogger.error(PigScriptListener.class, "Cannot get Pig script from DB", e);
//		}
	}
	
	public PigScriptListener() {
		LibsLogger.info(PigScriptListener.class, "no-arg constr");
	}
	
	public PigScriptListener(String init) {
		LibsLogger.info(PigScriptListener.class, "init ", init);
		if (StringUtilities.notEmpty(init)) {
			String[] parts = StringUtilities.split(init, ":");
			if (parts.length >= 3) {
				this.queryId = new UUID(parts[0].trim());
				this.userId = new UUID(parts[1].trim());
				this.scriptName = parts[2].trim();
				
				this.scriptName = this.scriptName != null ? 
						this.scriptName.replaceAll("___", " ") : 
						this.scriptName;
			}
		}
	}

//	@Override
//	public void initialPlanNotification(String scriptId, MROperPlan plan) {
//		
//	}

	@Override
	public void launchStartedNotification(String scriptId, int numJobsToLaunch) {
		
	}

	@Override
	public void jobsSubmittedNotification(String scriptId, int numJobsSubmitted) {
		
	}

	@Override
	public void jobStartedNotification(String scriptId, String assignedJobId) {
		
	}

	@Override
	public void jobFinishedNotification(String scriptId, JobStats jobStats) {
		
	}

	@Override
	public void jobFailedNotification(String scriptId, JobStats jobStats) {
		
	}

	@Override
	public void outputCompletedNotification(String scriptId, OutputStats outputStats) {
		
	}

	@Override
	public void progressUpdatedNotification(String scriptId, int progress) {		
		try {	
			LibsLogger.debug(PigScriptListener.class, format("Script for queryId %s done in %d percent (%s)", queryId, progress, getWatch()));
			LibsLogger.debug(PigScriptListener.class, CacheContains.class.getSimpleName(), " ", CacheContains.allMs, " [ms]");
			
			// report only progress larger or equal than 1%
			if ((progress - lastProgress) >= 1) {
//				QueryStatus s = QueryStatus.getStatus(queryId);
//				if (s != null) {
//					s.setProgress(progress);
//					s.save();
//				}
				
				NotebookEntryProgress progEntry = NotebookEntryProgressFactory.getNotebookEntryProgress(queryId);
				if (progEntry != null) {
					progEntry
						.running(progress, "Running...")
						.save();
				}
				
//				new Notification()
//					.setId(queryId)
//					.setDescription(scriptName)
//					.setLocked(false)
//					.setNotificationType(NotificationType.INFO)
//					.setTitle("Pig script: " + scriptName + " " + progress + "%")
//					.setUserId(userId)
//					.addMeta("progress", progress)
//					.save();
				
				LibsLogger.info(PigScriptListener.class, "New progress '", scriptName, "' ", progress);
			}
			
			this.lastProgress = progress;
		} catch (Throwable e) {
			LibsLogger.error(PigScriptListener.class, "Cannot save progress for queryId " + queryId, e);
		}
	}

	@Override
	public void launchCompletedNotification(String scriptId, int numJobsSucceeded) {
		
	}

	public Watch getWatch() {
		return watch;
	}

	public void setWatch(Watch watch) {
		this.watch = watch;
	}

	@Override
	public void initialPlanNotification(String arg0, OperatorPlan<?> arg1) {
		
	}

}
