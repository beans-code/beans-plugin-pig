package net.beanscode.plugin.pig.test;

import org.junit.Test;

import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.search.query.Query;
import net.hypki.libs5.utils.tests.LibsTestCase;

public class QueryParseTestCase extends LibsTestCase {

	@Test
	public void testParser() {
		Query q = new Query();
		q = Query.parseQuery("ik1f == 14 OR ik2f == 14");
		
		System.out.println(q);
	}
	
	@Test
	public void testParser2() {
		Query q = new Query();
		q = Query.parseQuery("(    (ik1f >= 10 AND ik1f < 13) "
				+ "            AND (ik2f >= 10 AND ik2f < 13)) "
				+ "            or  (    (ik3f >= 10 AND ik3f < 13) "
				+ "                 AND (ik4f >= 10 AND ik4f < 13))");
		
		System.out.println(q);
		System.out.println(q.isFulfilled(new Row()
				.addColumn("ik1f", 10)
				.addColumn("ik2f", 10)
				.addColumn("ik3f", 10)
				.addColumn("ik4f", 10)));
		System.out.println(q.isFulfilled(new Row()
				.addColumn("ik1f", 13)
				.addColumn("ik2f", 10)
				.addColumn("ik3f", 13)
				.addColumn("ik4f", 10)));
	}
}
