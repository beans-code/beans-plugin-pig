package net.beanscode.plugin.pig.test;

import java.io.File;
import java.io.IOException;

import org.junit.Test;

import net.beanscode.model.cass.Data;
import net.beanscode.model.cass.DataFactory;
import net.beanscode.model.connectors.CassandraConnector;
import net.beanscode.model.dataset.Dataset;
import net.beanscode.model.dataset.Table;
import net.beanscode.model.notebook.Notebook;
import net.beanscode.model.tests.BeansTestCase;
import net.beanscode.plugin.pig.PigScript;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.reflection.SystemUtils;
import net.hypki.libs5.utils.string.Meta;
import net.hypki.libs5.weblibs.SearchManager;
import net.hypki.libs5.weblibs.WeblibsConst;
import net.hypki.libs5.weblibs.user.User;

public class PigUnitTest extends BeansTestCase {
	
	@Test
	public void simplePigFast() throws IOException, ValidationException {
		User user = createTestUser("test");
		Dataset ds = createDataset(user, "datasetName");
		Table table = createTable(ds, "tableName");
		final int rowsMax = 7000;
		final int colsMax = 250;
		
		CassandraConnector data = new CassandraConnector(table);
		for (long row = 0; row < rowsMax; row++) {
			Row r = new Row(row);
			r.addColumn("tbid", table.getId().getId());
			r.addColumn("c1", 1.0 * row);
			for (int col = 2; col <= colsMax; col++) {
				r.addColumn("c" + col, col);
			}
			data.write(r);
			if (row == (20000/colsMax)) {
				LibsLogger.debug(PigUnitTest.class, "In total ", row, " saved");
			}
		}
		data.close();
		
		int rowsCounter = 0;
		for (Row row : DataFactory.getData(table.getId().getId())) {
			if (rowsCounter < 10)
				LibsLogger.debug(PigUnitTest.class, "row ", row);
			rowsCounter++;
		}
		assertTrue(rowsCounter == rowsMax, "Read " + rowsCounter + " rows but expected " + rowsMax);
		
		String pig = "--Central potential for Demo MOCCA n=40k simulations\n" + 
				"rows = load '" + table.getId().getId() + "' using TableFast();\n" +
				"rows2 = FOREACH rows GENERATE tbid, c1 as tphys, c2 as u1;\n" +
//				"X = LIMIT rows 10;\n" +
//				"DUMP X;\n"
				"DUMP rows2;\n"
				;
		
//		PigManager.runCassPigScript(pig, UUID.random(), UUID.random(), null);
		
	}
	
//	@Test
	public void simplePig() throws IOException, ValidationException {
//		LibsLogger.debug(PigUnitTest.class, InetAddress.getAllByName("localhost"));
		
		User user = createTestUser("test");
		Dataset ds = createDataset(user, "datasetName");
		Table table = createTable(ds, "tableName");
		final int rowsMax = 7000;
		final int colsMax = 5;
		String dataId = table.getId().getId();
		
		CassandraConnector data = new CassandraConnector(table);
		for (long row = 0; row < rowsMax; row++) {
			Row r = new Row(row);
			r.addColumn("tbid", dataId);
			r.addColumn("c1", 1.0 * row);
			for (int col = 2; col <= colsMax; col++) {
				r.addColumn("c" + col, col);
			}
			data.write(r);
			if (row == (20000/colsMax)) {
				
				LibsLogger.debug(PigUnitTest.class, "In total ", row, " saved");
			}
		}
		data.close();
		
		int rowsCounter = 0;
		for (Row row : DataFactory.getData(dataId)) {
			if (rowsCounter < 10)
				LibsLogger.debug(PigUnitTest.class, "row ", row);
			rowsCounter++;
		}
		assertTrue(rowsCounter == rowsMax, "Read " + rowsCounter + " rows but expected " + rowsMax);
		
		String pig = "--Central potential for Demo MOCCA n=40k simulations\n" + 
				"rows = load '" + dataId + "' using TableIter();\n" +
//				"rows = load '" + new CqlLocation().setKeyspace(WeblibsConst.KEYSPACE).setTableSchema(dataSchema).toString() + "' using CqlNativeStorage();\n" +
				"rows2 = FOREACH rows GENERATE tbid, c1 as tphys, c2 as u1;\n" +
//				"X = LIMIT rows 10;\n" +
//				"DUMP X;\n"
				"DUMP rows2;\n"
				;
		
//		String pig = "--Central potential for Demo MOCCA n=40k simulations\n" + 
//				"rows = load '" + new CqlLocation().setKeyspace(WeblibsConst.KEYSPACE).setTableSchema(dataSchema).toString() +
////					"&columns=tbid,c1,c2" +
////					"&input_cql=" + UrlUtilities.UrlEncode("SELECT tbid,c1,c2 FROM " + WeblibsConst.KEYSPACE.toLowerCase() + "." + dataCF + " WHERE token(pk) > ? and token(pk) <?") +
//				"' using CqlNativeStorage();\n" +
//				"rows = FOREACH rows GENERATE tbid, c1 as tphys, c2 as u1;\n" +
////				"X = LIMIT rows 10;\n" +
////				"DUMP X;\n"
//				"DUMP rows;\n"
//				;
		
		
//		PigManager.runCassPigScript(pig, UUID.random(), UUID.random(), null);
		
	}
	
//	@Test
	public void simpleNt40000MOCCA() throws IOException, ValidationException {
		final User user = createTestUser("ut");
		
		SearchManager.searchInstance().clearIndexType(WeblibsConst.KEYSPACE_LOWERCASE, Dataset.COLUMN_FAMILY.toLowerCase());
		SearchManager.searchInstance().clearIndexType(WeblibsConst.KEYSPACE_LOWERCASE, Table.COLUMN_FAMILY.toLowerCase());
		
		final Dataset dsMOCCA = new Dataset(user.getUserId(), "MOCCA n=40000");
		dsMOCCA.getMeta().add("n", 40000);
		dsMOCCA.save();
		addToRemove(dsMOCCA);
		
		final Table systemDat = new Table(dsMOCCA, "system.dat");
		systemDat.getMeta().add(new Meta("n", 40000));
		systemDat.save();
		systemDat.importFile(new File("./system.dat"));
		
		int rowsCounter = 0;
		for (Row row : systemDat.getDataIter()) {
			LibsLogger.debug(PigUnitTest.class, "Row ", rowsCounter, ": t=", row.get("c2"), " smt=", row.get("c3"), " pk=", row.get("pk"));
			rowsCounter++;
			if (rowsCounter == 10)
				break;
		}
		assertTrue(rowsCounter == 10, "Expected 10 rows from Data, there is " + rowsCounter);
		
		final Notebook notebookMOCCA = new Notebook(user.getUserId(), "MOCCA n=40k queries");
		notebookMOCCA.getMeta().add("DsQuery", "MOCCA n==40000");
		notebookMOCCA.getMeta().add("TbQuery", "system");
		notebookMOCCA.save();
		
		runAllJobs(1000);
		
		PigScript q = new PigScript(user, notebookMOCCA.getId(), "query line 1-10");
		q.setQuery("--Central potential for Demo MOCCA n=40k simulations\n" + 
				"rows = load 'mocca n==40000/system' using MultiTable();\n" +
//				"rows = FOREACH rows GENERATE CUT(tbid, '(0-4)..(28-31)') as tbid, c1 as tphys, c27 as u1;\n" +
				"rows = FOREACH rows GENERATE tbid, c1 as tphys, c27 as u1;\n" +
//				"X = LIMIT rows 3;\n" +
//				"DUMP X;\n"
				"DUMP rows;\n"
//				"store rows into 'plot1/TYPE lines COLUMNS tphys:u1 \"Central potential\" COLOR BY tbid LABELS \"Time [Myr]\", \"Central potential [Msun/pc^3]\" TITLE \"Central potential\"' using Plot();\n"
				);
		q.save();
		q.runQuery(false);
		
		runAllJobs();

//		Plot plot = Plot.getPlot(q.getId(), "plot1");
//		rowsCounter = 0;
//		for (Row row : plot.iterateRows()) {
//			LibsLogger.debug(PigUnitTest.class, "Row: ", row);
//			rowsCounter++;
//		}
//		assertTrue(rowsCounter == 10, "Expected 10 rows from Data, there is " + rowsCounter);
	}
	
//	@Test
	public void simpleQueryWithParams() throws IOException, ValidationException {
		final User user = createTestUser("ut");
		
		SearchManager.searchInstance().clearIndexType(WeblibsConst.KEYSPACE_LOWERCASE, Dataset.COLUMN_FAMILY.toLowerCase());
		SearchManager.searchInstance().clearIndexType(WeblibsConst.KEYSPACE_LOWERCASE, Table.COLUMN_FAMILY.toLowerCase());
		
		final Dataset dsLines = new Dataset(user.getUserId(), "lines");
		dsLines.save();
		addToRemove(dsLines);
		
		final Table line1_10 = new Table(dsLines, "line-1-10");
		line1_10.save();
		line1_10.importFile(SystemUtils.getResourceURL("testdata/line-1-10.plain").getFile());
		
		Data data = DataFactory.getData(WeblibsConst.KEYSPACE, line1_10.getId().getId());
		int rowsCounter = 0;
		for (Row row : data) {
			LibsLogger.debug(PigUnitTest.class, "Row: ", row);
			rowsCounter++;
		}
		assertTrue(rowsCounter == 10, "Expected 10 rows from Data, there is " + rowsCounter);
		
		final Notebook notebookLines = new Notebook(user.getUserId(), "lines queries");
		notebookLines.getMeta().add("DsQuery", "lines");
		notebookLines.getMeta().add("TbQuery", "line 1 10");
		notebookLines.save();
		
		runAllJobs(1000);
		
		PigScript q = new PigScript(user, notebookLines.getId(), "query line 1-10");
		q.setQuery("line = load '$DsQuery/$TbQuery' using Table();"
				+ "lineSqr = FOREACH line GENERATE tbid, x * x as x, y * y as y;"
				+ "plotData = FOREACH lineSqr GENERATE x, y, TBNAME(tbid) as tbname, DSNAME(tbid) as dsname, DSID(tbid) as dsid;"
				+ "dump plotData;"
				+ "store plotData into 'plot3/COLUMNS x:y TITLE \"Line squared\"' using Plot();"
				);
		q.save();
		q.runQuery(false);
		
		runAllJobs();
		

		String dataTableName = null;
//		Plot plot = null;
		
//		plot = PlotFactory.getPlot(q.getId());
//		rowsCounter = 0;
//		for (Row row : plot.getDataIter()) {
//			LibsLogger.debug(PigUnitTest.class, "Row: ", row);
//			rowsCounter++;
//		}
//		assertTrue(rowsCounter == 10, "Expected 10 rows from Data, there is " + rowsCounter);
	}

//	@Test
	public void simpleQuery() throws IOException, ValidationException {
		final User user = createTestUser("ut");
		
		SearchManager.searchInstance().clearIndexType(WeblibsConst.KEYSPACE_LOWERCASE, Dataset.COLUMN_FAMILY.toLowerCase());
		SearchManager.searchInstance().clearIndexType(WeblibsConst.KEYSPACE_LOWERCASE, Table.COLUMN_FAMILY.toLowerCase());
		
		final Dataset dsLines = new Dataset(user.getUserId(), "lines");
		dsLines.save();
		addToRemove(dsLines);
		
		final Table line1_10 = new Table(dsLines, "line-1-10");
		line1_10.save();
		line1_10.importFile(SystemUtils.getResourceURL("testdata/line-1-10.plain").getFile());
		
		Data data = DataFactory.getData(WeblibsConst.KEYSPACE, line1_10.getId().getId());
		int rowsCounter = 0;
		for (Row row : data) {
			LibsLogger.debug(PigUnitTest.class, "Row: ", row);
			rowsCounter++;
		}
		assertTrue(rowsCounter == 10, "Expected 10 rows from Data, there is " + rowsCounter);
		
		final Notebook notebookLines = new Notebook(user.getUserId(), "lines queries");
		notebookLines.save();
		
		runAllJobs(500);
		
		PigScript q = new PigScript(user, notebookLines.getId(), "query line 1-10");
		q.setQuery(
				"line = load 'lines/line 1 10' using Table();"
//				"line = load '" + cqlLocation + "' using CqlNativeStorage();"
//				"line = load '" + cqlLocation + "' using CqlStorage();"
//				+ "DESCRIBE line;"
//				+ "DUMP line;"
//				+ ""
				+ "lineSqr = FOREACH line GENERATE tbid, x * x as x, y * y as y;"
//				+ "DESCRIBE lineSqr;"
//				+ "DUMP lineSqr;"
//				+ ""
//				+ "lineSimple = FOREACH line GENERATE tbid.value as tbid, x.value as x, y.value as y;"
//				+ "DESCRIBE lineSimple;"
//				+ "DUMP lineSimple;"
//				+ ""
//				+ "lineShort = FILTER lineSimple BY x <= 8;"
//				+ "DESCRIBE lineShort;"
//				+ "DUMP lineShort;"
//				+ ""
//				+ "lineBinned = FOREACH lineShort GENERATE *, histogram(0.0, 8.0, 4.0, y) as bin;"
//				+ "DESCRIBE lineBinned;"
//				+ "DUMP lineBinned;"
//				+ ""
//				+ "lineGrouped = GROUP lineBinned BY bin;"
//				+ "DESCRIBE lineGrouped;"
//				+ "DUMP lineGrouped;"
//				+ ""
//				+ "lineGroupedCount = FOREACH lineGrouped GENERATE ('bin', $0), ('count', COUNT($1));"
//				+ "DESCRIBE lineGroupedCount;"
//				+ "DUMP lineGroupedCount;"
//				+ ""
//				+ "store lineGroupedCount into 'plot1/TYPE boxes 0.9 XRANGE 0; 10 YRANGE 0; 5 COLUMNS bin:count TITLE \"Initial mass distribution\"' using Plot();"
				
//				+ "plotData = FOREACH lineSqr GENERATE TOTUPLE(x, y);"
//				+ "dump plotData;"
//				+ "store plotData into 'plot1/COLUMNS x:y TITLE \"Line squared\"' using Plot();"
//				
//				+ "plotData = FOREACH lineSqr GENERATE TOTUPLE(TOTUPLE('uuid', UUID())), TOTUPLE(x, y);"
//				+ "dump plotData;"
//				+ "store plotData into 'plot2/COLUMNS x:y TITLE \"Line squared\"' using Plot();"

				+ "plotData = FOREACH lineSqr GENERATE x, y, TBNAME(tbid) as tbname, DSNAME(tbid) as dsname, DSID(tbid) as dsid;"
				+ "dump plotData;"
				+ "store plotData into 'plot3/COLUMNS x:y TITLE \"Line squared\"' using Plot();"
				);
		q.save();
		q.runQuery(false);
		
		runAllJobs();
	}
}
