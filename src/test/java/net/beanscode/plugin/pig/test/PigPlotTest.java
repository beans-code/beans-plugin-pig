package net.beanscode.plugin.pig.test;

import java.io.IOException;

import org.junit.Test;

import net.beanscode.model.connectors.CassandraConnector;
import net.beanscode.model.dataset.Dataset;
import net.beanscode.model.dataset.Table;
import net.beanscode.model.plugins.ColumnDef;
import net.beanscode.model.tests.BeansTestCase;
import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.schema.ColumnType;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.weblibs.SearchManager;
import net.hypki.libs5.weblibs.WeblibsConst;
import net.hypki.libs5.weblibs.user.User;

public class PigPlotTest extends BeansTestCase {

	@Test
	public void testPigWithPlot() throws IOException, ValidationException {
		SearchManager.searchInstance().clearIndexType(WeblibsConst.KEYSPACE_LOWERCASE, Dataset.COLUMN_FAMILY.toLowerCase());
		SearchManager.searchInstance().clearIndexType(WeblibsConst.KEYSPACE_LOWERCASE, Table.COLUMN_FAMILY.toLowerCase());
				
		final User user = createTestUser("ut");
		
		final Dataset ds = new Dataset(user.getUserId(), "test pig")
				.save();
		addToRemove(ds);
		
		final int rowsMax = 1000;
		final Table table = new Table(ds, "potential");
		table.getColumnDefs().add(new ColumnDef(DbObject.COLUMN_PK.toLowerCase(), null, ColumnType.LONG));
		table.getColumnDefs().add(new ColumnDef("tbid", null, ColumnType.STRING));
		table.getColumnDefs().add(new ColumnDef("c1", null, ColumnType.DOUBLE));
		table.getColumnDefs().add(new ColumnDef("c2", null, ColumnType.DOUBLE));
		table.getColumnDefs().add(new ColumnDef("c3", null, ColumnType.DOUBLE));
		table.save();
		
		CassandraConnector data = new CassandraConnector(table);
		double c2Sum = 0.0;
		double c3Sum = 0.0;
		for (long row = 0; row < rowsMax; row++) {
			
			Row r = new Row(UUID.random().getId());
			r.addColumn("tbid", table.getId().getId());
			r.addColumn("c1", 1.0 * row);
			r.addColumn("c2", 2.0 * row);
			r.addColumn("c3", 3.0 * row);
			data.write(r);
			
			c2Sum += r.getAsDouble("c2");
			c3Sum += r.getAsDouble("c3");
			
			if (row == (20000/3)) {
				LibsLogger.debug(PigUnitTest.class, "In total ", row, " saved");
			}
		}
		data.close();
		
		int rowsCounter = 0;
		double c2SumNew = 0.0;
		for (Row row : table.getDataIter()) {
			if (rowsCounter < 10)
				LibsLogger.debug(PigUnitTest.class, "row ", row);
			rowsCounter++;
			c2SumNew += (Double) row.get("c2");
		}
		assertTrue(rowsCounter == rowsMax, "Read " + rowsCounter + " rows but expected " + 123);
		assertTrue(c2Sum == c2SumNew, "Sum of the 2nd column does not match");
		
		runAllJobs(1000);
		
		String pig = "--Central potential for Demo MOCCA n=40k simulations\n" + 
				"rows = load 'test pig/potential' using TableFast();\n" +
				"rows = FOREACH rows GENERATE tbid, c1 as tphys, c2 as u1, c3 as z;\n" +
//				"X = LIMIT rows 10;\n" +
//				"DUMP X;\n"
//				"DUMP rows;\n"
				"store rows into 'plot1/COLUMNS tphys:u1 \"Central potential\" COLOR BY tbid LABELS \"Time [Myr]\", \"Central potential [Msun/pc^3]\" TITLE \"Central potential\"' using Plot();\n" +
				"store rows into 'plot2/COLUMNS tphys:z \"z\" COLOR BY tbid LABELS \"Time [Myr]\", \"z=c3\" TITLE \"z=c3\"' using Plot();\n" +
				"store rows into 'plot3/COLUMNS tphys:u1 COLUMNS tphys:z \"z\" COLOR BY tbid LABELS \"Time [Myr]\", \"u1 AND z=c3\" TITLE \"u1 AND z=c3\"' using Plot();\n"
				;
		
		final UUID queryId = UUID.random();
//		PigManager.runCassPigScript(pig, queryId, UUID.random(), null);
		
		runAllJobs(1000);
		
		
//		Plot plot = PlotFactory.getPlot(queryId);
//		assertTrue(plot != null, "Cannot find plot1");
//		c2SumNew = 0.0;
//		for (Row row : plot.getDataIter())
//			c2SumNew += (Double) row.get("u1");
//		assertTrue(c2Sum == c2SumNew, "Sum of the 2nd(=u1) column does not match");
//		plot.setFilename("ut-pig-plot1.pdf").plot();
//		
//		
//		plot = PlotFactory.getPlot(queryId);
//		assertTrue(plot != null, "Cannot find plot2");
//		double c3SumNew = 0.0;
//		for (Row row : plot.getDataIter())
//			c3SumNew += (Double) row.get("z");
//		assertTrue(c3Sum == c3SumNew, "Sum of the 3rd(=z) column does not match");
//		plot.setFilename("ut-pig-plot2.pdf").plot();
//		
//		plot = PlotFactory.getPlot(queryId);
//		assertTrue(plot != null, "Cannot find plot1");
//		plot.setFilename("ut-pig-plot3.pdf").plot();
	}
}
