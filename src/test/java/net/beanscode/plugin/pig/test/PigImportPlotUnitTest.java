package net.beanscode.plugin.pig.test;

import java.io.IOException;

import org.junit.Test;

import net.beanscode.model.dataset.Dataset;
import net.beanscode.model.dataset.Table;
import net.beanscode.model.tests.BeansTestCase;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.date.SimpleDate;
import net.hypki.libs5.utils.reflection.SystemUtils;

public class PigImportPlotUnitTest extends BeansTestCase {

	@Test
	public void bulkImportPlot() throws ValidationException, IOException {
//		SearchManager.searchInstance().clearIndexType(WeblibsConst.KEYSPACE_LOWERCASE, Dataset.COLUMN_FAMILY.toLowerCase());
//		SearchManager.searchInstance().clearIndexType(WeblibsConst.KEYSPACE_LOWERCASE, Table.COLUMN_FAMILY.toLowerCase());
		
		final Dataset dataset = createDataset(testUser(), "bulk system " + SimpleDate.now().toStringHuman());
		
		final Table tableNoHeader = 
				createTable(dataset, "fracb 0.1")
				.importFile(SystemUtils.getResourceURL("testdata/mocca/n=3000/fracb=0.1/system.dat.beans").getFile());
		
		runAllJobs(1000);
		
		String pig = "rows = load 'bulk system/fracb 0.1' using TableFast();\n" +
				"rows = FOREACH rows GENERATE tbid, tphys, u1;\n" +
				"store rows into 'plot1/COLUMNS tphys:u1 TYPE points \"Central potential\" COLOR BY tbid LABELS \"Time [Myr]\", \"Central potential [Msun/pc^3]\" TITLE \"Central potential\"' using Plot();\n"
				;
		
		final UUID queryId = UUID.random();
//		PigManager.runCassPigScript(pig, queryId, UUID.random(), null);
		
		runAllJobs(1000);
		
		
//		Plot plot = PlotFactory.getPlot(queryId);
//		assertTrue(plot != null, "Cannot find plot1");
//		plot.setFilename("ut-bulk-pig-plot1.pdf").plot();
	}
}
