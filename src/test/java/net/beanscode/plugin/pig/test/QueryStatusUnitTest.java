package net.beanscode.plugin.pig.test;

import java.io.IOException;

import net.beanscode.model.dataset.Dataset;
import net.beanscode.model.dataset.Table;
import net.beanscode.model.notebook.Notebook;
import net.beanscode.model.tests.BeansTestCase;
import net.beanscode.plugin.pig.PigScript;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.utils.reflection.SystemUtils;
import net.hypki.libs5.weblibs.user.User;

import org.junit.Test;

public class QueryStatusUnitTest extends BeansTestCase {
	
	@Test
	public void testStatusManually() throws ValidationException, IOException {
		final User user = testUser();
		final Notebook note = createNotebook(user, "test notebook");
		
		PigScript q = new PigScript(user, note.getId(), "fail1");
		q.setQuery("dummy script 1");
		q.save();
		addToRemove(q);
//		new QueryStatus(q, PigScriptStatus.RUNNING, "Query started").save();
//		new QueryStatus(q, PigScriptStatus.FAILED, "Query finished successfully").save();
		
		q = new PigScript(user, note.getId(), "fail2");
		q.setQuery("dummy script 2");
		q.save();
		addToRemove(q);
//		new QueryStatus(q, PigScriptStatus.RUNNING, "Query started").save();
//		new QueryStatus(q, PigScriptStatus.FAILED, "Query finished successfully").save();
		
		q = new PigScript(user, note.getId(), "success1");
		q.setQuery("dummy script - success 1");
		q.save();
		addToRemove(q);
//		new QueryStatus(q, PigScriptStatus.RUNNING, "Query started").save();
//		new QueryStatus(q, PigScriptStatus.DONE, "Query finished successfully").save();
		
		q = new PigScript(user, note.getId(), "success2");
		q.setQuery("dummy script - success 2");
		q.save();
		addToRemove(q);
//		new QueryStatus(q, PigScriptStatus.RUNNING, "Query started").save();
//		new QueryStatus(q, PigScriptStatus.DONE, "Query finished successfully").save();
		
		runAllJobs(1500);
		
//		List<QueryStatusSorted> statuses = QueryStatusSorted.searchLastUpdated(user.getUserId(), System.currentTimeMillis(), 100);
//		assertTrue(statuses.size() == 4, "Expected 4 status, there is " + statuses.size());
//		assertTrue(statuses.get(0).getStatus() == PigScriptStatus.DONE, "Expected " + PigScriptStatus.DONE);
//		assertTrue(statuses.get(1).getStatus() == PigScriptStatus.DONE, "Expected " + PigScriptStatus.DONE);
//		assertTrue(statuses.get(2).getStatus() == PigScriptStatus.FAILED, "Expected " + PigScriptStatus.DONE);
//		assertTrue(statuses.get(3).getStatus() == PigScriptStatus.FAILED, "Expected " + PigScriptStatus.DONE);
	}

//	@Test
	public void testStatus() throws ValidationException, IOException {
		final User user = testUser();
		final Notebook note = createNotebook(user, "test notebook");
		
		Dataset ds01 = new Dataset()
			.setName("MOCCA")
			.setUserId(user.getUserId())
			.addMeta("n", 3000)
			.addMeta("fracb", 0.1)
			.save();
		Dataset ds05 = new Dataset()
			.setName("MOCCA")
			.setUserId(user.getUserId())
			.addMeta("n", 3000)
			.addMeta("fracb", 0.5)
			.save();
		addToRemove(ds01);
		addToRemove(ds05);
		
		Table t1 = new Table(ds01, "system")
			.save()
			.importFile(SystemUtils.getResourceURL("testdata/mocca/n=3000/fracb=0.1/system.dat.beans").getFile());
		Table t2 = new Table(ds05, "system")
			.save()
			.importFile(SystemUtils.getResourceURL("testdata/mocca/n=3000/fracb=0.5/system.dat.beans").getFile());
		addToRemove(t1);
		addToRemove(t2);

		runAllJobs(1000);
		
		PigScript q = new PigScript(user, note.getId(), "b-fail1");
		q.setQuery("dummy script");
		q.save();
		q.runQuery(true);
		addToRemove(q);
		
		q = new PigScript(user, note.getId(), "b-fail2");
		q.setQuery("dummy script");
		q.save();
		q.runQuery(true);
		addToRemove(q);
		
		q = new PigScript(user, note.getId(), "b-success1");
		q.setQuery("rows = LOAD 'n == 3000 and fracb == 0.1/system' using Table();"
				+ "store rows into 'plot1/TYPE points "
				+ "						COLUMNS tphys:smt "
				+ "						TITLE \"Mass of the cluster for 2 simulations\"' "
				+ "										using Plot();");
		q.save();
		q.runQuery(true);
		addToRemove(q);
		
		q = new PigScript(user, note.getId(), "b-success2");
		q.setQuery("rows = LOAD 'n == 3000 and fracb == 0.5/system' using Table();"
				+ "store rows into 'plot1/TYPE points "
				+ "						COLUMNS tphys:smt "
				+ "						TITLE \"Mass of the cluster for 2 simulations\"' "
				+ "										using Plot();");
		q.save();
		q.runQuery(true);
		addToRemove(q);
		
		runAllJobs(1000);
		
//		List<QueryStatusSorted> statuses = QueryStatusSorted.searchLastUpdated(user.getUserId(), System.currentTimeMillis(), 4);
//		assertTrue(statuses.size() == 4, "Expected 4 status, there is " + statuses.size());
//		assertTrue(statuses.get(0).getStatus() == PigScriptStatus.DONE, "Expected " + PigScriptStatus.DONE);
//		assertTrue(statuses.get(0).getQueryName().equals("b-success2"));
//		assertTrue(statuses.get(1).getStatus() == PigScriptStatus.DONE, "Expected " + PigScriptStatus.DONE);
//		assertTrue(statuses.get(1).getQueryName().equals("b-success1"));
//		assertTrue(statuses.get(2).getStatus() == PigScriptStatus.FAILED, "Expected " + PigScriptStatus.DONE);
//		assertTrue(statuses.get(2).getQueryName().equals("b-fail2"));
//		assertTrue(statuses.get(3).getStatus() == PigScriptStatus.FAILED, "Expected " + PigScriptStatus.DONE);
//		assertTrue(statuses.get(3).getQueryName().equals("b-fail1"));
	}
}
