package net.beanscode.plugin.pig.test;

import java.io.IOException;

import org.junit.Test;

import net.beanscode.model.connectors.PlainConnector;
import net.beanscode.model.dataset.Dataset;
import net.beanscode.model.dataset.Table;
import net.beanscode.model.dataset.TableFactory;
import net.beanscode.model.notebook.Notebook;
import net.beanscode.model.tests.BeansTestCase;
import net.beanscode.plugin.pig.BeansTable;
import net.beanscode.plugin.pig.PigManager;
import net.beanscode.plugin.pig.PigScript;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.weblibs.user.User;

public class UniTableUnitTest extends BeansTestCase {

	@Test
	public void testUniTableDUMP() throws IOException, ValidationException {
		try {
			final User user = createTestUser("testUser");
			
			final Dataset ds = new Dataset(user.getUserId(), "testDataset")
					.save();
			addToRemove(ds);
			
			Notebook notebook = createNotebook(user, "testNotebook");
			
			final Table table = new Table(ds, "line 1-10");
			table.getConnectorList().addConnector(new PlainConnector(new FileExt("$HOME/beans/model/src/main/resources/testdata/line-1-10.json")));
			table.save();
					
			runAllJobs(1000);
			
			String pig = "rows10 = LOAD 'testDataset/line' using BeansTable(); \n"
					+ "DUMP rows10;\n"
					+ "DESCRIBE rows10;\n"
					+ ""
					+ "onlyX = FOREACH rows10 GENERATE x;\n"
					+ "only3 = FILTER onlyX BY x == 3;\n"
					+ "DUMP only3;\n"
					+ ""
					+ "STORE onlyX INTO 'NAME \"onlyX\"' using " + BeansTable.class.getSimpleName() + "();"
					;
			
			PigScript pigScript = new PigScript(notebook, "testPig");
			pigScript.save();
			
			runAllJobs();
			
			PigManager.runCassPigScriptInternal(pigScript, null);
			
			runAllJobs(1000);
			
			for (Table tableFromPig : TableFactory.iterateTables(pigScript)) {
				LibsLogger.debug(UniTableUnitTest.class, "Table from Pig: ", tableFromPig);
				for (Row row : tableFromPig.getConnectorList()) {
					LibsLogger.debug(UniTableUnitTest.class, "Row: ", row);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
