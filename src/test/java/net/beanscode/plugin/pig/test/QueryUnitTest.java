package net.beanscode.plugin.pig.test;

import java.io.IOException;

import net.beanscode.model.notebook.Notebook;
import net.beanscode.model.tests.BeansTestCase;
import net.beanscode.plugin.pig.PigScript;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.weblibs.user.User;

public class QueryUnitTest extends BeansTestCase {
	
//	@Test
//	public void testRun() throws ValidationException, IOException {
//		final String notebookName 	= "test-notebook";
//		final User user 			= createTestUser("t");
//		final Notebook notebook 	= createNotebook(user, notebookName);
//		
//		Dataset ds = new Dataset(user.getUserId(), "unittest dataset");
//		ds.save();
//		
//		Table table = new Table(ds, "system.dat");
//		table.save();
//		
//		table.importFile(SystemUtils.getResourceURL(QueryUnitTest.class, "testdata/mocca/n=3000/fracb=0.1/system.dat.beans").getFile());
//		
//		final int nrLines = FileUtils.countLines(SystemUtils.getResourceAsStream(QueryUnitTest.class, "testdata/mocca/n=3000/fracb=0.1/system.dat.beans")) - 1; // excluding first line comment
//		int tableRowsCount = 0;
//		for (Row row : table.getDataIter()) {
//			tableRowsCount++;
//		}
//		assertTrue(nrLines == tableRowsCount, "nrLinesExpected=" + nrLines + " tableRowsCount=" + tableRowsCount);
//		
//		PigScript q = new PigScript(user, notebook.getId(), notebookName);
//		q.setQuery("rows = load 'unittest dataset/system.dat' using MultiTable();\n\n" + 
//				   "store rows into 'plot1/TYPE points COLUMNS tphys:smt COLOR BY tbid' using Plot();");
//		q.save();
//		runAllJobs();
//		
//		q.runQuery(false);
//		runAllJobs();
//		
//		Plot plot = PlotFactory.getPlot(q.getId());
//		
//		// check if number of lines == PlotData size
//		int plotRows = 0;
//		for (Row row : plot.getDataIter())
//			plotRows++;
//		assertTrue(nrLines == plotRows, "nrLinesExpected=" + nrLines + " plotdata.size=" + plotRows);
//		
//		// ploto to the file
//		plot.setFilename("ut-QueryUnitTest-testRun.pdf").plot();
//		
//		// run query again and check number of lines again
//		q.runQuery(false);
//		runAllJobs();
//		plotRows = 0;
//		for (Row row : plot.getDataIter())
//			plotRows++;
//		assertTrue(nrLines == plotRows, "nrLines=" + nrLines + " plotdata.size=" + plotRows);
//		
//		// run query again and check number of lines again
//		q.runQuery(false);
//		runAllJobs();
//		plotRows = 0;
//		for (Row row : plot.getDataIter())
//			plotRows++;
//		assertTrue(nrLines == plotRows, "nrLines=" + nrLines + " plotdata.size=" + plotRows);
//		
//		// plot2
//		
//		q = new PigScript(user, notebook.getId(), notebookName);
//		q.setQuery("rows = load 'unittest dataset/system.dat' using MultiTable();\n\n" + 
//				   "store rows into 'plot2/TYPE points COLUMNS tphys:smt COLOR BY tbid' using Plot();");
//		q.save();
//		runAllJobs();
//		
//		q.runQuery(false);
//		runAllJobs();
//		
//		plot = PlotFactory.getPlot(q.getId());
//		plot.setFilename("ut-QueryUnitTest-testRun-2.pdf").plot();
//		
//		// check if number of lines == PlotData size
//		plotRows = 0;
//		for (Row row : plot.getDataIter())
//			plotRows++;
//		assertTrue(nrLines == plotRows, "nrLines=" + nrLines + " plotdata.size=" + plotRows);
//		
//		// run query again and check number of lines again
//		q.runQuery(false);
//		runAllJobs();
//		plotRows = 0;
//		for (Row row : plot.getDataIter())
//			plotRows++;
//		assertTrue(nrLines == plotRows, "nrLines=" + nrLines + " plotdata.size=" + plotRows);
//		
//	}

//	@Test
	public void testCreate() throws ValidationException, IOException {
		final User user = testUser();
		final Notebook note = createNotebook(user, "test notebook");
		final String queryName = "new name";
		
		PigScript q = new PigScript(user, note.getId(), queryName);
		q.save();
		addToRemove(q);
		
		runAllJobs();
		
		Notebook noteDb = Notebook.getNotebook(note.getId());
		assertTrue(noteDb.getEntries().size() == 1);
	}
	
//	@Test
	public void testProgress() throws ValidationException, IOException {
//		final User user = testUser();
//		final Notebook note = createNotebook(user, "test notebook");
//		final String queryName = "new name";
//		
//		PigScript q = new PigScript(user, note.getId(), queryName);
//		q.setQuery("dummy script");
//		q.save();
//		addToRemove(q);
//		
//		QueryStatus qs = QueryStatus.getStatus(q.getId());
//		assertTrue(qs == null);
//		
//		q.runQuery(true);
//		
//		qs = QueryStatus.getStatus(q.getId());
//		assertTrue(qs.getProgress() == 0);
//		
//		runAllJobs();
//					
//		qs = QueryStatus.getStatus(q.getId());
//		assertTrue(qs.getProgress() == 100, "Query in the case of failure did not report progress equal to 100%");
	}
	
}
