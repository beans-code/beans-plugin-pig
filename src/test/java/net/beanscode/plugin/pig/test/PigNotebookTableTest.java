package net.beanscode.plugin.pig.test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import net.beanscode.model.BeansConst;
import net.beanscode.model.BeansSearchManager;
import net.beanscode.model.connectors.CassandraConnector;
import net.beanscode.model.dataset.Dataset;
import net.beanscode.model.dataset.Table;
import net.beanscode.model.dataset.TableFactory;
import net.beanscode.model.notebook.Notebook;
import net.beanscode.model.plugins.ColumnDef;
import net.beanscode.model.tests.BeansTestCase;
import net.beanscode.plugin.pig.PigManager;
import net.beanscode.plugin.pig.PigScript;
import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.schema.ColumnType;
import net.hypki.libs5.db.db.schema.TableSchema;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.weblibs.SearchManager;
import net.hypki.libs5.weblibs.WeblibsConst;
import net.hypki.libs5.weblibs.user.User;

public class PigNotebookTableTest extends BeansTestCase {

	@Test
	public void testPigWithPlot() throws IOException, ValidationException {
		SearchManager.searchInstance().clearIndexType(WeblibsConst.KEYSPACE_LOWERCASE, Dataset.COLUMN_FAMILY.toLowerCase());
		SearchManager.searchInstance().clearIndexType(WeblibsConst.KEYSPACE_LOWERCASE, Table.COLUMN_FAMILY.toLowerCase());
				
		final User user = createTestUser("ut");
		
		final Dataset ds = new Dataset(user.getUserId(), "test pig notebook table")
				.save();
		addToRemove(ds);
		
		final int rowsMax = 100;
		final Table table = new Table(ds, "potential");
		table.getColumnDefs().add(new ColumnDef(DbObject.COLUMN_PK.toLowerCase(), null, ColumnType.LONG));
		table.getColumnDefs().add(new ColumnDef("tbid", null, ColumnType.STRING));
		table.getColumnDefs().add(new ColumnDef("c1", null, ColumnType.DOUBLE));
		table.getColumnDefs().add(new ColumnDef("c2", null, ColumnType.DOUBLE));
		table.getColumnDefs().add(new ColumnDef("c3", null, ColumnType.DOUBLE));
		table.save();
		
		CassandraConnector data = new CassandraConnector(table);
		double c2Sum = 0.0;
		double c3Sum = 0.0;
		for (long row = 0; row < rowsMax; row++) {
			
			Row r = new Row(UUID.random().getId());
			r.addColumn("tbid", table.getId().getId());
			r.addColumn("c1", 1.0 * row);
			r.addColumn("c2", 2.0 * row);
			r.addColumn("c3", 3.0 * row);
			data.write(r);
			
			c2Sum += r.getAsDouble("c2");
			c3Sum += r.getAsDouble("c3");
			
			if (row == (20000/3)) {
				LibsLogger.debug(PigNotebookTableTest.class, "In total ", row, " saved");
			}
		}
		data.close();
		
		int rowsCounter = 0;
		double c2SumNew = 0.0;
		for (Row row : table.getDataIter()) {
			if (rowsCounter < 10)
				LibsLogger.debug(PigNotebookTableTest.class, "row ", row);
			rowsCounter++;
			c2SumNew += row.getAsDouble("c2");
		}
		assertTrue(rowsCounter == rowsMax, "Read " + rowsCounter + " rows but expected " + 123);
		assertTrue(c2Sum == c2SumNew, "Sum of the 2nd column does not match");
		
		runAllJobs(1000);
		
//		final UUID notebookId = UUID.random();
		final Notebook notebook = new Notebook(user.getUserId(), "testnote");
		notebook.save();
		LibsLogger.debug(PigNotebookTableTest.class, "Notebook ", notebook.getId(), " saved");
		
		final PigScript pigScript = new PigScript(user, notebook.getId(), "test-query");
				
		String pig = "--Central potential for Demo MOCCA n=40k simulations\n" + 
				"rows = load 'test pig/potential' using TableFast();\n" +
				"rows5 = FILTER rows BY c1 <= 5;\n" +
				"rows15 = FILTER rows BY c1 <= 15;\n" +
				"STORE rows5 INTO 'NAME \"tab5\"' using NotebookTable();\n" + 
				"STORE rows15 INTO 'NAME \"tab15\"' using NotebookTable();\n"
				;
		
		pigScript.setQuery(pig);
		pigScript.save();
		
		notebook.addEntryId(pigScript.getId());
		
		PigManager.runCassPigScriptInternal(pigScript, null);
		
		runAllJobs(1000);
		
		LibsLogger.debug(PigNotebookTableTest.class, "Table tab5");
		Table nt = Table.getTable(pigScript.getId(), "tab5");
		rowsCounter = 0;
		for (Row row : nt.getDataIter()) {
			rowsCounter++;
			LibsLogger.debug(PigNotebookTableTest.class, "NotebookTable: c1= ", row.get("c1"), " c2= ", row.get("c2"), " c3= ", row.get("c3"));
		}
		assertTrue(rowsCounter == 6, "Expected to read 6 rows from tab5");
		
		LibsLogger.debug(PigNotebookTableTest.class, "Table tab15");
		nt = Table.getTable(pigScript.getId(), "tab15");
		rowsCounter = 0;
		for (Row row : nt.getDataIter()) {
			rowsCounter++;
			LibsLogger.debug(PigNotebookTableTest.class, "NotebookTable: c1= ", row.get("c1"), " c2= ", row.get("c2"), " c3= ", row.get("c3"));
		}
		assertTrue(rowsCounter == 16, "Expected to read 16 rows from tab15");
		
		int ntCount = 0;
		for (Table notebookTable : TableFactory.iterateTables(pigScript)) {
			LibsLogger.debug(PigNotebookTableTest.class, "Found NotebookTable ", notebookTable);
			ntCount++;
		}
		assertTrue(ntCount == 2, "Expected 2 tables");
		final List<String> expecedTables = new ArrayList<String>();
		expecedTables.add("tab5");
		expecedTables.add("tab15");
		for (Table notebookTable : TableFactory.iterateTables(pigScript)) {
			assertTrue(expecedTables.remove(notebookTable.getName()), "Table " + notebookTable.getName() + " was not expected to exist");
		}
		assertTrue(expecedTables.size() == 0, "Not all expected table were found in DB");
		
		runAllJobs(1000);
		
//		Plot plot = new Plot();
////		plot.setText("TITLE \"line5\" "
////				+ "NOTEBOOKS \"" + notebook.getId() + "\" "
////				+ "TABLES \"tab5\" "
////				+ "POINTS c1:c2");
//		plot.setFilename("tab5.pdf").plot();
		
		// remove all objects
		notebook.remove();
		ds.remove();
		user.remove();
		
		runAllJobs();
		
		
		for (TableSchema cf : BeansConst.getAllTableSchemas()) {			
			assertTrue(DbObject.getDatabaseProvider().isTableEmpty(WeblibsConst.KEYSPACE, cf.getName()), cf.getName() + " should be empty");
			assertTrue(BeansSearchManager.getSearchManager().isIndexTypeEmpty(WeblibsConst.KEYSPACE_LOWERCASE, cf.getName()), "Index " + cf.getName() + " is not empty");
		}
	}
}
