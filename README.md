# Apache Pig plugin for BEANS

`BEANS` is a web-based software for interactive distributed data analysis with a clear interface for querying, filtering, aggregating, and plotting data from an arbitrary number of datasets and tables [https://gitlab.com/beans-code/beans](https://gitlab.com/beans-code/beans).

`Apache Pig` is a platform for analyzing large data sets that consists of a high-level language for expressing data analysis programs, coupled with infrastructure for evaluating these programs [https://pig.apache.org](https://pig.apache.org).

## Features

`BEANS` fully supports `Apache Pig` and allows to write scripts which can directly, in parallel, read any datasets and tables from `BEANS`. The same for writing. The schemas of tables are also fully supported. With `BeansTable` in `LOAD` command one can read the data directly from `BEANS`.
